{-# LANGUAGE OverloadedStrings #-}

module SpecUtils (api, getTestHealthRecord, deleteAccount, createTestHealthRecordRows, deleteAll, createAccount, createProfile, createTestAccount, deleteTestAccountHealthRecordIfExist, signInTestAccount, deleteTestAccount, deleteTestAccountProfileIfExist, healthRecordData1, healthRecordData2, healthRecordData3, healthRecordData4, healthRecordData5, healthRecordData, createTestProfile, deleteTestAccountAll, createHealthRecordRows) where

import           Configuration.Dotenv      (defaultConfig, loadFile)
import           Control.Monad             (when)
import           Control.Monad.IO.Class    (MonadIO (liftIO))
import           Data.Aeson                (KeyValue ((.=)), Value, object)
import           Data.Csv                  (Header)
import           Data.Either.Extra         (fromRight')
import           Data.Maybe                (fromJust)
import           Data.String.Conversions   (cs)
import           Data.Time                 (fromGregorian)
import           Data.Vector               (Vector)
import           Db.Session                (checkAccountExistSession,
                                            deleteAccountSession)
import           Hasql.Pool                (use)
import           Lib                       (getDbPool)
import           Network.HTTP.Simple       (Response, addRequestHeader,
                                            getResponseHeader, httpJSON,
                                            parseRequest, setRequestBodyJSON)
import           Network.HTTP.Types        (hCookie)
import           Network.HTTP.Types.Header (hSetCookie)
import           Servant                   (ServerError)
import           System.Directory          (doesFileExist, removeFile)
import           Types.HealthRecord        (HealthRecordTable (HealthRecordTable))
import           Utils.HandleCsv           (FileType (HealthRecord, Profile),
                                            getCsvFilePath,
                                            parseByteStringToCsv, readCsv)
import           Web.Cookie                (SetCookie (setCookieValue),
                                            parseSetCookie)

api :: String -> String -> String
api method route = method <> " http://localhost:8080" <> route

createAccount :: String -> String -> IO ()
createAccount accountName accountPassword = do
  loadFile defaultConfig
  pool <- getDbPool
  Right _ <- liftIO $ use pool $ deleteAccountSession (cs accountName)
  signUpRequest' <- liftIO $ parseRequest ("POST" `api` "/auth/sign_up")
  let signUpRequest = setRequestBodyJSON
                      (object ["name" .= accountName, "password" .= accountPassword, "passwordConfirm" .= accountPassword])
                      signUpRequest'
  _ <- liftIO (httpJSON signUpRequest :: IO (Response Value))
  return ()

createTestAccount :: IO ()
createTestAccount = createAccount "existed_account" "password"

signInAccount :: String -> String -> IO String
signInAccount accountName accountPassword = do
  loadFile defaultConfig
  signInRequest' <- liftIO $ parseRequest ("POST" `api` "/auth/sign_in")
  let signInRequest = setRequestBodyJSON
                      (object ["name" .= accountName, "password" .= accountPassword])
                      signInRequest'
  signInResponse <- liftIO (httpJSON signInRequest :: IO (Response Value))
  let signInsetCookieHeader = cs $ head $ getResponseHeader hSetCookie signInResponse :: String
  let cookieValue = "servant-auth-cookie=" <> (cs $ setCookieValue $ parseSetCookie $ cs signInsetCookieHeader :: String)
  return cookieValue

signInTestAccount :: IO String
signInTestAccount = signInAccount "existed_account" "password"

createProfile :: String -> String -> String -> IO String
createProfile gender birthDate cookieValue = do
  loadFile defaultConfig
  deleteTestAccountProfileIfExist
  createProfileRequest' <- parseRequest ("POST" `api` "/profile")
  let createProfileRequest = setRequestBodyJSON
                             (object ["gender" .= gender, "birthDate" .= birthDate]) $
                             addRequestHeader hCookie (cs cookieValue)
                             createProfileRequest'
  _ <- httpJSON createProfileRequest :: IO (Response Value)
  return cookieValue

createTestProfile :: String -> IO String
createTestProfile = createProfile "Male" "1991-01-01"

deleteAccount :: String -> IO ()
deleteAccount accountName = do
  loadFile defaultConfig
  pool <- getDbPool
  Right _ <- liftIO $ use pool $ deleteAccountSession (cs accountName)
  return ()

deleteTestAccount :: IO ()
deleteTestAccount = deleteAccount "existed_account"

deleteProfileIfExist :: String -> IO ()
deleteProfileIfExist accountName = do
  loadFile defaultConfig
  pool <- getDbPool
  Right maybeAccount <- liftIO $ use pool $ checkAccountExistSession (cs accountName)
  case maybeAccount of
    Just (accountId, _, _) -> do
      csvFilePath <- liftIO $ getCsvFilePath (show accountId) Profile
      isFileExist <- liftIO $ doesFileExist csvFilePath
      when isFileExist $ removeFile csvFilePath
    Nothing -> return ()

deleteTestAccountProfileIfExist :: IO ()
deleteTestAccountProfileIfExist = deleteProfileIfExist "existed_account"

deleteHealthRecordIfExist :: String -> IO ()
deleteHealthRecordIfExist accountName = do
  loadFile defaultConfig
  pool <- getDbPool
  Right maybeAccount <- liftIO $ use pool $ checkAccountExistSession (cs accountName)
  case maybeAccount of
    Just (accountId, _, _) -> do
      csvFilePath <- liftIO $ getCsvFilePath (show accountId) HealthRecord
      isFileExist <- liftIO $ doesFileExist csvFilePath
      when isFileExist $ removeFile csvFilePath
    Nothing -> return ()


deleteTestAccountHealthRecordIfExist :: IO ()
deleteTestAccountHealthRecordIfExist = deleteHealthRecordIfExist "existed_account"

deleteAll :: String -> IO ()
deleteAll accountName = do
  deleteAccount accountName
  deleteProfileIfExist accountName
  deleteHealthRecordIfExist accountName

deleteTestAccountAll :: IO ()
deleteTestAccountAll = do
  deleteTestAccount
  deleteTestAccountProfileIfExist
  deleteTestAccountHealthRecordIfExist

healthRecordData1 :: HealthRecordTable
healthRecordData1 = HealthRecordTable
                      (fromGregorian 2000 1 1)
                      180
                      70
                      (Just 15)
                      (Just 22)
healthRecordData2 :: HealthRecordTable
healthRecordData2 = HealthRecordTable
                      (fromGregorian 2000 1 2)
                      181
                      71
                      (Just 16)
                      (Just 23)
healthRecordData3 :: HealthRecordTable
healthRecordData3 = HealthRecordTable
                      (fromGregorian 2000 1 3)
                      182
                      72
                      (Just 17)
                      (Just 24)
healthRecordData4 :: HealthRecordTable
healthRecordData4 = HealthRecordTable
                      (fromGregorian 2000 1 4)
                      183
                      73
                      (Just 18)
                      (Just 25)
healthRecordData5 :: HealthRecordTable
healthRecordData5 = HealthRecordTable
                      (fromGregorian 2000 1 5)
                      184
                      74
                      (Just 19)
                      (Just 26)

healthRecordData :: [HealthRecordTable]
healthRecordData = [ healthRecordData1
                   , healthRecordData2
                   , healthRecordData3
                   , healthRecordData4
                   , healthRecordData5
                   ]

createHealthRecordRows :: [HealthRecordTable] -> String -> IO String
createHealthRecordRows healthRecords cookieValue = do
  mapM_ sendRequest healthRecords
  return cookieValue
  where
    sendRequest :: HealthRecordTable -> IO ()
    sendRequest healthRecord = do
      createHealthRecordRequest' <- parseRequest ("POST" `api` "/health_record")
      let createHealthRecordRequest = setRequestBodyJSON healthRecord $
                                      addRequestHeader hCookie (cs cookieValue)
                                      createHealthRecordRequest'
      _ <- httpJSON createHealthRecordRequest :: IO (Response Value)
      return ()


createTestHealthRecordRows :: String -> IO String
createTestHealthRecordRows = createHealthRecordRows healthRecordData


getHealthRecord :: String
                -> IO (Either (ServerError, Maybe String)
                              (Header, Vector HealthRecordTable))
getHealthRecord accountName = do
  loadFile defaultConfig
  pool <- getDbPool
  eitherMaybeAccount <- liftIO $ use pool $ checkAccountExistSession (cs accountName)
  let (accountId, _, _) = (fromJust . fromRight') eitherMaybeAccount
  rawCsvByteString <- liftIO $ readCsv (show accountId) HealthRecord
  return $ rawCsvByteString >>=
            parseByteStringToCsv


getTestHealthRecord :: IO (Either (ServerError, Maybe String)
                                  (Header, Vector HealthRecordTable))
getTestHealthRecord = getHealthRecord "existed_account"

