{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module AuthSpec (authSpec) where

import           Configuration.Dotenv      (defaultConfig, loadFile)
import           Control.Monad.IO.Class    (MonadIO (liftIO))
import           Data.Aeson                (Value)
import           Data.Aeson.QQ.Simple      (aesonQQ)
import           Data.Maybe                (fromJust, isJust, isNothing)
import           Data.String.Conversions   (cs)
import           Db.Session                (checkAccountExistSession,
                                            deleteAccountSession)
import           Hasql.Pool                (use)
import           Lib                       (getDbPool)
import           Network.HTTP.Simple       (Response, addRequestHeader,
                                            getResponseBody, getResponseHeader,
                                            getResponseStatusCode, httpJSON,
                                            parseRequest, setRequestBodyJSON)
import           Network.HTTP.Types.Header (hCookie, hSetCookie)
import           SpecUtils                 (api, createTestAccount)
import           Test.Hspec                (SpecWith, afterAll_, after_,
                                            beforeAll_, context, describe, it,
                                            shouldBe, shouldEndWith,
                                            shouldSatisfy, shouldStartWith)
import           Web.Cookie                (SetCookie (setCookieValue),
                                            parseSetCookie)

authSpec :: SpecWith ()
authSpec =
  afterAll_ deleteTestAndExistedAccount $
  beforeAll_ createTestAndExistedAccount $
  after_ deleteTestAccount $ do
    let signUp = "/auth/sign_up"
    describe ("POST " <> signUp) $ do
      context "JSON" $ do
        it "with correct body should respond 201" $ do
          request' <- parseRequest ("POST" `api` signUp)
          let request = setRequestBodyJSON
                        [aesonQQ|{ "name": "test_account"
                                 , "password": "test_password"
                                 , "passwordConfirm": "test_password"
                                 }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": null
            , "payload": { "name": "test_account" }
            , "status": "Ok"
            }
          |]
          getResponseStatusCode response `shouldBe` 201

          pool <- getDbPool
          Right maybeAccount <- liftIO $ use pool (checkAccountExistSession "test_account")

          maybeAccount `shouldSatisfy` isJust
          fromJust maybeAccount `shouldSatisfy` (\(_, n, _) -> n == "test_account")

        it "with missing name body should respond 400" $ do
          request' <- parseRequest ("POST" `api` signUp)
          let request = setRequestBodyJSON
                        [aesonQQ|{ "password": "test_password"
                                 , "passwordConfirm": "test_password"
                                 }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $: key \"name\" not found"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

          pool <- getDbPool
          Right maybeAccount <- liftIO $ use pool (checkAccountExistSession "test_account")

          maybeAccount `shouldSatisfy` isNothing

        it "with missing password body should respond 400" $ do
          request' <- parseRequest ("POST" `api` signUp)
          let request = setRequestBodyJSON
                        [aesonQQ|{ "name": "test_account"
                                 , "passwordConfirm": "test_password"
                                 }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $: key \"password\" not found"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

        it "with missing passwordConfirm body should respond 400" $ do
          request' <- parseRequest ("POST" `api` signUp)
          let request = setRequestBodyJSON
                        [aesonQQ|{ "name": "test_account"
                                 , "password": "test_password"
                                 }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $: key \"passwordConfirm\" not found"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

        it "with invalid passwordConfirm body should respond 400" $ do
          request' <- parseRequest ("POST" `api` signUp)
          let request = setRequestBodyJSON
                        [aesonQQ|{ "name": "test_account"
                                 , "password": "test_password"
                                 , "passwordConfirm": "different_password"
                                 }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "password confirm not pass"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

        it "with empty body should respond 400" $ do
          request' <- parseRequest ("POST" `api` signUp)
          let request = setRequestBodyJSON
                        [aesonQQ|{ }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $: key \"name\" not found"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

          pool <- getDbPool
          Right maybeAccount <- liftIO $ use pool (checkAccountExistSession "test_account")

          maybeAccount `shouldSatisfy` isNothing

        it "with same name should respond 400" $ do
          request' <- parseRequest ("POST" `api` signUp)
          let request = setRequestBodyJSON
                        [aesonQQ|{ "name": "existed_account"
                                 , "password": "password"
                                 , "passwordConfirm": "password"
                                 }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "account has been taken"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

          pool <- getDbPool
          Right maybeAccount <- liftIO $ use pool (checkAccountExistSession "test_account")

          maybeAccount `shouldSatisfy` isNothing

    let signIn = "/auth/sign_in"
    describe ("POST " <> signIn) $ do
      context "JSON" $ do
        it "with correct body should respond 200 and Set-Cookie Header" $ do
          request' <- parseRequest ("POST" `api` signIn)
          let request = setRequestBodyJSON
                        [aesonQQ|{ "name": "existed_account", "password": "password" }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          let setCookieHeader = cs $ head $ getResponseHeader hSetCookie response :: String
          setCookieHeader `shouldStartWith` "servant-auth-cookie"
          setCookieHeader `shouldEndWith` "Path=/; HttpOnly; Secure; SameSite=Strict"
          getResponseBody response `shouldBe` [aesonQQ|{ "message": null
                                                       , "payload": null
                                                       , "status": "Ok"
                                                       }|]
          getResponseStatusCode response `shouldBe` 200


        it "with not exist account name should respond 400" $ do
          request' <- parseRequest ("POST" `api` signIn)
          let request = setRequestBodyJSON
                        [aesonQQ|{ "name": "not_existed_account", "password": "password" }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|{ "message": "account not exist"
                                                       , "payload": null
                                                       , "status": "Error"
                                                       }|]
          getResponseStatusCode response `shouldBe` 400

        it "with incorrect password should respond 400" $ do
          request' <- parseRequest ("POST" `api` signIn)
          let request = setRequestBodyJSON
                        [aesonQQ|{ "name": "existed_account", "password": "bad_password" }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|{ "message": "password incorrect"
                                                       , "payload": null
                                                       , "status": "Error"
                                                       }|]
          getResponseStatusCode response `shouldBe` 400

        it "with missing name body should respond 400" $ do
          request' <- parseRequest ("POST" `api` signIn)
          let request = setRequestBodyJSON
                        [aesonQQ|{ "password": "bad_password" }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $: key \"name\" not found"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

        it "with missing password body should respond 400" $ do
          request' <- parseRequest ("POST" `api` signIn)
          let request = setRequestBodyJSON
                        [aesonQQ|{ "name": "existed_account" }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $: key \"password\" not found"
            , "payload": null
            , "status": "Error"
            }
          |]

        it "with empty should respond 400" $ do
          request' <- parseRequest ("POST" `api` signIn)
          let request = setRequestBodyJSON
                        [aesonQQ|{ }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $: key \"name\" not found"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

    let signOut = "/auth/sign_out"
    describe ("POST " <> signOut) $ do
      context "JSON" $ do
        it "with correct auth cookie should responds with 200 and Set-Cookie Header" $ do
          signInRequest' <- parseRequest ("POST" `api` signIn)
          let signInRequest = setRequestBodyJSON
                        [aesonQQ|{ "name": "existed_account", "password": "password" }|]
                        signInRequest'
          signInResponse <- httpJSON signInRequest :: IO (Response Value)
          let signInsetCookieHeader = cs $ head $ getResponseHeader hSetCookie signInResponse :: String
          let cookieValue = "servant-auth-cookie=" <> (cs $ setCookieValue $ parseSetCookie $ cs signInsetCookieHeader :: String)

          request' <- parseRequest ("POST" `api` signOut)
          let request = setRequestBodyJSON [aesonQQ|{ }|] $
                        addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          let setCookieHeader = cs $ head $ getResponseHeader hSetCookie response :: String
          setCookieHeader `shouldBe` "servant-auth-cookie=; Path=/; Max-Age=-1; HttpOnly; Secure; SameSite=Strict"

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": null
            , "payload": null
            , "status": "Ok"
            }
          |]
          getResponseStatusCode response `shouldBe` 200

        it "without auth cookie should responds with 401" $ do
          request <- parseRequest ("POST" `api` signOut)
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "haven't sign in"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 401

        it "with invalid auth cookie should responds with 401" $ do
          signInRequest' <- parseRequest ("POST" `api` signIn)
          let signInRequest = setRequestBodyJSON
                              [aesonQQ|{ "name": "existed_account", "password": "password" }|]
                              signInRequest'
          signInResponse <- httpJSON signInRequest :: IO (Response Value)
          let setCookieHeader = cs $ head $ getResponseHeader hSetCookie signInResponse :: String
          let cookieValue = "servant-auth-cookie=invalid" <> (cs $ setCookieValue $ parseSetCookie $ cs setCookieHeader :: String)

          request' <- parseRequest ("POST" `api` signOut)
          let request = addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "session key corrupted"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 401
          getResponseHeader hSetCookie response `shouldBe` ["servant-auth-cookie=; Path=/; Max-Age=-1; HttpOnly; Secure; SameSite=Strict"]

createTestAndExistedAccount :: IO ()
createTestAndExistedAccount = do
  loadFile defaultConfig
  pool <- getDbPool
  _ <- liftIO $ use pool $ deleteAccountSession "existed_account"
  Right _ <- liftIO $ use pool $ deleteAccountSession "test_account"
  _ <- liftIO createTestAccount
  return ()

deleteTestAccount :: IO ()
deleteTestAccount = do
  loadFile defaultConfig
  pool <- getDbPool
  Right _ <- liftIO $ use pool $ deleteAccountSession "test_account"
  return ()

deleteTestAndExistedAccount :: IO ()
deleteTestAndExistedAccount = do
  loadFile defaultConfig
  pool <- getDbPool
  Right _ <- liftIO $ use pool $ deleteAccountSession "existed_account"
  Right _ <- liftIO $ use pool $ deleteAccountSession "test_account"
  return ()
