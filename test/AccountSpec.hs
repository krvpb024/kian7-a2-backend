{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module AccountSpec (accountSpec) where

import           Control.Monad.IO.Class    (MonadIO (liftIO))
import           Data.Aeson                (KeyValue ((.=)), Value (Null),
                                            object)
import           Data.Aeson.QQ.Simple      (aesonQQ)
import           Data.Maybe                (fromJust, isJust, isNothing)
import           Data.String.Conversions   (cs)
import           Db.Session                (checkAccountExistSession)
import           Hasql.Pool                (use)
import           Lib                       (getDbPool)
import           Network.HTTP.Simple       (Response, addRequestHeader,
                                            getResponseBody, getResponseHeader,
                                            getResponseStatusCode, httpJSON,
                                            parseRequest, setRequestBodyJSON)
import           Network.HTTP.Types.Header (hCookie, hSetCookie)
import           SpecUtils                 (api, createAccount,
                                            createTestAccount, deleteAccount,
                                            deleteTestAccount,
                                            deleteTestAccountProfileIfExist,
                                            signInTestAccount)
import           Test.Hspec                (SpecWith, after_, before, context,
                                            describe, it, shouldBe,
                                            shouldSatisfy)

account :: String
account = "/account"

accountSpec :: SpecWith ()
accountSpec =
  after_ (deleteTestAccount >> deleteAccount "existed_account_modified" >> deleteTestAccountProfileIfExist) $
  before (createTestAccount >> signInTestAccount) $ do
    describe ("GET " <> account) $ do
      context "Ok" $ do
        it "with correct cookie should respond 200" $ \cookieValue -> do
          request' <- parseRequest ("GET" `api` account)
          let request = addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          pool <- getDbPool
          Right maybeAccount <- liftIO $ use pool (checkAccountExistSession "existed_account")
          maybeAccount `shouldSatisfy` isJust
          let (accId, accName, _) = fromJust maybeAccount
          getResponseBody response `shouldBe`
            object [ "message" .= Null
                   , "payload" .= object [ "id" .=  accId
                                         , "name" .= accName
                                         ]
                   , "status" .= ("Ok" :: String)
                   ]
          getResponseStatusCode response `shouldBe` 200
      context "Error" $ do
        it "without auth cookie should respond 401" $ \_ -> do
          request <- parseRequest ("GET" `api` account)
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "haven't sign in"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 401

    let accountName = account <> "/name"
    describe ("PATCH " <> accountName) $ do
      context "JSON" $ do
        context "Ok" $ do
          it "with correct body should respond 200" $ \cookieValue -> do
            request' <- parseRequest ("PATCH" `api` accountName)
            let request = setRequestBodyJSON [aesonQQ|{ "name": "existed_account_modified"
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200

            pool <- getDbPool
            Right maybeAccount <- liftIO $ use pool (checkAccountExistSession "existed_account_modified")

            maybeAccount `shouldSatisfy` isJust
            fromJust maybeAccount `shouldSatisfy` (\(_, n, _) -> n == "existed_account_modified")
        context "Error" $ do
          it "without auth cookie should respond 401" $ \_ -> do
            request' <- parseRequest ("PATCH" `api` accountName)
            let request = setRequestBodyJSON [aesonQQ|{ "name": "existed_account_modified"
                                                      }|]
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't sign in"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 401

            pool <- getDbPool
            Right maybeAccount <- liftIO $ use pool (checkAccountExistSession "existed_account_modified")
            maybeAccount `shouldSatisfy` isNothing

            Right maybeOriginAccount <- liftIO $ use pool (checkAccountExistSession "existed_account")
            maybeOriginAccount `shouldSatisfy` isJust
            fromJust maybeOriginAccount `shouldSatisfy` (\(_, n, _) -> n == "existed_account")

          it "with missing name body should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PATCH" `api` accountName)
            let request = setRequestBodyJSON [aesonQQ|{}|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $: key \"name\" not found"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400

            pool <- getDbPool
            Right maybeAccount <- liftIO $ use pool (checkAccountExistSession "existed_account_modified")
            maybeAccount `shouldSatisfy` isNothing

            Right maybeOriginAccount <- liftIO $ use pool (checkAccountExistSession "existed_account")
            maybeOriginAccount `shouldSatisfy` isJust
            fromJust maybeOriginAccount `shouldSatisfy` (\(_, n, _) -> n == "existed_account")

          it "with null name body should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PATCH" `api` accountName)
            let request = setRequestBodyJSON [aesonQQ|{ "name": null
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $.name: parsing Lazy Text failed, expected String, but encountered Null"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400

            pool <- getDbPool
            Right maybeAccount <- liftIO $ use pool (checkAccountExistSession "existed_account_modified")
            maybeAccount `shouldSatisfy` isNothing

            Right maybeOriginAccount <- liftIO $ use pool (checkAccountExistSession "existed_account")
            maybeOriginAccount `shouldSatisfy` isJust
            fromJust maybeOriginAccount `shouldSatisfy` (\(_, n, _) -> n == "existed_account")

          it "with existed name body should respond 400" $ \cookieValue -> do
            _ <- liftIO $ createAccount "existed_account_for_test" "password"
            request' <- parseRequest ("PATCH" `api` accountName)
            let request = setRequestBodyJSON [aesonQQ|{ "name": "existed_account_for_test"
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
            { "message": "account has been taken"
            , "payload": null
            , "status": "Error"
            }
            |]
            getResponseStatusCode response `shouldBe` 400

            pool <- getDbPool
            Right maybeAccount <- liftIO $ use pool (checkAccountExistSession "existed_account_modified")
            maybeAccount `shouldSatisfy` isNothing

            Right maybeOriginAccount <- liftIO $ use pool (checkAccountExistSession "existed_account")
            maybeOriginAccount `shouldSatisfy` isJust
            fromJust maybeOriginAccount `shouldSatisfy` (\(_, n, _) -> n == "existed_account")

    let accountPassword = account <> "/password"
    describe ("PATCH " <> accountPassword) $ do
      context "JSON" $ do
        context "Ok" $ do
          it "with correct body should respond 200" $ \cookieValue -> do
            request' <- parseRequest ("PATCH" `api` accountPassword)
            let request = setRequestBodyJSON [aesonQQ|{ "passwordOld": "password"
                                                      , "passwordNew": "new_password"
                                                      , "passwordNewConfirm": "new_password"
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200

            signInRequest' <- parseRequest ("POST" `api` "/auth/sign_in")
            let signInRequest = setRequestBodyJSON
                                [aesonQQ|{ "name": "existed_account"
                                         , "password": "new_password"
                                         }
                                |]
                                signInRequest'
            signInResponse <- httpJSON signInRequest :: IO (Response Value)

            getResponseBody signInResponse `shouldBe` [aesonQQ|{ "message": null
                                                               , "payload": null
                                                               , "status": "Ok"
                                                               }|]
            getResponseStatusCode signInResponse `shouldBe` 200

        context "Error" $ do
          it "without auth cookie should respond 401" $ \_ -> do
            request' <- parseRequest ("PATCH" `api` accountPassword)
            let request = setRequestBodyJSON [aesonQQ|{ "passwordOld": "password"
                                                      , "passwordNew": "new_password"
                                                      , "passwordNewConfirm": "new_password"
                                                      }|]
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't sign in"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 401

            pool <- getDbPool
            Right maybeAccount <- liftIO $ use pool (checkAccountExistSession "existed_account_modified")
            maybeAccount `shouldSatisfy` isNothing

            Right maybeOriginAccount <- liftIO $ use pool (checkAccountExistSession "existed_account")
            maybeOriginAccount `shouldSatisfy` isJust
            fromJust maybeOriginAccount `shouldSatisfy` (\(_, n, _) -> n == "existed_account")

          it "with password confirm not pass should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PATCH" `api` accountPassword)
            let request = setRequestBodyJSON
                          [aesonQQ|{ "passwordOld": "password"
                                   , "passwordNew": "new_password"
                                   , "passwordNewConfirm": "invalid_new_password_confirm"
                                   }
                          |] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "password confirm not pass"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400

            signInRequest' <- parseRequest ("POST" `api` "/auth/sign_in")
            let signInRequest = setRequestBodyJSON
                                [aesonQQ|{ "name": "existed_account"
                                         , "password": "password"
                                         }
                                |]
                                signInRequest'
            signInResponse <- httpJSON signInRequest :: IO (Response Value)

            getResponseBody signInResponse `shouldBe` [aesonQQ|{ "message": null
                                                               , "payload": null
                                                               , "status": "Ok"
                                                               }|]
            getResponseStatusCode signInResponse `shouldBe` 200

          it "with missing passwordOld body should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PATCH" `api` accountPassword)
            let request = setRequestBodyJSON
                          [aesonQQ|{ "passwordNew": "new_password"
                                   , "passwordNewConfirm": "new_password"
                                   }
                          |] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $: key \"passwordOld\" not found"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400

            signInRequest' <- parseRequest ("POST" `api` "/auth/sign_in")
            let signInRequest = setRequestBodyJSON
                                [aesonQQ|{ "name": "existed_account"
                                         , "password": "password"
                                         }
                                |]
                                signInRequest'
            signInResponse <- httpJSON signInRequest :: IO (Response Value)

            getResponseBody signInResponse `shouldBe` [aesonQQ|{ "message": null
                                                               , "payload": null
                                                               , "status": "Ok"
                                                               }|]
            getResponseStatusCode signInResponse `shouldBe` 200

          it "with null passwordOld body should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PATCH" `api` accountPassword)
            let request = setRequestBodyJSON
                          [aesonQQ|{ "passwordOld": null
                                   , "passwordNew": "new_password"
                                   , "passwordNewConfirm": "new_password"
                                   }
                          |] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $.passwordOld: parsing Lazy Text failed, expected String, but encountered Null"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400

            signInRequest' <- parseRequest ("POST" `api` "/auth/sign_in")
            let signInRequest = setRequestBodyJSON
                                [aesonQQ|{ "name": "existed_account"
                                         , "password": "password"
                                         }
                                |]
                                signInRequest'
            signInResponse <- httpJSON signInRequest :: IO (Response Value)

            getResponseBody signInResponse `shouldBe` [aesonQQ|{ "message": null
                                                               , "payload": null
                                                               , "status": "Ok"
                                                               }|]
            getResponseStatusCode signInResponse `shouldBe` 200

          it "with missing passwordNew body should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PATCH" `api` accountPassword)
            let request = setRequestBodyJSON
                          [aesonQQ|{ "passwordOld": "new_password"
                                   , "passwordNewConfirm": "new_password"
                                   }
                          |] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $: key \"passwordNew\" not found"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400

            signInRequest' <- parseRequest ("POST" `api` "/auth/sign_in")
            let signInRequest = setRequestBodyJSON
                                [aesonQQ|{ "name": "existed_account"
                                         , "password": "password"
                                         }
                                |]
                                signInRequest'
            signInResponse <- httpJSON signInRequest :: IO (Response Value)

            getResponseBody signInResponse `shouldBe` [aesonQQ|{ "message": null
                                                               , "payload": null
                                                               , "status": "Ok"
                                                               }|]
            getResponseStatusCode signInResponse `shouldBe` 200

          it "with null passwordNew body should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PATCH" `api` accountPassword)
            let request = setRequestBodyJSON
                          [aesonQQ|{ "passwordOld": "password"
                                   , "passwordNew": null
                                   , "passwordNewConfirm": "new_password"
                                   }
                          |] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $.passwordNew: parsing Lazy Text failed, expected String, but encountered Null"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400

            signInRequest' <- parseRequest ("POST" `api` "/auth/sign_in")
            let signInRequest = setRequestBodyJSON
                                [aesonQQ|{ "name": "existed_account"
                                         , "password": "password"
                                         }
                                |]
                                signInRequest'
            signInResponse <- httpJSON signInRequest :: IO (Response Value)

            getResponseBody signInResponse `shouldBe` [aesonQQ|{ "message": null
                                                               , "payload": null
                                                               , "status": "Ok"
                                                               }|]
            getResponseStatusCode signInResponse `shouldBe` 200

          it "with missing passwordNewConfirm body should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PATCH" `api` accountPassword)
            let request = setRequestBodyJSON
                          [aesonQQ|{ "passwordOld": "password"
                                   , "passwordNew": "new_password"
                                   }
                          |] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $: key \"passwordNewConfirm\" not found"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400

            signInRequest' <- parseRequest ("POST" `api` "/auth/sign_in")
            let signInRequest = setRequestBodyJSON
                                [aesonQQ|{ "name": "existed_account"
                                         , "password": "password"
                                         }
                                |]
                                signInRequest'
            signInResponse <- httpJSON signInRequest :: IO (Response Value)

            getResponseBody signInResponse `shouldBe` [aesonQQ|{ "message": null
                                                               , "payload": null
                                                               , "status": "Ok"
                                                               }|]
            getResponseStatusCode signInResponse `shouldBe` 200

          it "with null passwordConfirm body should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PATCH" `api` accountPassword)
            let request = setRequestBodyJSON
                          [aesonQQ|{ "passwordOld": "password"
                                   , "passwordNew": "new_password"
                                   , "passwordNewConfirm": null
                                   }
                          |] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $.passwordNewConfirm: parsing Lazy Text failed, expected String, but encountered Null"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400

            signInRequest' <- parseRequest ("POST" `api` "/auth/sign_in")
            let signInRequest = setRequestBodyJSON
                                [aesonQQ|{ "name": "existed_account"
                                         , "password": "password"
                                         }
                                |]
                                signInRequest'
            signInResponse <- httpJSON signInRequest :: IO (Response Value)

            getResponseBody signInResponse `shouldBe` [aesonQQ|{ "message": null
                                                               , "payload": null
                                                               , "status": "Ok"
                                                               }|]
            getResponseStatusCode signInResponse `shouldBe` 200

    describe ("DELETE " <> account) $ do
      context "JSON" $ do
        context "Ok" $ do
          it "with correct body should remove cookie and respond 200" $ \cookieValue -> do
            request' <- parseRequest ("DELETE" `api` account)
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200
            let cookieHeader = cs $ head $ getResponseHeader hSetCookie response :: String
            cookieHeader `shouldBe` "servant-auth-cookie=; Path=/; Max-Age=-1; HttpOnly; Secure; SameSite=Strict"


            signInRequest' <- parseRequest ("POST" `api` "/auth/sign_in")
            let signInRequest = setRequestBodyJSON
                                [aesonQQ|{ "name": "existed_account"
                                         , "password": "new_password"
                                         }
                                |]
                                signInRequest'
            signInResponse <- httpJSON signInRequest :: IO (Response Value)

            getResponseBody signInResponse `shouldBe` [aesonQQ|{ "message": "account not exist"
                                                               , "payload": null
                                                               , "status": "Error"
                                                               }|]
            getResponseStatusCode signInResponse `shouldBe` 400

            pool <- getDbPool
            Right maybeAccount <- liftIO $ use pool (checkAccountExistSession "existed_account")
            maybeAccount `shouldSatisfy` isNothing

        context "Error" $ do
          it "without auth cookie should respond 401" $ \_ -> do
            request <- parseRequest ("DELETE" `api` account)
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't sign in"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 401

            pool <- getDbPool
            Right maybeAccount <- liftIO $ use pool (checkAccountExistSession "existed_account")
            maybeAccount `shouldSatisfy` isJust
