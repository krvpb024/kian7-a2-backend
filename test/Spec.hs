module Main (main) where

import           AccountSpec      (accountSpec)
import           AuthSpec         (authSpec)
import           HealthRecordSpec (healthRecordSpec)
import           ProfileSpec      (profileSpec)
import           Test.Hspec       (describe, hspec)

main :: IO ()
main = hspec $ do
  describe "Auth"         authSpec
  describe "Profile"      profileSpec
  describe "HealthRecord" healthRecordSpec
  describe "Account"      accountSpec
