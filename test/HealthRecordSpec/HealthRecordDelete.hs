{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module HealthRecordSpec.HealthRecordDelete (healthRecordDeleteSpec) where

import           Data.Aeson                (Value)
import           Data.Aeson.QQ.Simple      (aesonQQ)
import           Data.Either.Extra         (fromRight')
import           Data.String.Conversions   (cs)
import           Data.Vector               (fromList)
import           Network.HTTP.Simple       (Response, addRequestHeader,
                                            getResponseBody,
                                            getResponseStatusCode, httpJSON,
                                            parseRequest)
import           Network.HTTP.Types.Header (hCookie)
import           SpecUtils                 (api, createTestAccount,
                                            createTestHealthRecordRows,
                                            createTestProfile,
                                            deleteTestAccountAll,
                                            deleteTestAccountHealthRecordIfExist,
                                            deleteTestAccountProfileIfExist,
                                            getTestHealthRecord,
                                            healthRecordData2,
                                            healthRecordData3,
                                            healthRecordData4,
                                            healthRecordData5,
                                            signInTestAccount)
import           Test.Hspec                (SpecWith, afterAll_, after_, before,
                                            beforeAll_, context, describe, it,
                                            shouldBe)

healthRecord :: String
healthRecord = "/health_record"

healthRecordDeleteSpec :: SpecWith ()
healthRecordDeleteSpec =
  afterAll_ deleteTestAccountAll $
  beforeAll_ createTestAccount $
  after_ deleteTestAccountHealthRecordIfExist $
  before (signInTestAccount >>= createTestProfile >>= createTestHealthRecordRows) $ do
    describe ("DELETE " <> healthRecord) $ do
      context "JSON" $ do
        context "Ok" $ do
          it "with correct body should remove health record by day file and respond 200" $ \cookieValue -> do
            request' <- parseRequest ("DELETE" `api` healthRecord <> "/2000-01-01")
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200

            ioResult <- getTestHealthRecord
            snd (fromRight' ioResult) `shouldBe` fromList [ healthRecordData2
                                                          , healthRecordData3
                                                          , healthRecordData4
                                                          , healthRecordData5
                                                          ]

            request2' <- parseRequest ("DELETE" `api` healthRecord <> "/2000-01-03")
            let request2 = addRequestHeader hCookie (cs cookieValue)
                          request2'
            response2 <- httpJSON request2 :: IO (Response Value)

            getResponseBody response2 `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response2 `shouldBe` 200

            ioResult2 <- getTestHealthRecord
            snd (fromRight' ioResult2) `shouldBe` fromList [ healthRecordData2
                                                           , healthRecordData4
                                                           , healthRecordData5
                                                           ]

        context "Error" $ do
          it "without auth cookie should respond 401" $ \_ -> do
            request' <- parseRequest ("DELETE" `api` healthRecord <> "/2000-01-01")
            response <- httpJSON request' :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't sign in"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 401

          it "with auth cookie and haven't create profile should respond 400" $ \cookieValue -> do
            deleteTestAccountProfileIfExist
            request' <- parseRequest ("DELETE" `api` healthRecord <> "/2000-01-01")
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't create profile"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400

          it "with not existed day should respond 404" $ \cookieValue -> do
            request' <- parseRequest ("DELETE" `api` healthRecord <> "/2005-01-01")
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "health record on 2005-01-01 not found"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 404
