{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module HealthRecordSpec.HealthRecordsGet (healthRecordsGetSpec) where

import           Data.Aeson                (Value)
import           Data.Aeson.QQ.Simple      (aesonQQ)
import           Data.String.Conversions   (cs)
import           Network.HTTP.Simple       (Response, addRequestHeader,
                                            getResponseBody,
                                            getResponseStatusCode, httpJSON,
                                            parseRequest)
import           Network.HTTP.Types.Header (hCookie)
import           SpecUtils                 (api, createTestAccount,
                                            createTestHealthRecordRows,
                                            createTestProfile,
                                            deleteTestAccountAll,
                                            deleteTestAccountHealthRecordIfExist,
                                            deleteTestAccountProfileIfExist,
                                            signInTestAccount)
import           Test.Hspec                (SpecWith, afterAll_, after_, before,
                                            beforeAll_, context, describe, it,
                                            shouldBe)

healthRecords :: String
healthRecords = "/health_records"

healthRecordsGetSpec :: SpecWith ()
healthRecordsGetSpec =
  afterAll_ deleteTestAccountAll $
  beforeAll_ createTestAccount $
  after_ deleteTestAccountHealthRecordIfExist $
  before (signInTestAccount >>= createTestProfile >>= createTestHealthRecordRows) $ do
    describe ("GET " <> healthRecords) $ do
      context "JSON" $ do
        context "Ok" $ do
          it "without query params should respond 200" $ \cookieValue -> do
            request' <- parseRequest ("GET" `api` healthRecords)
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload":
                [
                  {
                    "date": "2000-01-05"
                  , "heightCm": 184
                  , "weightKg": 74
                  , "bodyFatPercentage": 19
                  , "waistlineCm": 26
                  }
                , {
                    "date": "2000-01-04"
                  , "heightCm": 183
                  , "weightKg": 73
                  , "bodyFatPercentage": 18
                  , "waistlineCm": 25
                  }
                , {
                    "date": "2000-01-03"
                  , "heightCm": 182
                  , "weightKg": 72
                  , "bodyFatPercentage": 17
                  , "waistlineCm": 24
                  }
                , {
                    "date": "2000-01-02"
                  , "heightCm": 181
                  , "weightKg": 71
                  , "bodyFatPercentage": 16
                  , "waistlineCm": 23
                  }
                , {
                    "date": "2000-01-01"
                  , "heightCm": 180
                  , "weightKg": 70
                  , "bodyFatPercentage": 15
                  , "waistlineCm": 22
                  }
                ]
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200

          it "without health record should respond 200" $ \cookieValue -> do
            deleteTestAccountHealthRecordIfExist
            request' <- parseRequest ("GET" `api` healthRecords)
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": []
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200

          it "with query params from should respond 200" $ \cookieValue -> do
            request' <- parseRequest ("GET" `api` (healthRecords <> "?from=2000-01-03"))
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload":
                [
                  { "date": "2000-01-05"
                  , "heightCm": 184
                  , "weightKg": 74
                  , "bodyFatPercentage": 19
                  , "waistlineCm": 26
                  }
                , { "date": "2000-01-04"
                  , "heightCm": 183
                  , "weightKg": 73
                  , "bodyFatPercentage": 18
                  , "waistlineCm": 25
                  }
                , { "date": "2000-01-03"
                  , "heightCm": 182
                  , "weightKg": 72
                  , "bodyFatPercentage": 17
                  , "waistlineCm": 24
                  }
                ]
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200

          it "with query params invalid from should respond 200" $ \cookieValue -> do

            request' <- parseRequest ("GET" `api` healthRecords <> "?from=334")
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload":
                [
                  { "date": "2000-01-05"
                  , "heightCm": 184
                  , "weightKg": 74
                  , "bodyFatPercentage": 19
                  , "waistlineCm": 26
                  }
                , { "date": "2000-01-04"
                  , "heightCm": 183
                  , "weightKg": 73
                  , "bodyFatPercentage": 18
                  , "waistlineCm": 25
                  }
                , { "date": "2000-01-03"
                  , "heightCm": 182
                  , "weightKg": 72
                  , "bodyFatPercentage": 17
                  , "waistlineCm": 24
                  }
                , { "date": "2000-01-02"
                  , "heightCm": 181
                  , "weightKg": 71
                  , "bodyFatPercentage": 16
                  , "waistlineCm": 23
                  }
                , { "date": "2000-01-01"
                  , "heightCm": 180
                  , "weightKg": 70
                  , "bodyFatPercentage": 15
                  , "waistlineCm": 22
                  }
                ]
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200

          it "with query params from and empty to should respond 200" $ \cookieValue -> do
            request' <- parseRequest ("GET" `api` (healthRecords <> "?from=2000-01-02&to="))
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload":
                [
                  { "date": "2000-01-05"
                  , "heightCm": 184
                  , "weightKg": 74
                  , "bodyFatPercentage": 19
                  , "waistlineCm": 26
                  }
                , { "date": "2000-01-04"
                  , "heightCm": 183
                  , "weightKg": 73
                  , "bodyFatPercentage": 18
                  , "waistlineCm": 25
                  }
                , { "date": "2000-01-03"
                  , "heightCm": 182
                  , "weightKg": 72
                  , "bodyFatPercentage": 17
                  , "waistlineCm": 24
                  }
                , { "date": "2000-01-02"
                  , "heightCm": 181
                  , "weightKg": 71
                  , "bodyFatPercentage": 16
                  , "waistlineCm": 23
                  }
                ]
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200

          it "with query params from and invalid to should respond 200" $ \cookieValue -> do
            request' <- parseRequest ("GET" `api` (healthRecords <> "?from=2000-01-02&to=null"))
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload":
                [
                  { "date": "2000-01-05"
                  , "heightCm": 184
                  , "weightKg": 74
                  , "bodyFatPercentage": 19
                  , "waistlineCm": 26
                  }
                , { "date": "2000-01-04"
                  , "heightCm": 183
                  , "weightKg": 73
                  , "bodyFatPercentage": 18
                  , "waistlineCm": 25
                  }
                , { "date": "2000-01-03"
                  , "heightCm": 182
                  , "weightKg": 72
                  , "bodyFatPercentage": 17
                  , "waistlineCm": 24
                  }
                ,  { "date": "2000-01-02"
                  , "heightCm": 181
                  , "weightKg": 71
                  , "bodyFatPercentage": 16
                  , "waistlineCm": 23
                  }
                ]
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200

          it "with query params to should respond 200" $ \cookieValue -> do
            request' <- parseRequest ("GET" `api` (healthRecords <> "?to=2000-01-03"))
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload":
                [
                  { "date": "2000-01-03"
                  , "heightCm": 182
                  , "weightKg": 72
                  , "bodyFatPercentage": 17
                  , "waistlineCm": 24
                  }
                , { "date": "2000-01-02"
                  , "heightCm": 181
                  , "weightKg": 71
                  , "bodyFatPercentage": 16
                  , "waistlineCm": 23
                  }
                ,  { "date": "2000-01-01"
                  , "heightCm": 180
                  , "weightKg": 70
                  , "bodyFatPercentage": 15
                  , "waistlineCm": 22
                  }
                ]
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200

          it "with query params invalid to should respond 200" $ \cookieValue -> do

            request' <- parseRequest ("GET" `api` healthRecords <> "?to=null")
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload":
                [
                  { "date": "2000-01-05"
                  , "heightCm": 184
                  , "weightKg": 74
                  , "bodyFatPercentage": 19
                  , "waistlineCm": 26
                  }
                , { "date": "2000-01-04"
                  , "heightCm": 183
                  , "weightKg": 73
                  , "bodyFatPercentage": 18
                  , "waistlineCm": 25
                  }
                , { "date": "2000-01-03"
                  , "heightCm": 182
                  , "weightKg": 72
                  , "bodyFatPercentage": 17
                  , "waistlineCm": 24
                  }
                , { "date": "2000-01-02"
                  , "heightCm": 181
                  , "weightKg": 71
                  , "bodyFatPercentage": 16
                  , "waistlineCm": 23
                  }
                , { "date": "2000-01-01"
                  , "heightCm": 180
                  , "weightKg": 70
                  , "bodyFatPercentage": 15
                  , "waistlineCm": 22
                  }
                ]
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200

          it "with query params to and empty from should respond 200" $ \cookieValue -> do
            request' <- parseRequest ("GET" `api` (healthRecords <> "?from=&to=2000-01-02"))
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload":
                [
                  { "date": "2000-01-02"
                  , "heightCm": 181
                  , "weightKg": 71
                  , "bodyFatPercentage": 16
                  , "waistlineCm": 23
                  }
                , { "date": "2000-01-01"
                  , "heightCm": 180
                  , "weightKg": 70
                  , "bodyFatPercentage": 15
                  , "waistlineCm": 22
                  }
                ]
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200

          it "with query params to and invalid from should respond 200" $ \cookieValue -> do
            request' <- parseRequest ("GET" `api` (healthRecords <> "?from=334&to=2000-01-02"))
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload":
                [
                  { "date": "2000-01-02"
                  , "heightCm": 181
                  , "weightKg": 71
                  , "bodyFatPercentage": 16
                  , "waistlineCm": 23
                  }
                , { "date": "2000-01-01"
                  , "heightCm": 180
                  , "weightKg": 70
                  , "bodyFatPercentage": 15
                  , "waistlineCm": 22
                  }
                ]
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200

          it "with query params from and to should respond 200" $ \cookieValue -> do
            request' <- parseRequest ("GET" `api` (healthRecords <> "?from=2000-01-02&to=2000-01-04"))
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload":
                [
                  { "date": "2000-01-04"
                  , "heightCm": 183
                  , "weightKg": 73
                  , "bodyFatPercentage": 18
                  , "waistlineCm": 25
                  }
                , { "date": "2000-01-03"
                  , "heightCm": 182
                  , "weightKg": 72
                  , "bodyFatPercentage": 17
                  , "waistlineCm": 24
                  }
                , { "date": "2000-01-02"
                  , "heightCm": 181
                  , "weightKg": 71
                  , "bodyFatPercentage": 16
                  , "waistlineCm": 23
                  }
                ]
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200

          it "with query params invalid from and invalid to should respond 200" $ \cookieValue -> do

            request' <- parseRequest ("GET" `api` healthRecords <> "?from=334&to=null")
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload":
                [
                  { "date": "2000-01-05"
                  , "heightCm": 184
                  , "weightKg": 74
                  , "bodyFatPercentage": 19
                  , "waistlineCm": 26
                  }
                , { "date": "2000-01-04"
                  , "heightCm": 183
                  , "weightKg": 73
                  , "bodyFatPercentage": 18
                  , "waistlineCm": 25
                  }
                , { "date": "2000-01-03"
                  , "heightCm": 182
                  , "weightKg": 72
                  , "bodyFatPercentage": 17
                  , "waistlineCm": 24
                  }
                , { "date": "2000-01-02"
                  , "heightCm": 181
                  , "weightKg": 71
                  , "bodyFatPercentage": 16
                  , "waistlineCm": 23
                  }
                ,  { "date": "2000-01-01"
                  , "heightCm": 180
                  , "weightKg": 70
                  , "bodyFatPercentage": 15
                  , "waistlineCm": 22
                  }
                ]
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200
        context "Error" $ do
          it "without auth cookie should respond 401" $ \_ -> do
            request <- parseRequest ("GET" `api` healthRecords)
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't sign in"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 401

          it "with auth cookie and haven't create profile should respond 400" $ \cookieValue -> do
            deleteTestAccountProfileIfExist
            request' <- parseRequest ("GET" `api` healthRecords)
            let request = addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't create profile"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
