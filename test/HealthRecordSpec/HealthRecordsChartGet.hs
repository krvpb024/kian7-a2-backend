{-# LANGUAGE OverloadedStrings #-}

module HealthRecordSpec.HealthRecordsChartGet (healthRecordsChartGetSpec) where
import qualified Data.Map              as Map
import           Data.Range            (lbi, ubi, (+=*))
import           Data.Time             (fromGregorian)
import qualified Data.Vector           as V
import           Test.Hspec            (SpecWith, describe, it, shouldBe)
import           Types.Chart           (ChartValue (ChartValue),
                                        ChartValueData (ChartValueData),
                                        ChartValueDataInfo (ChartValueDataInfo),
                                        ceilingMinFloorMaxHealthRecordsValue,
                                        getAllValueByIndicator,
                                        getDropLineCoords,
                                        getMinMaxHealthRecordsValue,
                                        getPaddedHealthRecordTables,
                                        getRangeCoords, getSvgViewBox,
                                        getSvgWidth, getXAxesCoords,
                                        getXBaseCoords, getXMaxCoords,
                                        getYAxesCoordByValue, getYAxesCoords,
                                        getYAxesCoordsWithInfo, getYBaseCoords,
                                        getYMaxCoords, roundTo)
import           Types.HealthIndicator (HealthIndicator (Bmi, BodyFatPercentage, WaistlineCm, WeightKg))
import           Types.HealthRecord    (HealthRecordTable (HealthRecordTable))

healthRecordsChartGetSpec :: SpecWith ()
healthRecordsChartGetSpec = do
    describe "roundTo" $ do
      it "should round to specific digit" $ do
        roundTo 2 (3.1415 :: Double) `shouldBe` 3.14
        roundTo 1 (2.19   :: Double) `shouldBe` 2.2
        roundTo 2 (1      :: Double) `shouldBe` 1.00
        roundTo 0 (0.22   :: Double) `shouldBe` 0
        roundTo 0 (0.62   :: Double) `shouldBe` 1

    describe "getSvgWidth" $ do
      it "input data distance width should calculate whole svg width" $ do
        getSvgWidth 30 (Map.fromList [ (fromGregorian 2023 5 1, Nothing) ]) `shouldBe` 30
        getSvgWidth 30 (Map.fromList [ (fromGregorian 2023 5 1, Just $ HealthRecordTable (fromGregorian 2023 5 1) 184 76 (Just 30) (Just 20))
                                     , (fromGregorian 2023 5 2, Nothing)
                                     , (fromGregorian 2023 5 3, Just $ HealthRecordTable (fromGregorian 2023 5 3) 184 76 (Just 30) (Just 20))
                                     , (fromGregorian 2023 5 4, Nothing)
                                     ]) `shouldBe` 120
        getSvgWidth 50 (Map.fromList [ (fromGregorian 2023 5 1, Just $ HealthRecordTable (fromGregorian 2023 5 1) 184 76 (Just 30) (Just 20))
                                     , (fromGregorian 2023 5 2, Nothing)
                                     , (fromGregorian 2023 5 3, Just $ HealthRecordTable (fromGregorian 2023 5 3) 184 76 (Just 30) (Just 20))
                                     , (fromGregorian 2023 5 4, Nothing)
                                     ]) `shouldBe` 200
        getSvgWidth 100 (Map.fromList [ (fromGregorian 2023 5 1, Nothing)
                                      , (fromGregorian 2023 5 2, Nothing)
                                      , (fromGregorian 2023 5 3, Nothing)
                                      , (fromGregorian 2023 5 4, Nothing)
                                      , (fromGregorian 2023 5 5, Nothing)
                                      ]) `shouldBe` 500

    describe "getDropLineCoords" $ do
      it "input coordinates, coordinates and vector day coordinates tuple should return vector coordinates coordinates tuple" $ do
        getDropLineCoords (1, 2)
                          (3, 4)
                          V.empty
          `shouldBe` V.empty
        getDropLineCoords (1, 2)
                          (3, 4)
                          (V.fromList [ (fromGregorian 2023 5 1, (5, 6))
                                      , (fromGregorian 2023 5 2, (7, 8))
                                      , (fromGregorian 2023 5 3, (9, 10))
                                      ])
          `shouldBe` V.fromList [ ((5, 2), (5, 4))
                                , ((7, 2), (7, 4))
                                , ((9, 2), (9, 4))
                                ]
        getDropLineCoords (324,  16)
                          (4352, 5733)
                          (V.fromList [ (fromGregorian 2023 5 5,  (25, 345))
                                      , (fromGregorian 2023 5 7,  (36, 36))
                                      , (fromGregorian 2023 5 20, (3,  7657))
                                      ])
          `shouldBe` V.fromList [ ((25, 16), (25, 5733))
                                , ((36, 16), (36, 5733))
                                , ((3,  16), (3,  5733))
                                ]

    describe "getSvgViewBox" $ do
      it "input width, height and paddings should return view box coords" $ do
        getSvgViewBox 1600 900  (20, 30, 20, 30)  `shouldBe` ((-30,  -20), (1660, 940))
        getSvgViewBox 1400 300  (0,  15, 4,  50)  `shouldBe` ((-50,  0),   (1465, 304))
        getSvgViewBox 1234 345  (24, 86, 19, 92)  `shouldBe` ((-92,  -24), (1412, 388))
        getSvgViewBox 430  1000 (0,  1,  2,  3)   `shouldBe` ((-3,   0),   (434,  1002))
        getSvgViewBox 2169 1280 (4,  77, 85, 111) `shouldBe` ((-111, -4),  (2357, 1369))

    describe "getXBaseCoords" $ do
      it "input width and height should return base coords of x axis" $ do
        getXBaseCoords 1600 900  `shouldBe` (0, 900)
        getXBaseCoords 1400 300  `shouldBe` (0, 300)
        getXBaseCoords 1234 345  `shouldBe` (0, 345)
        getXBaseCoords 430  1000 `shouldBe` (0, 1000)
        getXBaseCoords 2169 1280 `shouldBe` (0, 1280)

    describe "getXMaxCoords" $ do
      it "input width and height should return max coords of x axis" $ do
        getXMaxCoords 1600 900  `shouldBe` (1600, 900)
        getXMaxCoords 1400 300  `shouldBe` (1400, 300)
        getXMaxCoords 1234 345  `shouldBe` (1234, 345)
        getXMaxCoords 430  1000 `shouldBe` (430,  1000)
        getXMaxCoords 2169 1280 `shouldBe` (2169, 1280)

    describe "getYBaseCoords" $ do
      it "input width and height should return base coords of y axis" $ do
        getYBaseCoords 1600 900  `shouldBe` (0, 900)
        getYBaseCoords 1400 300  `shouldBe` (0, 300)
        getYBaseCoords 1234 345  `shouldBe` (0, 345)
        getYBaseCoords 430  1000 `shouldBe` (0, 1000)
        getYBaseCoords 2169 1280 `shouldBe` (0, 1280)

    describe "getYMaxCoords" $ do
      it "input width and height should return max coords of y axis" $ do
        getYMaxCoords 1600 900  `shouldBe` (0, 0)
        getYMaxCoords 1400 300  `shouldBe` (0, 0)
        getYMaxCoords 1234 345  `shouldBe` (0, 0)
        getYMaxCoords 430  1000 `shouldBe` (0, 0)
        getYMaxCoords 2169 1280 `shouldBe` (0, 0)

    describe "getXAxesCoords" $ do
      it "input width, height and Vector of Days should return Vector of Days and Coords tuple" $ do
        getXAxesCoords 2169 1280 (V.fromList [])
          `shouldBe` V.fromList []
        getXAxesCoords 1600 900 (V.fromList [ fromGregorian 2023 5 1
                                            , fromGregorian 2023 5 2
                                            , fromGregorian 2023 5 3
                                            , fromGregorian 2023 5 4])
          `shouldBe` V.fromList [ (fromGregorian 2023 5 1, (0,            900))
                                , (fromGregorian 2023 5 2, (1 / 3 * 1600, 900))
                                , (fromGregorian 2023 5 3, (2 / 3 * 1600, 900))
                                , (fromGregorian 2023 5 4, (3 / 3 * 1600, 900))
                                ]
        getXAxesCoords 1400 300 (V.fromList [ fromGregorian 2023 5 1
                                            , fromGregorian 2023 6 2
                                            , fromGregorian 2023 7 3
                                            , fromGregorian 2023 8 4])
          `shouldBe` V.fromList [ (fromGregorian 2023 5 1, (0,            300))
                                , (fromGregorian 2023 6 2, (1 / 3 * 1400, 300))
                                , (fromGregorian 2023 7 3, (2 / 3 * 1400, 300))
                                , (fromGregorian 2023 8 4, (3 / 3 * 1400, 300))
                                ]
        getXAxesCoords 1234 345 (V.fromList [ fromGregorian 2023 5 4
                                            , fromGregorian 2023 6 3
                                            , fromGregorian 2023 7 2
                                            , fromGregorian 2023 8 1])
          `shouldBe` V.fromList [ (fromGregorian 2023 5 4, (0,            345))
                                , (fromGregorian 2023 6 3, (1 / 3 * 1234, 345))
                                , (fromGregorian 2023 7 2, (2 / 3 * 1234, 345))
                                , (fromGregorian 2023 8 1, (3 / 3 * 1234, 345))
                                ]
        getXAxesCoords 430 1000 (V.fromList [ fromGregorian 2022 5 4
                                            , fromGregorian 2023 8 1])
          `shouldBe` V.fromList [ (fromGregorian 2022 5 4, (0,       1000))
                                , (fromGregorian 2023 8 1, (1 * 430, 1000))
                                ]

    describe "ceilingMinFloorMaxHealthRecordsValue" $ do
      it "input two double tuple should return two double tuple with padding" $ do
        ceilingMinFloorMaxHealthRecordsValue (22,  33)  `shouldBe` (10, 50)
        ceilingMinFloorMaxHealthRecordsValue (0,   40)  `shouldBe` (0,  50)
        ceilingMinFloorMaxHealthRecordsValue (-20, 123) `shouldBe` (0,  140)
        ceilingMinFloorMaxHealthRecordsValue (9,   24)  `shouldBe` (0,  40)
        ceilingMinFloorMaxHealthRecordsValue (40,  50)  `shouldBe` (30, 60)

    describe "getMinMaxHealthRecordsValue" $ do
      it "input day, maybe health record map and maybe health indicator range should return min max value tuple" $ do
        getMinMaxHealthRecordsValue (Just (9 +=* 30))
                                    Map.empty

          `shouldBe` Nothing
        getMinMaxHealthRecordsValue (Just (9 +=* 30))
                                    (Map.fromList [ ( fromGregorian 2023 5 1, Just 30 )
                                                  , ( fromGregorian 2023 5 2, Just 25 )
                                                  , ( fromGregorian 2023 5 3, Nothing )
                                                  , ( fromGregorian 2023 5 4, Just 83 )
                                                  ])
          `shouldBe` Just (0, 100)
        getMinMaxHealthRecordsValue (Just (40 +=* 100))
                                    (Map.fromList [ ( fromGregorian 2023 5 1, Just 30 )
                                                  , ( fromGregorian 2023 5 2, Just 25 )
                                                  , ( fromGregorian 2023 5 3, Nothing )
                                                  , ( fromGregorian 2023 5 4, Just 83 )
                                                  ])
          `shouldBe` Just (10, 110)
        getMinMaxHealthRecordsValue Nothing
                                    (Map.fromList [ ( fromGregorian 2023 5 1, Just 30 )
                                                  , ( fromGregorian 2023 5 2, Just 25 )
                                                  , ( fromGregorian 2023 5 3, Nothing )
                                                  , ( fromGregorian 2023 5 4, Just 83 )
                                                  ])

          `shouldBe` Just (10, 100)
        getMinMaxHealthRecordsValue (Just (lbi 9))
                                    (Map.fromList [ ( fromGregorian 2023 5 1, Just 30 )
                                                  , ( fromGregorian 2023 5 2, Just 25 )
                                                  , ( fromGregorian 2023 5 3, Nothing )
                                                  , ( fromGregorian 2023 5 4, Just 83 )
                                                  ])
          `shouldBe` Just (0, 100)
        getMinMaxHealthRecordsValue (Just (lbi 40))
                                    (Map.fromList [ ( fromGregorian 2023 5 1, Just 30 )
                                                  , ( fromGregorian 2023 5 2, Just 25 )
                                                  , ( fromGregorian 2023 5 3, Nothing )
                                                  , ( fromGregorian 2023 5 4, Just 83 )
                                                  ])
          `shouldBe` Just (10, 100)
        getMinMaxHealthRecordsValue (Just (ubi 193))
                                    (Map.fromList [ ( fromGregorian 2023 5 1, Just 30 )
                                                  , ( fromGregorian 2023 5 2, Just 25 )
                                                  , ( fromGregorian 2023 5 3, Nothing )
                                                  , ( fromGregorian 2023 5 4, Just 83 )
                                                  ])
          `shouldBe` Just (10, 210)
        getMinMaxHealthRecordsValue (Just (ubi 65))
                                    (Map.fromList [ ( fromGregorian 2023 5 1, Just 30 )
                                                  , ( fromGregorian 2023 5 2, Just 25 )
                                                  , ( fromGregorian 2023 5 3, Nothing )
                                                  , ( fromGregorian 2023 5 4, Just 83 )
                                                  ])
          `shouldBe` Just (10, 100)

    describe "getPaddedHealthRecordTables" $ do
      it "input vector health record table should return map day maybe health record table" $ do
        getPaddedHealthRecordTables Nothing Nothing V.empty `shouldBe` Map.empty
        getPaddedHealthRecordTables (Just (fromGregorian 2023 4 29))
                                    (Just (fromGregorian 2023 5 10))
                                    V.empty `shouldBe` Map.empty
        getPaddedHealthRecordTables Nothing Nothing (V.fromList [ HealthRecordTable (fromGregorian 2023 5 1) 184 76 (Just 30) (Just 20)
                                                                , HealthRecordTable (fromGregorian 2023 5 3) 184 74 (Just 21)  Nothing
                                                                , HealthRecordTable (fromGregorian 2023 5 4) 184 72 (Just 25) (Just 26)
                                                                , HealthRecordTable (fromGregorian 2023 5 7) 184 70  Nothing  (Just 23)
                                                                ])
          `shouldBe` Map.fromList [ ( fromGregorian 2023 5 1
                                    , Just (HealthRecordTable (fromGregorian 2023 5 1) 184 76 (Just 30) (Just 20))
                                    )
                                  , ( fromGregorian 2023 5 2, Nothing )
                                  , ( fromGregorian 2023 5 3
                                    , Just (HealthRecordTable (fromGregorian 2023 5 3) 184 74 (Just 21)  Nothing)
                                    )
                                  , ( fromGregorian 2023 5 4
                                    , Just (HealthRecordTable (fromGregorian 2023 5 4) 184 72 (Just 25) (Just 26))
                                    )
                                  , ( fromGregorian 2023 5 5, Nothing )
                                  , ( fromGregorian 2023 5 6, Nothing )
                                  , ( fromGregorian 2023 5 7
                                    , Just (HealthRecordTable (fromGregorian 2023 5 7) 184 70  Nothing  (Just 23))
                                    )
                                  ]
        getPaddedHealthRecordTables (Just (fromGregorian 2023 4 29))
                                    Nothing
                                    (V.fromList [ HealthRecordTable (fromGregorian 2023 5 1) 184 76 (Just 30) (Just 20)
                                                , HealthRecordTable (fromGregorian 2023 5 3) 184 74 (Just 21)  Nothing
                                                , HealthRecordTable (fromGregorian 2023 5 4) 184 72 (Just 25) (Just 26)
                                                , HealthRecordTable (fromGregorian 2023 5 7) 184 70  Nothing  (Just 23)
                                                ])
          `shouldBe` Map.fromList [ ( fromGregorian 2023 4 29,  Nothing )
                                  , ( fromGregorian 2023 4 30,  Nothing )
                                  , ( fromGregorian 2023 5 1
                                    , Just (HealthRecordTable (fromGregorian 2023 5 1) 184 76 (Just 30) (Just 20))
                                    )
                                  , ( fromGregorian 2023 5 2,  Nothing )
                                  , ( fromGregorian 2023 5 3
                                    , Just (HealthRecordTable (fromGregorian 2023 5 3) 184 74 (Just 21)  Nothing)
                                    )
                                  , ( fromGregorian 2023 5 4
                                    , Just (HealthRecordTable (fromGregorian 2023 5 4) 184 72 (Just 25) (Just 26))
                                    )
                                  , ( fromGregorian 2023 5 5,  Nothing )
                                  , ( fromGregorian 2023 5 6,  Nothing )
                                  , ( fromGregorian 2023 5 7
                                    , Just (HealthRecordTable (fromGregorian 2023 5 7) 184 70  Nothing  (Just 23))
                                    )
                                  ]
        getPaddedHealthRecordTables Nothing
                                    (Just (fromGregorian 2023 5 10))
                                    (V.fromList [ HealthRecordTable (fromGregorian 2023 5 1) 184 76 (Just 30) (Just 20)
                                                , HealthRecordTable (fromGregorian 2023 5 3) 184 74 (Just 21)  Nothing
                                                , HealthRecordTable (fromGregorian 2023 5 4) 184 72 (Just 25) (Just 26)
                                                , HealthRecordTable (fromGregorian 2023 5 7) 184 70  Nothing  (Just 23)
                                                ])
          `shouldBe` Map.fromList [ ( fromGregorian 2023 5 1
                                    , Just (HealthRecordTable (fromGregorian 2023 5 1) 184 76 (Just 30) (Just 20))
                                    )
                                  , ( fromGregorian 2023 5 2,  Nothing )
                                  , ( fromGregorian 2023 5 3
                                    , Just (HealthRecordTable (fromGregorian 2023 5 3) 184 74 (Just 21)  Nothing)
                                    )
                                  , ( fromGregorian 2023 5 4
                                    , Just (HealthRecordTable (fromGregorian 2023 5 4) 184 72 (Just 25) (Just 26))
                                    )
                                  , ( fromGregorian 2023 5 5,  Nothing )
                                  , ( fromGregorian 2023 5 6,  Nothing )
                                  , ( fromGregorian 2023 5 7
                                    , Just (HealthRecordTable (fromGregorian 2023 5 7) 184 70  Nothing  (Just 23))
                                    )
                                  , ( fromGregorian 2023 5 8,  Nothing )
                                  , ( fromGregorian 2023 5 9,  Nothing )
                                  , ( fromGregorian 2023 5 10, Nothing )
                                  ]
        getPaddedHealthRecordTables (Just (fromGregorian 2023 4 29))
                                    (Just (fromGregorian 2023 5 10))
                                    (V.fromList [ HealthRecordTable (fromGregorian 2023 5 1) 184 76 (Just 30) (Just 20)
                                                , HealthRecordTable (fromGregorian 2023 5 3) 184 74 (Just 21)  Nothing
                                                , HealthRecordTable (fromGregorian 2023 5 4) 184 72 (Just 25) (Just 26)
                                                , HealthRecordTable (fromGregorian 2023 5 7) 184 70  Nothing  (Just 23)
                                                ])
          `shouldBe` Map.fromList [ ( fromGregorian 2023 4 29,  Nothing )
                                  , ( fromGregorian 2023 4 30,  Nothing )
                                  , ( fromGregorian 2023 5 1
                                    , Just (HealthRecordTable (fromGregorian 2023 5 1) 184 76 (Just 30) (Just 20))
                                    )
                                  , ( fromGregorian 2023 5 2,  Nothing )
                                  , ( fromGregorian 2023 5 3
                                    , Just (HealthRecordTable (fromGregorian 2023 5 3) 184 74 (Just 21)  Nothing)
                                    )
                                  , ( fromGregorian 2023 5 4
                                    , Just (HealthRecordTable (fromGregorian 2023 5 4) 184 72 (Just 25) (Just 26))
                                    )
                                  , ( fromGregorian 2023 5 5,  Nothing )
                                  , ( fromGregorian 2023 5 6,  Nothing )
                                  , ( fromGregorian 2023 5 7
                                    , Just (HealthRecordTable (fromGregorian 2023 5 7) 184 70  Nothing  (Just 23))
                                    )
                                  , ( fromGregorian 2023 5 8,  Nothing )
                                  , ( fromGregorian 2023 5 9,  Nothing )
                                  , ( fromGregorian 2023 5 10, Nothing )
                                  ]

    describe "getAllValueByIndicator" $ do
      it "input map day health record table and health indicator should return map day maybe indicator value" $ do
        getAllValueByIndicator Bmi
                               (Map.fromList [ ( fromGregorian 2023 5 1
                                               , Just (HealthRecordTable (fromGregorian 2023 5 1) 184 76 (Just 30) (Just 20))
                                               )
                                             , ( fromGregorian 2023 5 2, Nothing )
                                             , ( fromGregorian 2023 5 3
                                               , Just (HealthRecordTable (fromGregorian 2023 5 3) 184 74 (Just 21) Nothing)
                                               )
                                             , ( fromGregorian 2023 5 4
                                               , Just (HealthRecordTable (fromGregorian 2023 5 4) 184 72 (Just 25) (Just 26))
                                               )
                                             , ( fromGregorian 2023 5 5, Nothing )
                                             , ( fromGregorian 2023 5 6, Nothing )
                                             , ( fromGregorian 2023 5 7
                                               , Just (HealthRecordTable (fromGregorian 2023 5 7) 184 70  Nothing (Just 23))
                                               )
                                             ])
          `shouldBe` Map.fromList [ ( fromGregorian 2023 5 1
                                    , Just (76 / ((184 / 100) ** 2)) )
                                  , ( fromGregorian 2023 5 2, Nothing )
                                  , ( fromGregorian 2023 5 3
                                    , Just (74 / ((184 / 100) ** 2)) )
                                  , ( fromGregorian 2023 5 4
                                    , Just (72 / ((184 / 100) ** 2)) )
                                  , ( fromGregorian 2023 5 5, Nothing )
                                  , ( fromGregorian 2023 5 6, Nothing )
                                  , ( fromGregorian 2023 5 7
                                    , Just (70 / ((184 / 100) ** 2)) )
                                  ]
        getAllValueByIndicator BodyFatPercentage
                               (Map.fromList [ ( fromGregorian 2023 5 1
                                               , Just (HealthRecordTable (fromGregorian 2023 5 1) 184 76 (Just 30) (Just 20))
                                               )
                                             , ( fromGregorian 2023 5 2, Nothing )
                                             , ( fromGregorian 2023 5 3
                                               , Just (HealthRecordTable (fromGregorian 2023 5 3) 184 74 (Just 21) Nothing)
                                               )
                                             , ( fromGregorian 2023 5 4
                                               , Just (HealthRecordTable (fromGregorian 2023 5 4) 184 72 (Just 25) (Just 26))
                                               )
                                             , ( fromGregorian 2023 5 5, Nothing )
                                             , ( fromGregorian 2023 5 6, Nothing )
                                             , ( fromGregorian 2023 5 7
                                               , Just (HealthRecordTable (fromGregorian 2023 5 7) 184 70  Nothing (Just 23))
                                               )
                                             ])
          `shouldBe` Map.fromList [ ( fromGregorian 2023 5 1, Just 30 )
                                  , ( fromGregorian 2023 5 2, Nothing )
                                  , ( fromGregorian 2023 5 3, Just 21 )
                                  , ( fromGregorian 2023 5 4, Just 25 )
                                  , ( fromGregorian 2023 5 5, Nothing )
                                  , ( fromGregorian 2023 5 6, Nothing )
                                  , ( fromGregorian 2023 5 7, Nothing )
                                  ]
        getAllValueByIndicator WaistlineCm
                               (Map.fromList [ ( fromGregorian 2023 5 1
                                               , Just (HealthRecordTable (fromGregorian 2023 5 1) 184 76 (Just 30) (Just 20))
                                               )
                                             , ( fromGregorian 2023 5 2, Nothing )
                                             , ( fromGregorian 2023 5 3
                                               , Just (HealthRecordTable (fromGregorian 2023 5 3) 184 74 (Just 21) Nothing)
                                               )
                                             , ( fromGregorian 2023 5 4
                                               , Just (HealthRecordTable (fromGregorian 2023 5 4) 184 72 (Just 25) (Just 26))
                                               )
                                             , ( fromGregorian 2023 5 5, Nothing )
                                             , ( fromGregorian 2023 5 6, Nothing )
                                             , ( fromGregorian 2023 5 7
                                               , Just (HealthRecordTable (fromGregorian 2023 5 7) 184 70 Nothing (Just 23))
                                               )
                                             ])
          `shouldBe` Map.fromList [ ( fromGregorian 2023 5 1, Just 20 )
                                  , ( fromGregorian 2023 5 2, Nothing )
                                  , ( fromGregorian 2023 5 3, Nothing )
                                  , ( fromGregorian 2023 5 4, Just 26 )
                                  , ( fromGregorian 2023 5 5, Nothing )
                                  , ( fromGregorian 2023 5 6, Nothing )
                                  , ( fromGregorian 2023 5 7, Just 23 )
                                  ]
        getAllValueByIndicator WeightKg
                               (Map.fromList [ ( fromGregorian 2023 5 1
                                               , Just (HealthRecordTable (fromGregorian 2023 5 1) 184 76 (Just 30) (Just 20))
                                               )
                                             , ( fromGregorian 2023 5 2, Nothing )
                                             , ( fromGregorian 2023 5 3
                                               , Just (HealthRecordTable (fromGregorian 2023 5 3) 184 74 (Just 21) Nothing)
                                               )
                                             , ( fromGregorian 2023 5 4
                                               , Just (HealthRecordTable (fromGregorian 2023 5 4) 184 72 (Just 25) (Just 26))
                                               )
                                             , ( fromGregorian 2023 5 5, Nothing )
                                             , ( fromGregorian 2023 5 6, Nothing )
                                             , ( fromGregorian 2023 5 7
                                               , Just (HealthRecordTable (fromGregorian 2023 5 7) 184 70  Nothing (Just 23))
                                               )
                                             ])
          `shouldBe` Map.fromList [ ( fromGregorian 2023 5 1, Just 76 )
                                  , ( fromGregorian 2023 5 2, Nothing )
                                  , ( fromGregorian 2023 5 3, Just 74 )
                                  , ( fromGregorian 2023 5 4, Just 72 )
                                  , ( fromGregorian 2023 5 5, Nothing )
                                  , ( fromGregorian 2023 5 6, Nothing )
                                  , ( fromGregorian 2023 5 7, Just 70 )
                                  ]

    describe "getYAxesCoordByValue" $ do
      it "input min value, max value height and value should return y coords value" $ do
        getYAxesCoordByValue 50  100 600 70  `shouldBe` 600 - (600 * ((70  - 50)  / (100 - 50)))
        getYAxesCoordByValue 23  134 400 25  `shouldBe` 400 - (400 * ((25  - 23)  / (134 - 23)))
        getYAxesCoordByValue 93  520 646 363 `shouldBe` 646 - (646 * ((363 - 93)  / (520 - 93)))
        getYAxesCoordByValue 124 346 942 124 `shouldBe` 942 - (942 * ((124 - 124) / (346 - 124)))
        getYAxesCoordByValue 1   7   600 7   `shouldBe` 600 - (600 * ((7   - 1)   / (7   - 1)))

    describe "getRangeCoords" $ do
      it "input x base, x max, maybe min max value tuple, height and maybe range should return maybe range coords" $ do
        getRangeCoords (0, 600) (1000, 600) Nothing          600 (9 +=* 30) `shouldBe` Nothing
        getRangeCoords (0, 600) (1000, 600) (Just (50, 100)) 600 (9 +=* 30)
          `shouldBe` Just ( (9,  ((0, getYAxesCoordByValue 50 100 600 9),  (1000, getYAxesCoordByValue 50 100 600 9)),  True)
                          , (30, ((0, getYAxesCoordByValue 50 100 600 30), (1000, getYAxesCoordByValue 50 100 600 30)), True) )
        getRangeCoords (0, 600) (1000, 600) (Just (50, 100)) 600 (ubi 12)
          `shouldBe` Just ( (50, ((0, getYAxesCoordByValue 50 100 600 50), (1000, getYAxesCoordByValue 50 100 600 50)), False)
                          , (12, ((0, getYAxesCoordByValue 50 100 600 12), (1000, getYAxesCoordByValue 50 100 600 12)), True) )
        getRangeCoords (0, 600) (1000, 600) (Just (50, 100)) 600 (lbi 2)
          `shouldBe` Just ( (2,   ((0, getYAxesCoordByValue 50 100 600 2),   (1000, getYAxesCoordByValue 50 100 600 2)),   True)
                          , (100, ((0, getYAxesCoordByValue 50 100 600 100), (1000, getYAxesCoordByValue 50 100 600 100)), False) )

    describe "getYAxesCoords" $ do
      it "input height, maybe min max value tuple, vector day, coords tuple and map day maybe indicator value should return vector day maybe indicator value, coords tuple tuple" $ do
        getYAxesCoords 600 Nothing (V.fromList [(fromGregorian 2023 5 1, (0, 0))]) (Map.fromList [(fromGregorian 2023 5 1, Just 23)]) `shouldBe` mempty
        getYAxesCoords 600 (Just (20, 50)) mempty (Map.fromList [(fromGregorian 2023 5 1, Just 23)]) `shouldBe` mempty
        getYAxesCoords 600 (Just (20, 50)) (V.fromList [(fromGregorian 2023 5 1, (0, 0))]) mempty `shouldBe` mempty
        getYAxesCoords 600
                       (Just (20, 50))
                       (V.fromList [ (fromGregorian 2023 5 1, (2, 3))
                                   , (fromGregorian 2023 5 2, (0, 0))
                                   , (fromGregorian 2023 5 2, (4, 5))
                                   ])
                       (Map.fromList [ (fromGregorian 2023 5 1, Just 23)
                                     , (fromGregorian 2023 5 2, Nothing)
                                     , (fromGregorian 2023 5 3, Just 49)
                                     ])
          `shouldBe` [ ( fromGregorian 2023 5 1, Just ( 23
                                                      , ( 2, getYAxesCoordByValue 20 50 600 23 )
                                                      ) )
                     , ( fromGregorian 2023 5 2, Nothing )
                     , ( fromGregorian 2023 5 3, Just ( 49
                                                      , ( 4, getYAxesCoordByValue 20 50 600 49 )
                                                      ) )
                     ]

    describe "getYAxesCoordsWithInfo" $ do
      it "input list day, maybe value, coords tuple and maybe range should return list chart value" $ do
        getYAxesCoordsWithInfo mempty Nothing `shouldBe` []
        getYAxesCoordsWithInfo [ (fromGregorian 2023 5 1,  Nothing)
                               , (fromGregorian 2023 5 2,  Just (1,  (3,  4)))
                               , (fromGregorian 2023 5 3,  Nothing)
                               , (fromGregorian 2023 5 4,  Just (5,  (6,  7)))
                               , (fromGregorian 2023 5 5,  Nothing)
                               , (fromGregorian 2023 5 6,  Nothing)
                               , (fromGregorian 2023 5 7,  Nothing)
                               , (fromGregorian 2023 5 8,  Just (8,  (9,  10)))
                               , (fromGregorian 2023 5 9,  Just (22, (12, 13)))
                               , (fromGregorian 2023 5 10, Nothing)
                               ]
                               Nothing
          `shouldBe` [ ChartValue (fromGregorian 2023 5 1) Nothing
                     , ChartValue (fromGregorian 2023 5 2)
                                  (Just (ChartValueData 1
                                                        (3, 4)
                                                        Nothing
                                                        Nothing))
                     , ChartValue (fromGregorian 2023 5 3) Nothing
                     , ChartValue (fromGregorian 2023 5 4)
                                  (Just (ChartValueData 5
                                                        (6, 7)
                                                        Nothing
                                                        (Just (ChartValueDataInfo 2 4))))
                     , ChartValue (fromGregorian 2023 5 5) Nothing
                     , ChartValue (fromGregorian 2023 5 6) Nothing
                     , ChartValue (fromGregorian 2023 5 7) Nothing
                     , ChartValue (fromGregorian 2023 5 8)
                                  (Just (ChartValueData 8
                                                        (9, 10)
                                                        Nothing
                                                        (Just (ChartValueDataInfo 4 3))))
                     , ChartValue (fromGregorian 2023 5 9)
                                  (Just (ChartValueData 22
                                                        (12, 13)
                                                        Nothing
                                                        (Just (ChartValueDataInfo 1 14))))
                     , ChartValue (fromGregorian 2023 5 10) Nothing
                     ]
        getYAxesCoordsWithInfo [ (fromGregorian 2023 5 1,  Nothing)
                               , (fromGregorian 2023 5 2,  Just (1,  (3,  4)))
                               , (fromGregorian 2023 5 3,  Nothing)
                               , (fromGregorian 2023 5 4,  Just (5,  (6,  7)))
                               , (fromGregorian 2023 5 5,  Nothing)
                               , (fromGregorian 2023 5 6,  Nothing)
                               , (fromGregorian 2023 5 7,  Nothing)
                               , (fromGregorian 2023 5 8,  Just (8,  (9,  10)))
                               , (fromGregorian 2023 5 9,  Just (22, (12, 13)))
                               , (fromGregorian 2023 5 10, Nothing)
                               ]
                               (Just (3 +=* 10))
          `shouldBe` [ ChartValue (fromGregorian 2023 5 1) Nothing
                     , ChartValue (fromGregorian 2023 5 2)
                                  (Just (ChartValueData 1
                                                        (3, 4)
                                                        (Just (True, False, False))
                                                        Nothing))
                     , ChartValue (fromGregorian 2023 5 3) Nothing
                     , ChartValue (fromGregorian 2023 5 4)
                                  (Just (ChartValueData 5
                                                        (6, 7)
                                                        (Just (False, True, False))
                                                        (Just (ChartValueDataInfo 2 4))))
                     , ChartValue (fromGregorian 2023 5 5) Nothing
                     , ChartValue (fromGregorian 2023 5 6) Nothing
                     , ChartValue (fromGregorian 2023 5 7) Nothing
                     , ChartValue (fromGregorian 2023 5 8)
                                  (Just (ChartValueData 8
                                                        (9, 10)
                                                        (Just (False, True, False))
                                                        (Just (ChartValueDataInfo 4 3))))
                     , ChartValue (fromGregorian 2023 5 9)
                                  (Just (ChartValueData 22
                                                        (12, 13)
                                                        (Just (False, False, True))
                                                        (Just (ChartValueDataInfo 1 14))))
                     , ChartValue (fromGregorian 2023 5 10) Nothing
                     ]
        getYAxesCoordsWithInfo [ (fromGregorian 2023 5 1,  Nothing)
                               , (fromGregorian 2023 5 2,  Just (1,  (3,  4)))
                               , (fromGregorian 2023 5 3,  Nothing)
                               , (fromGregorian 2023 5 4,  Just (5,  (6,  7)))
                               , (fromGregorian 2023 5 5,  Nothing)
                               , (fromGregorian 2023 5 6,  Nothing)
                               , (fromGregorian 2023 5 7,  Nothing)
                               , (fromGregorian 2023 5 8,  Just (8,  (9,  10)))
                               , (fromGregorian 2023 5 9,  Just (22, (12, 13)))
                               , (fromGregorian 2023 5 10, Nothing)
                               ]
                               (Just (ubi 10))
          `shouldBe` [ ChartValue (fromGregorian 2023 5 1) Nothing
                     , ChartValue (fromGregorian 2023 5 2)
                                  (Just (ChartValueData 1
                                                        (3, 4)
                                                        (Just (False, True, False))
                                                        Nothing))
                     , ChartValue (fromGregorian 2023 5 3) Nothing
                     , ChartValue (fromGregorian 2023 5 4)
                                  (Just (ChartValueData 5
                                                        (6, 7)
                                                        (Just (False, True, False))
                                                        (Just (ChartValueDataInfo 2 4))))
                     , ChartValue (fromGregorian 2023 5 5) Nothing
                     , ChartValue (fromGregorian 2023 5 6) Nothing
                     , ChartValue (fromGregorian 2023 5 7) Nothing
                     , ChartValue (fromGregorian 2023 5 8)
                                  (Just (ChartValueData 8
                                                        (9, 10)
                                                        (Just (False, True, False))
                                                        (Just (ChartValueDataInfo 4 3))))
                     , ChartValue (fromGregorian 2023 5 9)
                                  (Just (ChartValueData 22
                                                        (12, 13)
                                                        (Just (False, False, True))
                                                        (Just (ChartValueDataInfo 1 14))))
                     , ChartValue (fromGregorian 2023 5 10) Nothing
                     ]
        getYAxesCoordsWithInfo [ (fromGregorian 2023 5 1,  Nothing)
                               , (fromGregorian 2023 5 2,  Just (1,  (3,  4)))
                               , (fromGregorian 2023 5 3,  Nothing)
                               , (fromGregorian 2023 5 4,  Just (5,  (6,  7)))
                               , (fromGregorian 2023 5 5,  Nothing)
                               , (fromGregorian 2023 5 6,  Nothing)
                               , (fromGregorian 2023 5 7,  Nothing)
                               , (fromGregorian 2023 5 8,  Just (8,  (9,  10)))
                               , (fromGregorian 2023 5 9,  Just (22, (12, 13)))
                               , (fromGregorian 2023 5 10, Nothing)
                               ]
                               (Just (lbi 3))
          `shouldBe` [ ChartValue (fromGregorian 2023 5 1) Nothing
                     , ChartValue (fromGregorian 2023 5 2)
                                  (Just (ChartValueData 1
                                                        (3, 4)
                                                        (Just (True, False, False))
                                                        Nothing))
                     , ChartValue (fromGregorian 2023 5 3) Nothing
                     , ChartValue (fromGregorian 2023 5 4)
                                  (Just (ChartValueData 5
                                                        (6, 7)
                                                        (Just (False, True, False))
                                                        (Just (ChartValueDataInfo 2 4))))
                     , ChartValue (fromGregorian 2023 5 5) Nothing
                     , ChartValue (fromGregorian 2023 5 6) Nothing
                     , ChartValue (fromGregorian 2023 5 7) Nothing
                     , ChartValue (fromGregorian 2023 5 8)
                                  (Just (ChartValueData 8
                                                        (9, 10)
                                                        (Just (False, True, False))
                                                        (Just (ChartValueDataInfo 4 3))))
                     , ChartValue (fromGregorian 2023 5 9)
                                  (Just (ChartValueData 22
                                                        (12, 13)
                                                        (Just (False, True, False))
                                                        (Just (ChartValueDataInfo 1 14))))
                     , ChartValue (fromGregorian 2023 5 10) Nothing
                     ]
