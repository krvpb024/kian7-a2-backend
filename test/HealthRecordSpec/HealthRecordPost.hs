{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module HealthRecordSpec.HealthRecordPost (healthRecordPostSpec) where

import           Data.Aeson                (Value)
import           Data.Aeson.QQ.Simple      (aesonQQ)
import           Data.Either.Extra         (fromLeft', fromRight')
import           Data.Maybe                (fromJust)
import           Data.String.Conversions   (cs)
import           Data.Time.Format.ISO8601  (iso8601ParseM)
import           Data.Vector               (fromList)
import           Network.HTTP.Simple       (Response, addRequestHeader,
                                            getResponseBody,
                                            getResponseStatusCode, httpJSON,
                                            parseRequest, setRequestBodyJSON)
import           Network.HTTP.Types.Header (hCookie)
import           SpecUtils                 (api, createTestAccount,
                                            createTestProfile,
                                            deleteTestAccountAll,
                                            deleteTestAccountHealthRecordIfExist,
                                            deleteTestAccountProfileIfExist,
                                            getTestHealthRecord,
                                            healthRecordData1,
                                            healthRecordData2,
                                            signInTestAccount)
import           Test.Hspec                (SpecWith, afterAll_, after_, before,
                                            beforeAll_, context, describe, it,
                                            shouldBe)
import           Types.HealthRecord        (HealthRecordTable (HealthRecordTable))
import           Utils.HandleCsv           (FileType (HealthRecord),
                                            fileNotFoundErr)

healthRecord :: String
healthRecord = "/health_record"

healthRecordPostSpec :: SpecWith ()
healthRecordPostSpec =
  afterAll_ deleteTestAccountAll $
  beforeAll_ createTestAccount $
  after_ deleteTestAccountHealthRecordIfExist $
  before (signInTestAccount >>= createTestProfile) $ do
    describe ("POST " <> healthRecord) $ do
      context "JSON" $ do
        context "Ok" $ do
          it "with correct body should create csv file and respond 201" $ \cookieValue -> do
            request' <- parseRequest ("POST" `api` healthRecord)
            let request = setRequestBodyJSON healthRecordData1 $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 201
            snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

          it "with correct body but null bodyFatPercentage should create csv file and respond 201" $ \cookieValue -> do
            request' <- parseRequest ("POST" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 180
                                                      , "weightKg": 70
                                                      , "bodyFatPercentage": null
                                                      , "waistlineCm": 22
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 201
            snd (fromRight' ioResult) `shouldBe` fromList
              [ HealthRecordTable (fromJust $ iso8601ParseM "2000-01-01")
                                  180
                                  70
                                  Nothing
                                  (Just 22)
              ]

          it "with correct body but null waistlineCm should create csv file and respond 201" $ \cookieValue -> do
            request' <- parseRequest ("POST" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 180
                                                      , "weightKg": 70
                                                      , "bodyFatPercentage": 15
                                                      , "waistlineCm": null
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 201
            snd (fromRight' ioResult) `shouldBe` fromList
              [ HealthRecordTable (fromJust $ iso8601ParseM "2000-01-01")
                                  180
                                  70
                                  (Just 15)
                                  Nothing
              ]

          it "with correct body but null bodyFatPercentage and waistlineCm should create csv file and respond 201" $ \cookieValue -> do
            request' <- parseRequest ("POST" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 180
                                                      , "weightKg": 70
                                                      , "bodyFatPercentage": null
                                                      , "waistlineCm": null
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 201
            snd (fromRight' ioResult) `shouldBe` fromList
              [ HealthRecordTable (fromJust $ iso8601ParseM "2000-01-01")
                                  180
                                  70
                                  Nothing
                                  Nothing
              ]

          it "with correct body should add data to csv file and respond 201" $ \cookieValue -> do
            firstRequest' <- parseRequest ("POST" `api` healthRecord)
            let firstRequest = setRequestBodyJSON healthRecordData1 $
                          addRequestHeader hCookie (cs cookieValue)
                          firstRequest'
            _ <- httpJSON firstRequest :: IO (Response Value)

            request' <- parseRequest ("POST" `api` healthRecord)
            let request = setRequestBodyJSON healthRecordData2 $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 201
            snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1, healthRecordData2]

        context "Error" $ do
          it "without auth cookie and not exist csv file should not create csv file and respond 401" $ \_ -> do
            request' <- parseRequest ("POST" `api` healthRecord)
            let request = setRequestBodyJSON healthRecordData2
                          request'
            response <- httpJSON request :: IO (Response Value)
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't sign in"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 401
            fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

          it "without auth cookie should not add data to csv file and respond 401" $ \cookieValue -> do
            firstRequest' <- parseRequest ("POST" `api` healthRecord)
            let firstRequest = setRequestBodyJSON healthRecordData1 $
                              addRequestHeader hCookie (cs cookieValue)
                              firstRequest'
            _ <- httpJSON firstRequest :: IO (Response Value)

            request' <- parseRequest ("POST" `api` healthRecord)
            let request = setRequestBodyJSON healthRecordData2
                          request'
            response <- httpJSON request :: IO (Response Value)
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't sign in"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 401
            snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

          it "profile not exist should not create csv file and respond 400" $ \cookieValue -> do
            deleteTestAccountProfileIfExist
            request' <- parseRequest ("POST" `api` healthRecord)
            let request = setRequestBodyJSON healthRecordData1 $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't create profile"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

          it "duplicate date should not add data to csv file and respond 400" $ \cookieValue -> do
            duplicateRequest' <- parseRequest ("POST" `api` healthRecord)
            let duplicateRequest = setRequestBodyJSON healthRecordData1 $
                          addRequestHeader hCookie (cs cookieValue)
                          duplicateRequest'
            _ <- httpJSON duplicateRequest :: IO (Response Value)

            request' <- parseRequest ("POST" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 181
                                                      , "weightKg": 71
                                                      , "bodyFatPercentage": 16
                                                      , "waistlineCm": 23
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request :: IO (Response Value)
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "record on 2000-01-01 already exist"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

          context "Invalid field" $ do
            context "date" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "20000101"
                                                          , "heightCm": 180
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $.date: could not parse date: Failed reading: date must be of form [+,-]YYYY-MM-DD"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` healthRecord)
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "20000101"
                                                          , "heightCm": 180
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $.date: could not parse date: Failed reading: date must be of form [+,-]YYYY-MM-DD"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "heightCm" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": "180"
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $.heightCm: parsing Double failed, unexpected String"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` healthRecord)
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": "180"
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $.heightCm: parsing Double failed, unexpected String"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "weightKg" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": 180
                                                          , "weightKg": "70"
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $.weightKg: parsing Double failed, unexpected String"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` healthRecord)
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": 180
                                                          , "weightKg": "70"
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $.weightKg: parsing Double failed, unexpected String"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "bodyFatPercentage" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": 180
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": "15"
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $.bodyFatPercentage: parsing Double failed, unexpected String"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` healthRecord)
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": 180
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": "15"
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $.bodyFatPercentage: parsing Double failed, unexpected String"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "waistlineCm" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": 180
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": "22"
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $.waistlineCm: parsing Double failed, unexpected String"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` healthRecord)
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": 180
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": "22"
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $.waistlineCm: parsing Double failed, unexpected String"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

          context "Missing field" $ do
            context "date" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "heightCm": 180
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $: key \"date\" not found"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` healthRecord)
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "heightCm": 180
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $: key \"date\" not found"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "heightCm" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $: key \"heightCm\" not found"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` healthRecord)
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $: key \"heightCm\" not found"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "weightKg" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": 180
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $: key \"weightKg\" not found"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` healthRecord)
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": 180
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $: key \"weightKg\" not found"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "bodyFatPercentage" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": 180
                                                          , "weightKg": 70
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $: key \"bodyFatPercentage\" not found"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` healthRecord)
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": 180
                                                          , "weightKg": 70
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $: key \"bodyFatPercentage\" not found"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "waistlineCm" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": 180
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": 15
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $: key \"waistlineCm\" not found"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` healthRecord)
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": 180
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": 15
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $: key \"waistlineCm\" not found"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

          context "Double field catch Null" $ do
            context "heightCm" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": null
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $: ['heightCm']: parsing Double failed, unexpected Null"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` healthRecord)
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": null
                                                          , "weightKg": 70
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $: ['heightCm']: parsing Double failed, unexpected Null"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "weightKg" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecord)
                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": 180
                                                          , "weightKg": null
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $: ['weightKg']: parsing Double failed, unexpected Null"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecord)
                firstRequest' <- parseRequest ("POST" `api` healthRecord)
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                          , "heightCm": 180
                                                          , "weightKg": null
                                                          , "bodyFatPercentage": 15
                                                          , "waistlineCm": 22
                                                          }|] $
                              addRequestHeader hCookie (cs cookieValue)
                              request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "Error in $: ['weightKg']: parsing Double failed, unexpected Null"
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]
