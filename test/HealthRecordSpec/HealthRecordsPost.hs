{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module HealthRecordSpec.HealthRecordsPost (healthRecordsPostSpec) where

import           Data.Aeson                            (Value)
import           Data.Aeson.QQ.Simple                  (aesonQQ)
import           Data.Either.Extra                     (fromLeft', fromRight')
import           Data.Maybe                            (fromJust)
import           Data.String.Conversions               (cs)
import           Data.Time.Format.ISO8601              (iso8601ParseM)
import           Data.Vector                           (fromList)
import           Network.HTTP.Client.MultipartFormData (formDataBody,
                                                        partFileSource)
import           Network.HTTP.Simple                   (Response,
                                                        addRequestHeader,
                                                        getResponseBody,
                                                        getResponseStatusCode,
                                                        httpJSON, parseRequest,
                                                        setRequestBodyJSON)
import           Network.HTTP.Types.Header             (hCookie)
import           SpecUtils                             (api, createTestAccount,
                                                        createTestProfile,
                                                        deleteTestAccountAll,
                                                        deleteTestAccountHealthRecordIfExist,
                                                        deleteTestAccountProfileIfExist,
                                                        getTestHealthRecord,
                                                        healthRecordData1,
                                                        signInTestAccount)
import           Test.Hspec                            (SpecWith, afterAll_,
                                                        after_, before,
                                                        beforeAll_, context,
                                                        describe, it, shouldBe)
import           Types.HealthRecord                    (HealthRecordTable (HealthRecordTable))
import           Utils.HandleCsv                       (FileType (HealthRecord),
                                                        fileNotFoundErr)

healthRecords :: String
healthRecords = "/health_records"

healthRecordsPostSpec :: SpecWith ()
healthRecordsPostSpec =
  afterAll_ deleteTestAccountAll $
  beforeAll_ createTestAccount $
  after_ deleteTestAccountHealthRecordIfExist $
  before (signInTestAccount >>= createTestProfile) $ do
    describe ("POST " <> healthRecords) $ do
      context "JSON" $ do
        context "Ok" $ do
          it "with correct csv file should respond 201" $ \cookieValue -> do
            request' <- parseRequest ("POST" `api` healthRecords)
            request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record.csv"] $
                       addRequestHeader hCookie (cs cookieValue)
                       request'
            response <- httpJSON request :: IO (Response Value)
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 201
            snd (fromRight' ioResult) `shouldBe` fromList
              [ HealthRecordTable
                  (fromJust $ iso8601ParseM "2010-02-14")
                  183
                  73
                  Nothing
                  Nothing
              ,  HealthRecordTable
                  (fromJust $ iso8601ParseM "2010-02-13")
                  182
                  72
                  (Just 17)
                  Nothing
              ,  HealthRecordTable
                  (fromJust $ iso8601ParseM "2010-02-12")
                  181
                  71
                  Nothing
                  (Just 23)
              , HealthRecordTable
                  (fromJust $ iso8601ParseM "2010-02-11")
                  180
                  70
                  (Just 15)
                  (Just 22)
              ]

        context "Error" $ do
          it "without auth cookie and not exist csv file should not create csv file and respond 401" $ \_ -> do
            request' <- parseRequest ("POST" `api` healthRecords)
            request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record.csv"]
                       request'
            response <- httpJSON request :: IO (Response Value)
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't sign in"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 401
            fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

          it "without auth cookie should not add data to csv file and respond 401" $ \cookieValue -> do
            firstRequest' <- parseRequest ("POST" `api` "/health_record")
            let firstRequest = setRequestBodyJSON healthRecordData1 $
                               addRequestHeader hCookie (cs cookieValue)
                               firstRequest'
            _ <- httpJSON firstRequest :: IO (Response Value)

            request' <- parseRequest ("POST" `api` healthRecords)
            request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record.csv"]
                       request'
            response <- httpJSON request :: IO (Response Value)
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't sign in"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 401
            snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

          it "profile not exist should not create csv file and respond 400" $ \cookieValue -> do
            deleteTestAccountProfileIfExist
            request' <- parseRequest ("POST" `api` healthRecords)
            request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record.csv"] $
                       addRequestHeader hCookie (cs cookieValue)
                       request'
            response <- httpJSON request :: IO (Response Value)
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't create profile"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

          context "Invalid field" $ do
            context "date" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_invalid_date.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: parse date field error) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` "/health_record")
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_invalid_date.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: parse date field error) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "heightCm" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_invalid_height_cm.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: in named field \"heightCm\": expected Double, got \"invalid\" (Failed reading: takeWhile1)) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` "/health_record")
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_invalid_height_cm.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: in named field \"heightCm\": expected Double, got \"invalid\" (Failed reading: takeWhile1)) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "weightKg" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_invalid_weight_kg.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: in named field \"weightKg\": expected Double, got \"invalid\" (Failed reading: takeWhile1)) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` "/health_record")
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_invalid_weight_kg.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: in named field \"weightKg\": expected Double, got \"invalid\" (Failed reading: takeWhile1)) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "bodyFatPercentage" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_invalid_body_fat_percentage.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: in named field \"bodyFatPercentage\": expected Double, got \"invalid\" (Failed reading: takeWhile1)) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` "/health_record")
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_invalid_body_fat_percentage.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: in named field \"bodyFatPercentage\": expected Double, got \"invalid\" (Failed reading: takeWhile1)) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "waistlineCm" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_invalid_waistline_cm.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: in named field \"waistlineCm\": expected Double, got \"invalid\" (Failed reading: takeWhile1)) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` "/health_record")
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_invalid_waistline_cm.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: in named field \"waistlineCm\": expected Double, got \"invalid\" (Failed reading: takeWhile1)) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

          context "Missing field" $ do
            context "date" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_missing_date.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: parse date field error) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` "/health_record")
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_missing_date.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: parse date field error) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "heightCm" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_missing_height_cm.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: in named field \"heightCm\": expected Double, got \"\" (not enough input)) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` "/health_record")
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_missing_height_cm.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: in named field \"heightCm\": expected Double, got \"\" (not enough input)) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]

            context "weightKg" $ do
              it "not exist csv file should create empty csv file and respond 400" $ \cookieValue -> do
                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_missing_weight_kg.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: in named field \"weightKg\": expected Double, got \"\" (not enough input)) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                fromLeft' ioResult `shouldBe` fileNotFoundErr HealthRecord

              it "exist csv file should not add data to csv file and respond 400" $ \cookieValue -> do
                firstRequest' <- parseRequest ("POST" `api` "/health_record")
                let firstRequest = setRequestBodyJSON healthRecordData1 $
                                  addRequestHeader hCookie (cs cookieValue)
                                  firstRequest'
                _ <- httpJSON firstRequest :: IO (Response Value)

                request' <- parseRequest ("POST" `api` healthRecords)
                request <- formDataBody [partFileSource "health_record_csv" "test/Seeds/test_health_record_missing_weight_kg.csv"] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
                response <- httpJSON request :: IO (Response Value)
                ioResult <- getTestHealthRecord

                getResponseBody response `shouldBe` [aesonQQ|
                  { "message": "parse error (Failed reading: conversion error: in named field \"weightKg\": expected Double, got \"\" (not enough input)) at \"\\n2010-02-13,182.0,72.0,17.0,24.0\\n\""
                  , "payload": null
                  , "status": "Error"
                  }
                |]
                getResponseStatusCode response `shouldBe` 400
                snd (fromRight' ioResult) `shouldBe` fromList [healthRecordData1]
