{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module HealthRecordSpec.HealthRecordPut (healthRecordPutSpec) where

import           Data.Aeson.QQ.Simple      (aesonQQ)
import           Data.Either.Extra         (fromRight')
import           Data.Maybe                (fromJust)
import           Data.String.Conversions   (cs)
import           Data.Time.Format.ISO8601  (iso8601ParseM)
import           Data.Vector               (fromList)
import           Network.HTTP.Simple       (addRequestHeader, getResponseBody,
                                            getResponseStatusCode, httpJSON,
                                            parseRequest, setRequestBodyJSON)
import           Network.HTTP.Types.Header (hCookie)
import           SpecUtils                 (api, createTestAccount,
                                            createTestHealthRecordRows,
                                            createTestProfile,
                                            deleteTestAccountAll,
                                            deleteTestAccountHealthRecordIfExist,
                                            deleteTestAccountProfileIfExist,
                                            getTestHealthRecord,
                                            healthRecordData1,
                                            healthRecordData2,
                                            healthRecordData3,
                                            healthRecordData4,
                                            healthRecordData5,
                                            signInTestAccount)
import           Test.Hspec                (SpecWith, afterAll_, after_, before,
                                            beforeAll_, context, describe, it,
                                            shouldBe)
import           Types.HealthRecord        (HealthRecordTable (HealthRecordTable))

healthRecord :: String
healthRecord = "/health_record"

healthRecordPutSpec :: SpecWith ()
healthRecordPutSpec =
  afterAll_ deleteTestAccountAll $
  beforeAll_ createTestAccount $
  after_ deleteTestAccountHealthRecordIfExist $
  before (signInTestAccount >>= createTestProfile >>= createTestHealthRecordRows) $ do
    describe ("PUT " <> healthRecord) $ do
      context "JSON" $ do
        context "Ok" $ do
          it "with correct body should respond 200" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 185
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200
            snd (fromRight' ioResult) `shouldBe`
              fromList [ HealthRecordTable
                          (fromJust $ iso8601ParseM "2000-01-01")
                          185
                          75
                          (Just 20)
                          (Just 27)
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

          it "with correct body but null bodyFatPercentage should respond 200" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 185
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": null
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200
            snd (fromRight' ioResult) `shouldBe`
              fromList [ HealthRecordTable
                          (fromJust $ iso8601ParseM "2000-01-01")
                          185
                          75
                          Nothing
                          (Just 27)
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

          it "with correct body but null waistlineCm should respond 200" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 185
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": null
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200
            snd (fromRight' ioResult) `shouldBe`
              fromList [ HealthRecordTable
                          (fromJust $ iso8601ParseM "2000-01-01")
                          185
                          75
                          (Just 20)
                          Nothing
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

          it "with correct body but null bodyFatPercentage and waistlineCm should respond 200" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 185
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": null
                                                      , "waistlineCm": null
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": null
              , "payload": null
              , "status": "Ok"
              }
            |]
            getResponseStatusCode response `shouldBe` 200
            snd (fromRight' ioResult) `shouldBe`
              fromList [ HealthRecordTable
                          (fromJust $ iso8601ParseM "2000-01-01")
                          185
                          75
                          Nothing
                          Nothing
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

        context "Error" $ do
          it "without auth cookie should respond 401" $ \_ -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 185
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": 27
                                                      }|]
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't sign in"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 401
            snd (fromRight' ioResult) `shouldBe`
              fromList [ healthRecordData1
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

          it "profile not exist should respond 400" $ \cookieValue -> do
            deleteTestAccountProfileIfExist
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 185
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "haven't create profile"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400

          it "health record file not exist should respond 404" $ \cookieValue -> do
            deleteTestAccountHealthRecordIfExist
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2001-01-01"
                                                      , "heightCm": 185
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "health record on 2001-01-01 not found"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 404

          it "health record file exist but row not found should respond 404" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2001-01-01"
                                                      , "heightCm": 185
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "health record on 2001-01-01 not found"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 404
            snd (fromRight' ioResult) `shouldBe`
              fromList [ healthRecordData1
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

        context "Invalid field" $ do
          it "date should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "20000101"
                                                      , "heightCm": 185
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $.date: could not parse date: Failed reading: date must be of form [+,-]YYYY-MM-DD"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            snd (fromRight' ioResult) `shouldBe`
              fromList [ healthRecordData1
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

          it "heightCm should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": "185"
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $.heightCm: parsing Double failed, unexpected String"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            snd (fromRight' ioResult) `shouldBe`
              fromList [ healthRecordData1
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

          it "weightKg should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 185
                                                      , "weightKg": "75"
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $.weightKg: parsing Double failed, unexpected String"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            snd (fromRight' ioResult) `shouldBe`
              fromList [ healthRecordData1
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

          it "bodyFatPercentage should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 185
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": "20"
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $.bodyFatPercentage: parsing Double failed, unexpected String"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            snd (fromRight' ioResult) `shouldBe`
              fromList [ healthRecordData1
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

          it "waistlineCm should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 185
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": "27"
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $.waistlineCm: parsing Double failed, unexpected String"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            snd (fromRight' ioResult) `shouldBe`
              fromList [ healthRecordData1
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

        context "Missing field" $ do
          it "date should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "heightCm": 185
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $: key \"date\" not found"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            snd (fromRight' ioResult) `shouldBe`
              fromList [ healthRecordData1
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

          it "heightCm should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $: key \"heightCm\" not found"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            snd (fromRight' ioResult) `shouldBe`
              fromList [ healthRecordData1
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

          it "weightKg should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 185
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $: key \"weightKg\" not found"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            snd (fromRight' ioResult) `shouldBe`
              fromList [ healthRecordData1
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

          it "bodyFatPercentage should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 185
                                                      , "weightKg": 75
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $: key \"bodyFatPercentage\" not found"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            snd (fromRight' ioResult) `shouldBe`
              fromList [ healthRecordData1
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

          it "waistlineCm should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 185
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": 20
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $: key \"waistlineCm\" not found"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            snd (fromRight' ioResult) `shouldBe`
              fromList [ healthRecordData1
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

        context "Double field catch Null" $ do
          it "heightCm should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": null
                                                      , "weightKg": 75
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $: ['heightCm']: parsing Double failed, unexpected Null"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            snd (fromRight' ioResult) `shouldBe`
              fromList [ healthRecordData1
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]

          it "weightKg should respond 400" $ \cookieValue -> do
            request' <- parseRequest ("PUT" `api` healthRecord)
            let request = setRequestBodyJSON [aesonQQ|{ "date": "2000-01-01"
                                                      , "heightCm": 185
                                                      , "weightKg": null
                                                      , "bodyFatPercentage": 20
                                                      , "waistlineCm": 27
                                                      }|] $
                          addRequestHeader hCookie (cs cookieValue)
                          request'
            response <- httpJSON request
            ioResult <- getTestHealthRecord

            getResponseBody response `shouldBe` [aesonQQ|
              { "message": "Error in $: ['weightKg']: parsing Double failed, unexpected Null"
              , "payload": null
              , "status": "Error"
              }
            |]
            getResponseStatusCode response `shouldBe` 400
            snd (fromRight' ioResult) `shouldBe`
              fromList [ healthRecordData1
                       , healthRecordData2
                       , healthRecordData3
                       , healthRecordData4
                       , healthRecordData5
                       ]
