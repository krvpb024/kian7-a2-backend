module HealthRecordSpec (healthRecordSpec) where

import           HealthRecordSpec.HealthRecordDelete    (healthRecordDeleteSpec)
import           HealthRecordSpec.HealthRecordPost      (healthRecordPostSpec)
import           HealthRecordSpec.HealthRecordPut       (healthRecordPutSpec)
import           HealthRecordSpec.HealthRecordsChartGet (healthRecordsChartGetSpec)
import           HealthRecordSpec.HealthRecordsGet      (healthRecordsGetSpec)
import           HealthRecordSpec.HealthRecordsPost     (healthRecordsPostSpec)
import           Test.Hspec                             (SpecWith)

healthRecordSpec :: SpecWith ()
healthRecordSpec = do
    healthRecordPostSpec
    healthRecordPutSpec
    healthRecordDeleteSpec
    healthRecordsGetSpec
    healthRecordsPostSpec
    healthRecordsChartGetSpec
