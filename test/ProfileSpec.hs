{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module ProfileSpec (profileSpec) where

import           Data.Aeson                (Value)
import           Data.Aeson.QQ.Simple      (aesonQQ)
import           Data.String.Conversions   (cs)
import           Network.HTTP.Simple       (Response, addRequestHeader,
                                            getResponseBody,
                                            getResponseStatusCode, httpJSON,
                                            parseRequest, setRequestBodyJSON)
import           Network.HTTP.Types.Header (hCookie)
import           SpecUtils                 (api, createTestAccount,
                                            deleteTestAccount,
                                            deleteTestAccountProfileIfExist,
                                            signInTestAccount)
import           Test.Hspec                (SpecWith, afterAll_, beforeAll,
                                            before_, context, describe, it,
                                            shouldBe)

profileSpec :: SpecWith ()
profileSpec =
  before_ deleteTestAccountProfileIfExist $
  afterAll_ (deleteTestAccountProfileIfExist >> deleteTestAccount) $
  beforeAll (createTestAccount >> signInTestAccount) $ do
    let profile = "/profile"
    describe ("POST " <> profile) $ do
      context "JSON" $ do
        it "with correct body should respond 201" $ \cookieValue -> do
          request' <- parseRequest ("POST" `api` profile)
          let request = setRequestBodyJSON [aesonQQ|{ "gender": "Male"
                                                    , "birthDate": "1991-01-01"
                                                    }|] $
                        addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": null
            , "payload": {
                "birthDate": "1991-01-01"
              , "gender": "Male"
              }
            , "status": "Ok"
            }
          |]
          getResponseStatusCode response `shouldBe` 201

        it "without auth cookie should respond 401" $ \_ -> do
          request' <- parseRequest ("POST" `api` profile)
          let request = setRequestBodyJSON [aesonQQ|{ "gender": "Male"
                                                    , "birthDate": "1991-01-01"
                                                    }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "haven't sign in"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 401

        it "profile already exist should respond 400" $ \cookieValue -> do
          request' <- parseRequest ("POST" `api` profile)
          let request = setRequestBodyJSON [aesonQQ|{ "gender": "Male"
                                                    , "birthDate": "1991-01-01"
                                                    }|] $
                        addRequestHeader hCookie (cs cookieValue)
                        request'
          _ <- httpJSON request :: IO (Response Value)
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Profile has created already"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

        it "with invalid gender should respond 400" $ \cookieValue -> do
          request' <- parseRequest ("POST" `api` profile)
          let request = setRequestBodyJSON [aesonQQ|{ "gender": "not Male or Female"
                                                    , "birthDate": "1991-01-01"
                                                    }|] $
                        addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $.gender: parsing Types.Profile.Gender failed, expected one of the tags [\"Male\",\"Female\"], but found tag \"not Male or Female\""
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

        it "with missing gender should respond 400" $ \cookieValue -> do
          request' <- parseRequest ("POST" `api` profile)
          let request = setRequestBodyJSON [aesonQQ|{ "birthDate": "1991-01-01"
                                                    }|] $
                        addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $: key \"gender\" not found"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

        it "with invalid birthDate should respond 400" $ \cookieValue -> do
          request' <- parseRequest ("POST" `api` profile)
          let request = setRequestBodyJSON [aesonQQ|{ "gender": "Male"
                                                    , "birthDate": "19910101"
                                                    }|] $
                        addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $.birthDate: could not parse date: Failed reading: date must be of form [+,-]YYYY-MM-DD"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

        it "with missing birthDate should respond 400" $ \cookieValue -> do
          request' <- parseRequest ("POST" `api` profile)
          let request = setRequestBodyJSON [aesonQQ|{ "gender": "Male"
                                                    }|] $
                        addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $: key \"birthDate\" not found"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

    describe ("GET " <> profile) $ do
      context "JSON" $ do
        it "with auth cookie and existed profile should respond 200" $ \cookieValue -> do
          createProfileRequest' <- parseRequest ("POST" `api` profile)
          let createProfileRequest = setRequestBodyJSON [aesonQQ|{ "gender": "Male"
                                                                 , "birthDate": "1991-01-01"
                                                                 }|] $
                                     addRequestHeader hCookie (cs cookieValue)
                                     createProfileRequest'
          _ <- httpJSON createProfileRequest :: IO (Response Value)

          request' <- parseRequest ("GET" `api` profile)
          let request = addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": null
            , "payload": { "gender": "Male"
                         , "birthDate": "1991-01-01"
             }
            , "status": "Ok"
            }
          |]
          getResponseStatusCode response `shouldBe` 200

        it "without auth cookie should respond 401" $ \_ -> do
          request <- parseRequest ("GET" `api` profile)
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "haven't sign in"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 401

        it "with auth cookie and haven't create profile should respond 400" $ \cookieValue -> do
          request' <- parseRequest ("GET" `api` profile)
          let request = addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Profile data not found"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 404

    describe ("PUT " <> profile) $ do
      context "JSON" $ do
        it "with correct body should respond 200" $ \cookieValue -> do
          createProfileRequest' <- parseRequest ("POST" `api` profile)
          let createProfileRequest = setRequestBodyJSON [aesonQQ|{ "gender": "Male"
                                                                 , "birthDate": "1991-01-01"
                                                                 }|] $
                                     addRequestHeader hCookie (cs cookieValue)
                                     createProfileRequest'
          _ <- httpJSON createProfileRequest :: IO (Response Value)

          request' <- parseRequest ("PUT" `api` profile)
          let request = setRequestBodyJSON [aesonQQ|{ "gender": "Female"
                                                    , "birthDate": "1993-01-01"
                                                    }|] $
                        addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": null
            , "payload": {
                "birthDate": "1993-01-01"
              , "gender": "Female"
              }
            , "status": "Ok"
            }
          |]
          getResponseStatusCode response `shouldBe` 200

        it "without auth cookie should respond 401" $ \_ -> do
          request' <- parseRequest ("PUT" `api` profile)
          let request = setRequestBodyJSON [aesonQQ|{ "gender": "Female"
                                                    , "birthDate": "1993-01-01"
                                                    }|]
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "haven't sign in"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 401

        it "profile not exist should respond 404" $ \cookieValue -> do
          request' <- parseRequest ("PUT" `api` profile)
          let request = setRequestBodyJSON [aesonQQ|{ "gender": "Female"
                                                    , "birthDate": "1993-01-01"
                                                    }|] $
                        addRequestHeader hCookie (cs cookieValue)
                        request'
          _ <- httpJSON request :: IO (Response Value)
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Profile data not found"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 404

        it "with invalid gender should respond 400" $ \cookieValue -> do
          request' <- parseRequest ("PUT" `api` profile)
          let request = setRequestBodyJSON [aesonQQ|{ "gender": "not Male or Female"
                                                    , "birthDate": "1993-01-01"
                                                    }|] $
                        addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $.gender: parsing Types.Profile.Gender failed, expected one of the tags [\"Male\",\"Female\"], but found tag \"not Male or Female\""
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

        it "with missing gender should respond 400" $ \cookieValue -> do
          request' <- parseRequest ("PUT" `api` profile)
          let request = setRequestBodyJSON [aesonQQ|{ "birthDate": "1993-01-01"
                                                    }|] $
                        addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $: key \"gender\" not found"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

        it "with invalid birthDate should respond 400" $ \cookieValue -> do
          request' <- parseRequest ("PUT" `api` profile)
          let request = setRequestBodyJSON [aesonQQ|{ "gender": "Female"
                                                    , "birthDate": "19910101"
                                                    }|] $
                        addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $.birthDate: could not parse date: Failed reading: date must be of form [+,-]YYYY-MM-DD"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400

        it "with missing birthDate should respond 400" $ \cookieValue -> do
          request' <- parseRequest ("PUT" `api` profile)
          let request = setRequestBodyJSON [aesonQQ|{ "gender": "Female"
                                                    }|] $
                        addRequestHeader hCookie (cs cookieValue)
                        request'
          response <- httpJSON request :: IO (Response Value)

          getResponseBody response `shouldBe` [aesonQQ|
            { "message": "Error in $: key \"birthDate\" not found"
            , "payload": null
            , "status": "Error"
            }
          |]
          getResponseStatusCode response `shouldBe` 400
