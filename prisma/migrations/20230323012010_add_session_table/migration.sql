-- CreateTable
CREATE TABLE "session" (
    "id" TEXT NOT NULL,
    "account_id" INTEGER NOT NULL,
    "expire_at" TIMESTAMPTZ DEFAULT (now() + '31 days'::interval),

    CONSTRAINT "session_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "session" ADD CONSTRAINT "session_account_id_fkey" FOREIGN KEY ("account_id") REFERENCES "account"("id") ON DELETE CASCADE ON UPDATE CASCADE;
