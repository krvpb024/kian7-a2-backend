/*
  Warnings:

  - You are about to drop the column `account_id` on the `session` table. All the data in the column will be lost.
  - You are about to drop the column `expire_at` on the `session` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "session" DROP CONSTRAINT "session_account_id_fkey";

-- AlterTable
ALTER TABLE "session" DROP COLUMN "account_id",
DROP COLUMN "expire_at";
