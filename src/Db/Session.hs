module Db.Session (createAccountSession, createSessionSession, checkAccountExistSession, deleteSessionSession, deleteAccountSession, updateAccountNameSession, updateAccountPasswordSession, deleteAccountByIdSession, selectAccountByIdSession) where

import           Data.Int      (Int32)
import qualified Data.Text     as TS
import           Db.Statement  (deleteAccountByIdStatement,
                                deleteAccountByNameStatement,
                                deleteSessionStatement, insertAccountStatement,
                                insertSessionStatement,
                                selectAccountByIdStatement,
                                selectAccountByNameStatement,
                                updateAccountNameStatement,
                                updateAccountPasswordStatement)
import qualified Hasql.Session as Session

createAccountSession :: TS.Text -> TS.Text -> Session.Session ()
createAccountSession name password =
  Session.statement (name, password) insertAccountStatement

checkAccountExistSession :: TS.Text -> Session.Session (Maybe (Int32, TS.Text, TS.Text))
checkAccountExistSession name =
  Session.statement name selectAccountByNameStatement

selectAccountByIdSession :: Int32 -> Session.Session (Maybe (Int32, TS.Text, TS.Text))
selectAccountByIdSession accountId =
  Session.statement accountId selectAccountByIdStatement

deleteAccountSession :: TS.Text -> Session.Session ()
deleteAccountSession name =
  Session.statement name deleteAccountByNameStatement

createSessionSession :: TS.Text -> Int32 -> Session.Session TS.Text
createSessionSession sessionId accountId =
  Session.statement (sessionId, accountId) insertSessionStatement

deleteSessionSession :: TS.Text -> Session.Session ()
deleteSessionSession sessionId =
  Session.statement sessionId deleteSessionStatement

updateAccountNameSession :: Int32 -> TS.Text -> Session.Session ()
updateAccountNameSession accountId name =
  Session.statement (accountId, name) updateAccountNameStatement

updateAccountPasswordSession :: Int32 -> TS.Text -> Session.Session ()
updateAccountPasswordSession accountId password =
  Session.statement (accountId, password) updateAccountPasswordStatement

deleteAccountByIdSession :: Int32 ->  Session.Session ()
deleteAccountByIdSession accountId =
  Session.statement accountId deleteAccountByIdStatement
