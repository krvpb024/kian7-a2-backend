{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Db.Statement (insertAccountStatement, insertSessionStatement, selectAccountByNameStatement, deleteSessionStatement, deleteAccountByNameStatement, updateAccountNameStatement, updateAccountPasswordStatement, deleteAccountByIdStatement, selectAccountByIdStatement) where

import           Data.Int        (Int32)
import qualified Data.Text       as TS
import           Hasql.Statement (Statement)
import qualified Hasql.TH        as TH

insertAccountStatement :: Statement (TS.Text, TS.Text) ()
insertAccountStatement =
  [TH.resultlessStatement|
    INSERT INTO account (name, password) VALUES ($1 :: text, $2 :: text)
  |]

selectAccountByNameStatement :: Statement TS.Text (Maybe (Int32, TS.Text, TS.Text))
selectAccountByNameStatement =
  [TH.maybeStatement|
    SELECT id :: int4, name :: text, password :: text FROM account WHERE name = $1 :: text
  |]

selectAccountByIdStatement :: Statement Int32 (Maybe (Int32, TS.Text, TS.Text))
selectAccountByIdStatement =
  [TH.maybeStatement|
    SELECT id :: int4, name :: text, password :: text FROM account WHERE id = $1 :: int4
  |]

deleteAccountByNameStatement :: Statement TS.Text ()
deleteAccountByNameStatement =
  [TH.resultlessStatement|
    DELETE FROM account WHERE name = $1 :: text
  |]

insertSessionStatement :: Statement (TS.Text, Int32) TS.Text
insertSessionStatement =
  [TH.singletonStatement|
    INSERT INTO session (id, account_id) VALUES ($1 :: text, $2 :: int4) RETURNING (id :: text)
  |]

deleteSessionStatement :: Statement TS.Text ()
deleteSessionStatement =
  [TH.resultlessStatement|
    DELETE FROM session WHERE id = $1 :: text
  |]

updateAccountNameStatement :: Statement (Int32, TS.Text) ()
updateAccountNameStatement =
  [TH.resultlessStatement|
    UPDATE account SET name = $2 :: text WHERE id = $1 :: int4
  |]

updateAccountPasswordStatement :: Statement (Int32, TS.Text) ()
updateAccountPasswordStatement =
  [TH.resultlessStatement|
    UPDATE account SET password = $2 :: text WHERE id = $1 :: int4
  |]

deleteAccountByIdStatement :: Statement Int32 ()
deleteAccountByIdStatement =
  [TH.resultlessStatement|
    DELETE FROM account WHERE id = $1 :: int4
  |]
