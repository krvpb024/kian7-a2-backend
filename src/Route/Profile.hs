{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Route.Profile (ProfileAPI, profileServerReader, getProfileData) where

import           Conf                             (Env)
import           Control.Exception                (try)
import           Control.Monad                    (unless, when)
import           Control.Monad.IO.Class           (MonadIO (liftIO))
import           Control.Monad.Trans.Reader       (Reader, asks)
import           Data.Aeson                       (encode)
import qualified Data.ByteString.Lazy             as BL
import qualified Data.Csv                         as C
import           Data.Either.Extra                (maybeToEither)
import           Data.String.Conversions          (cs)
import qualified Data.Vector                      as V
import           Network.Wai                      (Request)

import           Data.Maybe                       (fromJust, isJust)
import           Servant                          (AuthProtect, Get,
                                                   HasServer (ServerT, hoistServerWithContext),
                                                   JSON, Proxy (Proxy), Put,
                                                   ReqBody, Server,
                                                   ServerError (errBody),
                                                   StdMethod (POST), Verb,
                                                   throwError, (:<|>) ((:<|>)),
                                                   (:>))
import           Servant.Server.Experimental.Auth (AuthHandler)
import           System.Directory                 (doesFileExist)
import           Types.Auth                       (SessionData (SessionData))
import           Types.Profile                    (ProfileTable)
import           Utils.ErrorResponse              (jsonErr400, unknownError)
import           Utils.HandleCsv                  (FileType (Profile),
                                                   createCsvFile, csvEmptyError,
                                                   fileNotFoundErr,
                                                   getCsvFilePath,
                                                   parseByteStringToCsv,
                                                   readCsv)
import           Utils.ReaderHandler              (ReaderHandler,
                                                   readerToHandler)
import           Utils.ResponseType               (GeneralResponse (GeneralResponse),
                                                   Status (Error, Ok))

profileServerReader :: Reader Env (Server ProfileAPI)
profileServerReader = asks $ \env ->
  hoistServerWithContext profileAPI
                         (Proxy :: Proxy '[AuthHandler Request (Either ServerError SessionData)])
                         (readerToHandler env)
                         profileServerT

profileServerT :: ServerT ProfileAPI ReaderHandler
profileServerT = profilePostJSONHandler
            :<|> profileGetHandler
            :<|> profilePutJSONHandler

profileAPI :: Proxy ProfileAPI
profileAPI = Proxy
type ProfileAPI = "profile" :> ( ProfilePostAPI
                            :<|> ProfileGetAPI
                            :<|> ProfilePutAPI
                               )

type ProfilePostAPI = AuthProtect "cookie-auth" :>
                      ReqBody '[JSON] ProfileTable :>
                      Verb 'POST 201 '[JSON] (GeneralResponse ProfileTable)

profilePostJSONHandler :: Either ServerError SessionData
                       -> ProfileTable
                       -> ReaderHandler (GeneralResponse ProfileTable)
profilePostJSONHandler (Left err) _ = throwError err
profilePostJSONHandler (Right (SessionData _ accountId)) profileData = do
  csvFilePath <- liftIO $ getCsvFilePath (show accountId) Profile
  isFileExist <- liftIO $ doesFileExist csvFilePath
  if isFileExist
  then
    throwError jsonErr400
      { errBody = encode $
                  GeneralResponse Error
                  (Just $ cs $ show Profile <> " has created already")
                  (Nothing :: Maybe ProfileTable) }
  else do
    liftIO $ createCsvFile (show accountId) Profile
  rawCsvByteString <- liftIO $ readCsv (show accountId) Profile
  let parsedCsvData = rawCsvByteString >>= parseByteStringToCsv >>= createProfileRow profileData
  createCsvIOResult <- liftIO $ createProfileFile csvFilePath parsedCsvData
  case createCsvIOResult of
    Left (serverError, maybeErrMsg) -> do
      when (isJust maybeErrMsg) $ liftIO $ putStrLn $ fromJust maybeErrMsg
      throwError serverError
    Right _ -> do
      return $ GeneralResponse Ok Nothing $ Just profileData
  where
    createProfileRow :: ProfileTable
                     -> (C.Header, V.Vector ProfileTable)
                     -> Either (ServerError, Maybe String)
                               (C.Header, V.Vector ProfileTable)
    createProfileRow newProfile record = Right $ fmap (`V.snoc` newProfile) record

    createProfileFile :: String
                      -> Either (ServerError, Maybe String)
                                (C.Header, V.Vector ProfileTable)
                      -> IO (Either (ServerError, Maybe String) ())
    createProfileFile filePath csv = case csv of
      Left err -> return $ Left err
      Right (header, profiles) -> do
        ioResult <- try $ BL.writeFile filePath $
                                       C.encodeByName header $
                                       V.toList profiles :: IO (Either IOError ())
        case ioResult of
          Left err -> return $ Left (unknownError, Just $ show err)
          Right _  -> return $ Right ()

type ProfileGetAPI = AuthProtect "cookie-auth" :> Get '[JSON] (GeneralResponse ProfileTable)

profileGetHandler :: Either ServerError SessionData
                  -> ReaderHandler (GeneralResponse ProfileTable)
profileGetHandler (Left err) = throwError err
profileGetHandler (Right (SessionData _ accountId)) = do
  rawCsvByteString <- liftIO $ readCsv (show accountId) Profile
  let profileData = rawCsvByteString >>=
                    parseByteStringToCsv >>=
                    getProfileData
  case profileData of
    Left (serverError, maybeErrMsg) -> do
      when (isJust maybeErrMsg) $ liftIO $ putStrLn $ fromJust maybeErrMsg
      throwError serverError
    Right profile -> return $ GeneralResponse Ok Nothing $ Just profile

getProfileData :: (C.Header, V.Vector ProfileTable)
                -> Either (ServerError, Maybe String) ProfileTable
getProfileData (_, profiles) = maybeToEither
                                  (csvEmptyError Profile)
                                  (profiles V.!? 0)

type ProfilePutAPI = AuthProtect "cookie-auth" :>
                       ReqBody '[JSON] ProfileTable :>
                       Put '[JSON] (GeneralResponse ProfileTable)

profilePutJSONHandler :: Either ServerError SessionData
                      -> ProfileTable
                      -> ReaderHandler (GeneralResponse ProfileTable)
profilePutJSONHandler (Left err) _ = throwError err
profilePutJSONHandler (Right (SessionData _ accountId)) profileData = do
  csvFilePath <- liftIO $ getCsvFilePath (show accountId) Profile
  isFileExist <- liftIO $ doesFileExist csvFilePath
  unless isFileExist $ throwError $ fst $ fileNotFoundErr Profile
  rawCsvByteString <- liftIO $ readCsv (show accountId) Profile
  let editedCsvData = rawCsvByteString >>=
                      parseByteStringToCsv >>=
                      editProfileRow profileData
  editCsvIOResult <- liftIO $ editProfileFile csvFilePath editedCsvData
  case editCsvIOResult of
    Left (serverError, maybeErrMsg) -> do
      when (isJust maybeErrMsg) $ liftIO $ putStrLn $ fromJust maybeErrMsg
      throwError serverError
    Right _ -> do
      return $ GeneralResponse Ok Nothing $ Just profileData
  where
    editProfileRow :: ProfileTable
                   -> (C.Header, V.Vector ProfileTable)
                   -> Either (ServerError, Maybe String)
                            (C.Header, V.Vector ProfileTable)
    editProfileRow newProfile record = Right $ fmap (const $ V.singleton newProfile) record

    editProfileFile :: String
                    -> Either (ServerError, Maybe String)
                              (C.Header, V.Vector ProfileTable)
                    -> IO (Either (ServerError, Maybe String) ())
    editProfileFile filePath csv = case csv of
      Left err -> return $ Left err
      Right (header, profiles) -> do
        ioResult <- try $ BL.writeFile filePath $
                                       C.encodeByName header $
                                       V.toList profiles :: IO (Either IOError ())
        case ioResult of
          Left err -> return $ Left (unknownError, Just $ show err)
          Right _  -> return $ Right ()
