{-# LANGUAGE ConstraintKinds       #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

module Route.Auth (AuthAPI, authServerReader, authSignUpPostHandler, getValidAccount, getCheckPwResult, getAccount) where

import           Conf                             (Env (getPool, getSessionKey))
import           Control.Monad                    (unless, when)
import           Control.Monad.IO.Class           (MonadIO (liftIO))
import           Control.Monad.Trans.Reader       (Reader, asks)
import qualified Data.Aeson                       as A
import           Data.Maybe                       (fromJust, isJust)
import           Data.Password.Argon2             (PasswordCheck (PasswordCheckFail, PasswordCheckSuccess),
                                                   PasswordHash (PasswordHash, unPasswordHash),
                                                   checkPassword, hashPassword,
                                                   mkPassword)
import           Data.String.Conversions          (cs)
import qualified Data.Text.Lazy                   as TL
import           Db.Session                       (checkAccountExistSession,
                                                   createAccountSession,
                                                   createSessionSession,
                                                   deleteSessionSession)
import           Hasql.Pool                       (Pool,
                                                   UsageError (SessionUsageError),
                                                   use)
import           Hasql.Session                    (CommandError (ResultError),
                                                   QueryError (QueryError),
                                                   ResultError (ServerError))
import           Network.URI.Encode               (encode)
import           Network.Wai                      (Request)
import           Servant                          (AuthProtect, FormUrlEncoded,
                                                   HasServer (ServerT, hoistServerWithContext),
                                                   Header, Headers, JSON,
                                                   NoContent (NoContent),
                                                   Proxy (Proxy), ReqBody,
                                                   Server,
                                                   ServerError (errBody),
                                                   StdMethod (POST), UVerb,
                                                   Union,
                                                   WithStatus (WithStatus),
                                                   addHeader, respond,
                                                   throwError,
                                                   type (:<|>) ((:<|>)),
                                                   type (:>))
import           Servant.Auth.Server              (SetCookie)
import           Servant.Server.Experimental.Auth (AuthHandler)
import           Types.Account                    (AccountTable (AccountTable),
                                                   accountTableId,
                                                   accountTablePassword)
import           Types.Auth                       (AuthSignInData (AuthSignInData),
                                                   AuthSignUpData (AuthSignUpData),
                                                   SessionData (SessionData))
import           Utils.AuthContext                (defaultSessionCookieSetting,
                                                   deleteSessionCookieSetting)
import           Utils.ErrorResponse              (jsonErr400,
                                                   passwordConfirmNotPassError,
                                                   unknownError)
import           Utils.ReaderHandler              (ReaderHandler,
                                                   readerToHandler)
import           Utils.ResponseType               (GeneralResponse (GeneralResponse),
                                                   RequestBodyContentType (FormBody),
                                                   Status (Error, Ok),
                                                   getMediaType)
import qualified Web.ClientSession                as ClientSession
import           Web.Cookie                       (SetCookie (setCookieValue))

authServerReader :: Reader Env (Server AuthAPI)
authServerReader = asks $ \env ->
  hoistServerWithContext authAPI
                         (Proxy :: Proxy '[AuthHandler Request (Either ServerError SessionData)])
                         (readerToHandler env)
                         authServerT

authServerT :: ServerT AuthAPI ReaderHandler
authServerT = authSignUpPostHandler
         :<|> authSignInPostHandler
         :<|> authSignOutHandler

authAPI :: Proxy AuthAPI
authAPI = Proxy

type AuthAPI = "auth" :> ( AuthSignUpAPI
                      :<|> AuthSignInAPI
                      :<|> AuthSignOutAPI
                         )

type AuthSignUpAPI = "sign_up" :> Header "Content-Type" String :>
                                  ReqBody '[JSON, FormUrlEncoded] AuthSignUpData :>
                                  UVerb 'POST '[JSON] [ WithStatus 201 (GeneralResponse A.Value)
                                                      , WithStatus 303 (Headers '[Header "Location" TL.Text] NoContent)
                                                      ]


authSignUpPostHandler :: Maybe String
                      -> AuthSignUpData
                      -> ReaderHandler (Union '[ WithStatus 201 (GeneralResponse A.Value)
                                               , WithStatus 303 (Headers '[Header "Location" TL.Text] NoContent)
                                               ])
authSignUpPostHandler contentType (AuthSignUpData name password passwordConfirm) = do
  pool <- asks getPool
  unless (password == passwordConfirm) $ throwError passwordConfirmNotPassError
  hashedPassword <- hashPassword $ mkPassword $ cs password
  dbResult <- liftIO $ use pool (createAccountSession (cs name) (cs $ unPasswordHash hashedPassword))
  case dbResult of
    Left (SessionUsageError (QueryError _ _ (ResultError (ServerError "23505" _ _ _ _)))) ->
      throwError jsonErr400
        { errBody = A.encode (GeneralResponse Error (Just "account has been taken") Nothing
                           :: GeneralResponse ())
        }
    Left _ -> do
      throwError unknownError
    Right _  -> case getMediaType contentType of
      FormBody -> respond $ WithStatus @303 $
                            ( addHeader ("/sign_in" :: TL.Text)
                              NoContent :: Headers '[Header "Location" TL.Text] NoContent
                            )
      _        -> respond $ WithStatus @201 $
                            GeneralResponse Ok Nothing $ Just $ A.object ["name" A..= name]

type AuthSignInAPI = "sign_in" :> Header "Content-Type" String :>
                                  ReqBody '[JSON, FormUrlEncoded] AuthSignInData :>
                                  UVerb 'POST '[JSON] [ WithStatus 200 (Headers '[ Header "Set-Cookie" SetCookie
                                                                                 ] (GeneralResponse A.Value)
                                                                       )
                                                      , WithStatus 303 (Headers '[ Header "Set-Cookie" SetCookie
                                                                                 , Header "Location" TL.Text
                                                                                 ] NoContent
                                                                       )
                                                      ]

authSignInPostHandler :: Maybe String
                      -> AuthSignInData
                      -> ReaderHandler (Union '[ WithStatus 200 (Headers '[ Header "Set-Cookie" SetCookie
                                                                          ] (GeneralResponse A.Value)
                                                                )
                                               , WithStatus 303 (Headers '[ Header "Set-Cookie" SetCookie
                                                                          , Header "Location" TL.Text
                                                                          ] NoContent
                                                                )
                                               ])
authSignInPostHandler contentType (AuthSignInData name password) = do
  pool <- asks getPool
  sessionKey <- asks getSessionKey
  account <- getAccount pool name
  let validAccount = getValidAccount <$> getCheckPwResult password $ account
  storedSession <- storeSession pool sessionKey validAccount
  case storedSession of
    Left (serverError, maybeErrMsg) -> do
      when (isJust maybeErrMsg) $ liftIO $ print $ fromJust maybeErrMsg
      throwError serverError
    Right sessionId -> case getMediaType contentType of
      FormBody -> respond $ WithStatus @303 $
                            ( addHeader (defaultSessionCookieSetting { setCookieValue = cs $ encode $ cs sessionId }) $
                              addHeader ("/sign_in" :: TL.Text)
                              NoContent
                              :: Headers '[ Header "Set-Cookie" SetCookie
                                          , Header "Location" TL.Text
                                          ] NoContent
                            )
      _        -> respond $ WithStatus @200 $
                            ( addHeader (defaultSessionCookieSetting { setCookieValue = cs $ encode $ cs sessionId }) $
                              GeneralResponse Ok Nothing Nothing
                              :: Headers '[Header "Set-Cookie" SetCookie] (GeneralResponse A.Value)
                            )
  where
    storeSession :: (MonadIO m) => Pool
                                -> ClientSession.Key
                                -> Either (ServerError, Maybe String) AccountTable
                                -> m (Either (ServerError, Maybe String) TL.Text)
    storeSession _  _   (Left err) = return $ Left err
    storeSession pl key (Right accT) = do
      sessionId <- liftIO $ ClientSession.encryptIO key $ cs $ show $ accountTableId accT
      dbResult <- liftIO $ use pl $ createSessionSession (cs sessionId)
                                                         (accountTableId accT)
      return $ case dbResult of
        Right sid -> Right $ cs sid
        Left err  -> Left (unknownError, Just $ show err)

getAccount :: (MonadIO m) => Pool
                          -> TL.Text
                          -> m (Either (ServerError, Maybe String)
                                        AccountTable)
getAccount pl accName = do
  eitherAccount <- liftIO $ use pl (checkAccountExistSession $ cs accName)
  return $ case eitherAccount of
    Right (Just (accountId, n, p)) -> Right $ AccountTable accountId (cs n) (cs p)
    Right Nothing -> Left (jsonErr400 { errBody = A.encode $
                                                  GeneralResponse Error
                                                  (Just "account not exist")
                                                  (Nothing :: Maybe ())
                                      }, Nothing)
    Left err -> Left (unknownError, Just $ show err)

getCheckPwResult :: TL.Text
                  -> Either e AccountTable
                  -> Either e (AccountTable, PasswordCheck)
getCheckPwResult inputPw (Right accT) =
  Right (accT, checkPassword (mkPassword $ cs inputPw) (PasswordHash $ cs $ accountTablePassword accT))
getCheckPwResult _ (Left e) = Left e

getValidAccount :: Either (ServerError, Maybe String)
                          (AccountTable, PasswordCheck)
                -> Either (ServerError, Maybe String)
                          AccountTable
getValidAccount (Right (accT, PasswordCheckSuccess)) = Right accT
getValidAccount (Right (_,    PasswordCheckFail))    = Left (jsonErr400
  { errBody = A.encode $ GeneralResponse Error
                                        (Just "password incorrect")
                                        (Nothing :: Maybe ())
  }, Nothing)
getValidAccount (Left e) = Left $ (fmap . fmap) show e

type AuthSignOutAPI = "sign_out" :> ( Header "Content-Type" String :>
                                      AuthProtect "cookie-auth" :>
                                      UVerb 'POST '[JSON] [ WithStatus 200 (Headers '[ Header "Set-Cookie" SetCookie
                                                                                     ] (GeneralResponse A.Value)
                                                                           )
                                                          , WithStatus 303 (Headers '[ Header "Set-Cookie" SetCookie
                                                                                     , Header "Location" TL.Text
                                                                                     ] NoContent
                                                                           )
                                                          ]
                                    )

authSignOutHandler :: Maybe String
                   -> Either ServerError SessionData
                   -> ReaderHandler (Union '[ WithStatus 200 ( Headers '[ Header "Set-Cookie" SetCookie
                                                                        ] (GeneralResponse A.Value)
                                                             )
                                            , WithStatus 303 ( Headers '[ Header "Set-Cookie" SetCookie
                                                                        , Header "Location" TL.Text
                                                                        ] NoContent
                                                             )
                                            ])
authSignOutHandler _ (Left err) = throwError err
authSignOutHandler contentType (Right (SessionData sid _)) = do
  pool <- asks getPool
  dbResult <- liftIO $ use pool $ deleteSessionSession $ cs sid
  case dbResult of
    Left err -> do liftIO $ print err; throwError unknownError
    Right _  -> case getMediaType contentType of
      FormBody -> respond $ WithStatus @303 $
                            ( addHeader deleteSessionCookieSetting $
                              addHeader ("/sign_in" :: TL.Text)
                              NoContent
                              :: Headers '[ Header "Set-Cookie" SetCookie
                                          , Header "Location" TL.Text
                                          ] NoContent
                            )
      _        -> respond $ WithStatus @200 $
                            ( addHeader deleteSessionCookieSetting $
                              GeneralResponse Ok Nothing Nothing
                              :: Headers '[Header "Set-Cookie" SetCookie] (GeneralResponse A.Value)
                            )

