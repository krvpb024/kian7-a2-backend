{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Route.Account (AccountAPI, accountServerReader) where

import           Conf                             (Env (getPool))
import           Control.Monad.Trans.Reader       (Reader, asks)
import           Network.Wai                      (Request)

import           Control.Monad                    (when)
import           Control.Monad.Extra              (unless)
import           Control.Monad.IO.Class           (MonadIO (liftIO))
import qualified Data.Aeson                       as A
import           Data.Either.Extra                (isLeft, maybeToEither)
import           Data.Int                         (Int32)
import           Data.Maybe                       (fromJust, isJust)
import           Data.Password.Argon2             (PasswordHash (unPasswordHash),
                                                   hashPassword, mkPassword)
import           Data.String.Conversions          (cs)
import qualified Data.Text.Lazy                   as TL
import           Data.Tuple.Extra                 (uncurry3)
import           Db.Session                       (deleteAccountByIdSession,
                                                   selectAccountByIdSession,
                                                   updateAccountNameSession,
                                                   updateAccountPasswordSession)
import           Hasql.Pool                       (Pool,
                                                   UsageError (SessionUsageError),
                                                   use)
import           Hasql.Session                    (CommandError (ResultError),
                                                   QueryError (QueryError),
                                                   ResultError (ServerError))
import           Route.Auth                       (getCheckPwResult,
                                                   getValidAccount)
import           Servant                          (AuthProtect, Get,
                                                   HasServer (ServerT, hoistServerWithContext),
                                                   Header, Headers, JSON,
                                                   Proxy (Proxy), ReqBody,
                                                   Server, ServerError,
                                                   StdMethod (DELETE, PATCH),
                                                   Verb, addHeader, noHeader,
                                                   throwError, (:<|>) ((:<|>)),
                                                   (:>))
import           Servant.Auth.Server              (SetCookie)
import           Servant.Server.Experimental.Auth (AuthHandler)
import           Types.Account                    (AccountNamePatch (newAccountName),
                                                   AccountPasswordPatch (AccountPasswordPatch),
                                                   AccountTable (AccountTable),
                                                   AccountValue (AccountValue))
import           Types.Auth                       (SessionData (SessionData))
import           Utils.AuthContext                (deleteSessionCookieSetting)
import           Utils.ErrorResponse              (accountHasBeenTakenError,
                                                   passwordConfirmNotPassError,
                                                   unknownError)
import           Utils.HandleCsv                  (deleteAccountCsvFile)
import           Utils.ReaderHandler              (ReaderHandler,
                                                   readerToHandler)
import           Utils.ResponseType               (GeneralResponse (GeneralResponse),
                                                   Status (Ok))

accountServerReader :: Reader Env (Server AccountAPI)
accountServerReader = asks $ \env ->
  hoistServerWithContext accountAPI
                         (Proxy :: Proxy '[AuthHandler Request (Either ServerError SessionData)])
                         (readerToHandler env)
                         accountServerT

accountServerT :: ServerT AccountAPI ReaderHandler
accountServerT = accountGetHandler
            :<|> accountDeleteHandler
            :<|> accountNamePatchJSONHandler
            :<|> accountPasswordPatchJSONHandler

accountAPI :: Proxy AccountAPI
accountAPI = Proxy

type AccountAPI = "account" :> (AccountGetAPI
                           :<|> AccountDeleteAPI
                           :<|> AccountNamePatchAPI
                           :<|> AccountPasswordPatchAPI)

type AccountGetAPI = AuthProtect "cookie-auth" :>
                     Get '[JSON] (Headers '[ Header "Set-Cookie" SetCookie ] (GeneralResponse AccountValue))
accountGetHandler :: Either ServerError SessionData
                  -> ReaderHandler (Headers '[ Header "Set-Cookie" SetCookie ] (GeneralResponse AccountValue))
accountGetHandler (Left err)  = throwError err
accountGetHandler (Right (SessionData _ accountId)) = do
  pool <- asks getPool
  selectAccountResult <- selectAccount pool accountId
  case selectAccountResult of
    Left (serverError, maybeErrMsg) -> do
      when (isJust maybeErrMsg) $ liftIO $ print $ fromJust maybeErrMsg
      throwError serverError
    Right Nothing -> return $ addHeader deleteSessionCookieSetting $
                              GeneralResponse Ok Nothing Nothing
    Right (Just (accId, accName, _)) ->  return $ noHeader $ GeneralResponse Ok
                                                          Nothing $
                                                          return $ AccountValue accId $
                                                                                cs accName

type AccountDeleteAPI = AuthProtect "cookie-auth" :>
                        Verb 'DELETE 200 '[JSON] (Headers '[ Header "Set-Cookie" SetCookie ]
                                                           (GeneralResponse A.Value)
                                                 )

accountDeleteHandler :: Either ServerError SessionData
                     -> ReaderHandler (Headers '[ Header "Set-Cookie" SetCookie ]
                                                (GeneralResponse A.Value)
                                      )
accountDeleteHandler (Left err) = throwError err
accountDeleteHandler (Right (SessionData _ accountId)) = do
  pool <- asks getPool
  deleteAccountResult <- deleteAccount pool accountId
  deleteAccountCsvFileResult <- liftIO $ deleteAccountCsvFile accountId
  case deleteAccountResult *> deleteAccountCsvFileResult of
    Left (serverError, maybeErrMsg) -> do
      when (isJust maybeErrMsg) $ liftIO $ print $ fromJust maybeErrMsg
      throwError serverError
    Right _ -> do
      return $ addHeader deleteSessionCookieSetting $ GeneralResponse Ok Nothing Nothing
  where
    deleteAccount :: (MonadIO m) => Pool
                                 -> Int32
                                 -> m (Either (ServerError, Maybe String) ())
    deleteAccount pl accId = do
      eitherAccount <- liftIO $ use pl $ deleteAccountByIdSession accId
      return $ case eitherAccount of
        Right _  -> Right ()
        Left err -> Left (unknownError, Just $ show err)

type AccountNamePatchAPI = "name" :>
                            AuthProtect "cookie-auth" :>
                            ReqBody '[JSON] AccountNamePatch :>
                            Verb 'PATCH 200 '[JSON] (GeneralResponse A.Value)

accountNamePatchJSONHandler :: Either ServerError SessionData
                            -> AccountNamePatch
                            -> ReaderHandler (GeneralResponse A.Value)
accountNamePatchJSONHandler (Left err) _ = throwError err
accountNamePatchJSONHandler (Right (SessionData _ accountId)) account = do
  pool <- asks getPool
  updateAccountNameResult <- updateAccountName pool accountId $ newAccountName account
  case updateAccountNameResult of
    Left (serverError, maybeErrMsg) -> do
      when (isJust maybeErrMsg) $ liftIO $ print $ fromJust maybeErrMsg
      throwError serverError
    Right _ -> do
      return $ GeneralResponse Ok Nothing Nothing
  where
    updateAccountName :: (MonadIO m) => Pool
                                     -> Int32
                                     -> TL.Text
                                     -> m (Either (ServerError, Maybe String) ())
    updateAccountName pl accId newAccName = do
      eitherAccount <- liftIO $ use pl $ updateAccountNameSession accId $ cs newAccName
      return $ case eitherAccount of
        Right _  -> Right ()
        Left (SessionUsageError (QueryError _ _ (ResultError (ServerError "23505" _ _ _ _)))) ->
          Left (accountHasBeenTakenError, Nothing)
        Left err -> Left (unknownError, Just $ show err)

type AccountPasswordPatchAPI = "password" :>
                               AuthProtect "cookie-auth" :>
                               ReqBody '[JSON] AccountPasswordPatch :>
                               Verb 'PATCH 200 '[JSON] (GeneralResponse A.Value)

accountPasswordPatchJSONHandler :: Either ServerError SessionData
                                -> AccountPasswordPatch
                                -> ReaderHandler (GeneralResponse A.Value)
accountPasswordPatchJSONHandler (Left err) _ = throwError err
accountPasswordPatchJSONHandler (Right (SessionData _ accountId))
                                (AccountPasswordPatch accountPasswordOld
                                                      accountPasswordNew
                                                      accountPasswordNewConfirm) = do
  pool <- asks getPool
  unless (accountPasswordNew == accountPasswordNewConfirm) $
         throwError passwordConfirmNotPassError

  eitherAccount <- selectAccount pool accountId
  when (isLeft eitherAccount) $ do
    liftIO $ print eitherAccount
    throwError unknownError

  let eitherValidAccount = getValidAccount <$>
                           getCheckPwResult accountPasswordOld $
                                            (uncurry3 AccountTable <$> (maybeToEither (unknownError, Just "account not found") =<<
                                                                                      eitherAccount))
  hashedPassword <- hashPassword $ mkPassword $ cs accountPasswordNew
  updateAccountPasswordResult <- updateAccountPassword pool (cs $ unPasswordHash hashedPassword) eitherValidAccount
  case updateAccountPasswordResult of
    Left (serverError, maybeErrMsg) -> do
      when (isJust maybeErrMsg) $ liftIO $ print $ fromJust maybeErrMsg
      throwError serverError
    Right _ -> do
      return $ GeneralResponse Ok Nothing Nothing
  where
    updateAccountPassword :: (MonadIO m) => Pool
                                         -> TL.Text
                                         -> Either (ServerError, Maybe String) AccountTable
                                         -> m (Either (ServerError, Maybe String) ())
    updateAccountPassword _  _              (Left err) = return $ Left err
    updateAccountPassword pl newAccPassword (Right (AccountTable accId _ _)) = do
      eitherAccount <- liftIO $ use pl $ updateAccountPasswordSession accId $ cs newAccPassword
      return $ case eitherAccount of
        Right _  -> Right ()
        Left err -> Left (unknownError, Just $ show err)

selectAccount :: MonadIO m => Pool
                           -> Int32
                           -> m (Either (ServerError, Maybe String) (Maybe (Int32, TL.Text, TL.Text)))
selectAccount pl accId = do
  eitherAccount <- liftIO $ use pl $ selectAccountByIdSession accId
  return $ case eitherAccount of
    Left err       -> Left (unknownError, Just $ show err)
    Right Nothing -> Right Nothing
    Right (Just (aId, aName, aPassword)) -> Right (Just (aId, cs aName, cs aPassword))
