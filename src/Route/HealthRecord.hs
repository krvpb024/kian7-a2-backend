{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Route.HealthRecord (healthRecordServerReader, HealthRecordAPI) where

import           Conf                             (Env)
import           Control.Monad                    (unless, when)
import           Control.Monad.IO.Class           (MonadIO (liftIO))
import           Control.Monad.Trans.Reader       (Reader, asks)
import qualified Data.Aeson                       as A
import qualified Data.Csv                         as C
import           Data.Functor                     ((<&>))
import qualified Data.HashMap.Lazy                as HL
import           Data.Maybe                       (fromJust, fromMaybe, isJust)
import           Data.Ord                         (comparing)
import           Data.String.Conversions          (cs)
import           Data.Time                        (Day, UTCTime (utctDay),
                                                   fromGregorian,
                                                   getCurrentTime, toGregorian)
import           Data.Time.Format.ISO8601         (iso8601Show)
import           Data.Tuple.Extra                 (uncurry3)
import           Data.Vector                      (Vector, modify)
import qualified Data.Vector                      as V
import qualified Data.Vector.Algorithms.Intro     as VA
import           Network.Wai                      (Request)
import           Route.Profile                    (getProfileData)
import           Servant                          (AuthProtect, Capture, Get,
                                                   HasServer (ServerT, hoistServerWithContext),
                                                   JSON, Proxy (Proxy),
                                                   QueryParam, ReqBody, Server,
                                                   ServerError (errBody, errHTTPCode),
                                                   throwError, (:<|>) ((:<|>)),
                                                   (:>))
import           Servant.API.Verbs                (Delete, PostCreated, Put)
import           Servant.Multipart                (Mem, MultipartForm)
import           Servant.Server.Experimental.Auth (AuthHandler)
import           System.Directory                 (doesFileExist)
import           Types.Auth                       (SessionData (SessionData))
import           Types.Chart                      (Chart, genChart)
import           Types.HealthIndicator            (HealthIndicator,
                                                   getHealthIndicatorNormalRange,
                                                   readIndicatorJson)
import           Types.HealthRecord               (HealthRecordTable (healthRecordDate))
import           Types.Profile                    (ProfileTable (profileTableBirthDate, profileTableGender),
                                                   ageFromTo)
import           Utils.CombinatorType             (MaybeQueryParam (getMaybeQueryParam))
import           Utils.ErrorResponse              (haveNotCreateProfileError,
                                                   jsonErr, jsonErr400)
import           Utils.HandleCsv                  (FileType (HealthRecord, Profile),
                                                   createCsvFile,
                                                   getCsvFilePath,
                                                   parseByteStringToCsv,
                                                   readCsv, writeCsvToFile)
import           Utils.ReaderHandler              (ReaderHandler,
                                                   readerToHandler)
import           Utils.ResponseType               (CSV,
                                                   GeneralResponse (GeneralResponse),
                                                   HealthRecordCSV (HealthRecordCSV),
                                                   Status (Error, Ok))

healthRecordServerReader :: Reader Env (Server HealthRecordAPI)
healthRecordServerReader = asks $ \env ->
  hoistServerWithContext healthRecordAPI
                         (Proxy :: Proxy '[AuthHandler Request (Either ServerError SessionData)])
                         (readerToHandler env)
                         healthRecordServerT

healthRecordServerT :: ServerT HealthRecordAPI ReaderHandler
healthRecordServerT = (healthRecordPostHandler
                  :<|> healthRecordPutHandler
                  :<|> healthRecordDeleteHandler)
                 :<|> (healthRecordsGetHandler
                  :<|> healthRecordsPostHandler
                  :<|> healthRecordsChartGetHandler)

healthRecordAPI :: Proxy HealthRecordAPI
healthRecordAPI = Proxy

type HealthRecordAPI = "health_record" :> (HealthRecordPostAPI
                                      :<|> HealthRecordPutAPI
                                      :<|> HealthRecordDeleteAPI)
                  :<|> "health_records" :> (HealthRecordsGetAPI
                                       :<|> HealthRecordsPostAPI
                                       :<|> HealthRecordsChartGetAPI)

type HealthRecordPostAPI = AuthProtect "cookie-auth" :>
                           ReqBody '[JSON] HealthRecordTable :>
                           PostCreated '[JSON] (GeneralResponse ())

healthRecordPostHandler :: Either ServerError SessionData
                        -> HealthRecordTable
                        -> ReaderHandler (GeneralResponse ())
healthRecordPostHandler (Left err) _ = throwError err
healthRecordPostHandler (Right (SessionData _ accountId)) healthRecordData = do
  profileCsvFilePath <- liftIO $ getCsvFilePath (show accountId) Profile
  isProfileFileExist <- liftIO $ doesFileExist profileCsvFilePath
  unless isProfileFileExist $ throwError haveNotCreateProfileError

  healthRecordCsvFilePath <- liftIO $ getCsvFilePath (show accountId) HealthRecord
  isHealthRecordFileExist <- liftIO $ doesFileExist healthRecordCsvFilePath
  liftIO $ unless isHealthRecordFileExist $ createCsvFile (show accountId) HealthRecord

  rawCsvByteString <- liftIO $ readCsv (show accountId) HealthRecord
  let insertedCsvData = rawCsvByteString >>=
                        parseByteStringToCsv >>=
                        checkRecordDateNotDuplicate (healthRecordDate healthRecordData) >>=
                        insertHealthRecord healthRecordData
  saveCsvFileResult <- liftIO $ writeCsvToFile healthRecordCsvFilePath insertedCsvData
  case saveCsvFileResult of
    Left (serverError, maybeErrMsg) -> do
      when (isJust maybeErrMsg) $ liftIO $ putStrLn $ fromJust maybeErrMsg
      throwError serverError
    Right _ -> do
      return $ GeneralResponse Ok Nothing Nothing
  where
    checkRecordDateNotDuplicate :: Day
                                -> (C.Header, Vector HealthRecordTable)
                                -> Either (ServerError, Maybe String)
                                          (C.Header, Vector HealthRecordTable)
    checkRecordDateNotDuplicate recordDate healthRecordCsv@(_, healthRecords) = do
      let maybeDuplicate = V.find ((== recordDate) . healthRecordDate) healthRecords
      let isoFormatRecordDate = cs $ iso8601Show recordDate
      case maybeDuplicate of
        Nothing -> return healthRecordCsv
        Just _ -> Left (jsonErr400 { errBody = A.encode $
                                               GeneralResponse Error
                                                               (Just $ "record on " <>
                                                                       isoFormatRecordDate <>
                                                                       " already exist")
                                                               (Nothing :: Maybe ()) }
                       , Nothing)

    insertHealthRecord :: HealthRecordTable
                       -> (C.Header, Vector HealthRecordTable)
                       -> Either (ServerError, Maybe String)
                                 (C.Header, Vector HealthRecordTable)
    insertHealthRecord newHealthRecordData record = Right $ (`V.snoc` newHealthRecordData) <$> record

type HealthRecordsGetAPI = AuthProtect "cookie-auth" :>
                           QueryParam "from" (MaybeQueryParam Day) :>
                           QueryParam "to" (MaybeQueryParam Day) :>
                           Get '[JSON, CSV] (GeneralResponse HealthRecordCSV)

healthRecordsGetHandler :: Either ServerError SessionData
                        -> Maybe (MaybeQueryParam Day)
                        -> Maybe (MaybeQueryParam Day)
                        -> ReaderHandler (GeneralResponse HealthRecordCSV)
healthRecordsGetHandler (Left err) _ _ = throwError err
healthRecordsGetHandler (Right (SessionData _ accountId))
                        maybeFilterFrom
                        maybeFilterTo = do
  today <- liftIO $ toGregorian . utctDay <$> getCurrentTime
  let (filterFrom, filterTo) = extractDefaultFilter (uncurry3 fromGregorian today) maybeFilterFrom maybeFilterTo
  profileCsvFilePath <- liftIO $ getCsvFilePath (show accountId) Profile
  isProfileFileExist <- liftIO $ doesFileExist profileCsvFilePath
  unless isProfileFileExist $ throwError haveNotCreateProfileError

  healthRecordCsvFilePath <- liftIO $ getCsvFilePath (show accountId) HealthRecord
  isHealthRecordFileExist <- liftIO $ doesFileExist healthRecordCsvFilePath
  liftIO $ unless isHealthRecordFileExist $ createCsvFile (show accountId) HealthRecord

  rawCsvByteString <- liftIO $ readCsv (show accountId) HealthRecord
  let healthRecordCsvData = (rawCsvByteString >>=
                            parseByteStringToCsv >>=
                            filterHealthRecord filterFrom filterTo) <&>
                            (modify (VA.sortBy $ flip $ comparing healthRecordDate) <$>)
  case healthRecordCsvData of
    Left (serverError, maybeErrMsg) -> do
      when (isJust maybeErrMsg) $ liftIO $ putStrLn $ fromJust maybeErrMsg
      throwError serverError
    Right healthRecords -> do
      return $ GeneralResponse Ok
                               Nothing $
                               Just $ HealthRecordCSV healthRecords

type HealthRecordPutAPI = AuthProtect "cookie-auth" :>
                          ReqBody '[JSON] HealthRecordTable :>
                          Put '[JSON] (GeneralResponse HealthRecordCSV)

healthRecordPutHandler :: Either ServerError SessionData
                       -> HealthRecordTable
                       -> ReaderHandler (GeneralResponse HealthRecordCSV)
healthRecordPutHandler (Left err) _ = throwError err
healthRecordPutHandler (Right (SessionData _ accountId)) healthRecordData = do
  profileCsvFilePath <- liftIO $ getCsvFilePath (show accountId) Profile
  isProfileFileExist <- liftIO $ doesFileExist profileCsvFilePath
  unless isProfileFileExist $ throwError haveNotCreateProfileError

  healthRecordCsvFilePath <- liftIO $ getCsvFilePath (show accountId) HealthRecord
  isHealthRecordFileExist <- liftIO $ doesFileExist healthRecordCsvFilePath
  liftIO $ unless isHealthRecordFileExist $ createCsvFile (show accountId) HealthRecord

  rawCsvByteString <- liftIO $ readCsv (show accountId) HealthRecord
  let updatedCsvData = rawCsvByteString >>=
                       parseByteStringToCsv >>=
                       checkRowOnDateExist (healthRecordDate healthRecordData) >>=
                       updateHealthRecordByDate healthRecordData
  saveCsvFileResult <- liftIO $ writeCsvToFile healthRecordCsvFilePath updatedCsvData
  case saveCsvFileResult of
    Left (serverError, maybeErrMsg) -> do
      when (isJust maybeErrMsg) $ liftIO $ putStrLn $ fromJust maybeErrMsg
      throwError serverError
    Right _ -> do
      return $ GeneralResponse Ok Nothing Nothing
  where
    updateHealthRecordByDate :: HealthRecordTable
                             -> (C.Header, Vector HealthRecordTable, Int)
                             -> Either (ServerError, Maybe String)
                                       (C.Header, Vector HealthRecordTable)
    updateHealthRecordByDate updateData (header, healthRecords, index) =
      return (header, healthRecords V.// [(index, updateData)])

type HealthRecordsPostAPI = AuthProtect "cookie-auth" :>
                            MultipartForm Mem HealthRecordCSV :>
                            PostCreated '[JSON] (GeneralResponse ())

healthRecordsPostHandler :: Either ServerError SessionData
                         -> HealthRecordCSV
                         -> ReaderHandler (GeneralResponse ())
healthRecordsPostHandler (Left err) _ = throwError err
healthRecordsPostHandler (Right (SessionData _ accountId)) (HealthRecordCSV newHealthRecord) = do
  profileCsvFilePath <- liftIO $ getCsvFilePath (show accountId) Profile
  isProfileFileExist <- liftIO $ doesFileExist profileCsvFilePath
  unless isProfileFileExist $ throwError haveNotCreateProfileError

  healthRecordCsvFilePath <- liftIO $ getCsvFilePath (show accountId) HealthRecord
  isHealthRecordFileExist <- liftIO $ doesFileExist healthRecordCsvFilePath
  liftIO $ unless isHealthRecordFileExist $ createCsvFile (show accountId) HealthRecord

  rawCsvByteString <- liftIO $ readCsv (show accountId) HealthRecord
  let updatedCsvData = rawCsvByteString >>=
                       parseByteStringToCsv >>=
                       upsertHealthRecord newHealthRecord

  saveCsvFileResult <- liftIO $ writeCsvToFile healthRecordCsvFilePath updatedCsvData
  case saveCsvFileResult of
    Left (serverError, maybeErrMsg) -> do
      when (isJust maybeErrMsg) $ liftIO $ putStrLn $ fromJust maybeErrMsg
      throwError serverError
    Right _ -> do
      return $ GeneralResponse Ok Nothing Nothing
  where
    upsertHealthRecord :: (C.Header, Vector HealthRecordTable)
                       -> (C.Header, Vector HealthRecordTable)
                       -> Either (ServerError, Maybe String)
                                 (C.Header, Vector HealthRecordTable)
    upsertHealthRecord (_, nhr) (h, chr) = return $
      hashMapToVector <$> (h, HL.union (vectorToHashMap nhr)
                                       (vectorToHashMap chr))

    vectorToHashMap :: Vector HealthRecordTable
                    -> HL.HashMap Day HealthRecordTable
    vectorToHashMap = HL.fromList . fmap (\hr -> (healthRecordDate hr, hr)) . V.toList

    hashMapToVector :: HL.HashMap Day HealthRecordTable
                    -> Vector HealthRecordTable
    hashMapToVector = (V.fromList . fmap snd) . HL.toList

type HealthRecordsChartGetAPI = AuthProtect "cookie-auth" :>
                                "chart" :>
                                Capture "type" HealthIndicator :>
                                QueryParam "dataDistanceWidth" Double :>
                                QueryParam "height" Double :>
                                QueryParam "from" (MaybeQueryParam Day) :>
                                QueryParam "to" (MaybeQueryParam Day) :>
                                Get '[JSON] (GeneralResponse Chart)

healthRecordsChartGetHandler :: Either ServerError SessionData
                             -> HealthIndicator
                             -> Maybe Double
                             -> Maybe Double
                             -> Maybe (MaybeQueryParam Day)
                             -> Maybe (MaybeQueryParam Day)
                             -> ReaderHandler (GeneralResponse Chart)
healthRecordsChartGetHandler (Left err) _ _ _ _ _ = throwError err
healthRecordsChartGetHandler (Right (SessionData _ accountId))
                              indicatorType
                              dataDistanceWidth
                              chartHeight
                              maybeFilterFrom
                              maybeFilterTo = do
  today <- liftIO $ toGregorian . utctDay <$> getCurrentTime
  let (filterFrom, filterTo) = extractDefaultFilter (uncurry3 fromGregorian today)
                                                    maybeFilterFrom
                                                    maybeFilterTo
  isProfileFileExist <- liftIO $ getCsvFilePath (show accountId) Profile >>=
                                 doesFileExist
  unless isProfileFileExist $ throwError haveNotCreateProfileError

  profileRawCsvByteString <- liftIO $ readCsv (show accountId) Profile
  let profileData = profileRawCsvByteString >>=
                    parseByteStringToCsv >>=
                    getProfileData
  indicatorJson <- readIndicatorJson indicatorType

  let profileAgeKey = ageFromTo today . toGregorian . profileTableBirthDate <$> profileData
      profileGender = profileTableGender <$> profileData

  let healthIndicatorNormalRange = (getHealthIndicatorNormalRange <$> profileGender) <*>
                                   profileAgeKey <*>
                                   pure indicatorJson
  healthRecordCsvFilePath <- liftIO $ getCsvFilePath (show accountId) HealthRecord
  isHealthRecordFileExist <- liftIO $ doesFileExist healthRecordCsvFilePath
  liftIO $ unless isHealthRecordFileExist $ createCsvFile (show accountId) HealthRecord

  rawCsvByteString <- liftIO $ readCsv (show accountId) HealthRecord
  let healthRecordCsvData = (rawCsvByteString >>=
                            parseByteStringToCsv >>=
                            filterHealthRecord filterFrom filterTo) <&>
                            snd
  let eitherChart = genChart (fromMaybe 50  dataDistanceWidth)
                             (fromMaybe 300 chartHeight)
                             filterFrom
                             filterTo
                             indicatorType <$>
                    healthIndicatorNormalRange <*>
                    healthRecordCsvData
  case eitherChart of
    Left (serverError, maybeErrMsg) -> do
      when (isJust maybeErrMsg) $ liftIO $ putStrLn $ fromJust maybeErrMsg
      throwError serverError
    Right chart -> do
      return $ GeneralResponse Ok
                               Nothing
                               chart

type HealthRecordDeleteAPI = AuthProtect "cookie-auth" :>
                             Capture "date" Day :>
                             Delete '[JSON] (GeneralResponse HealthRecordCSV)

healthRecordDeleteHandler :: Either ServerError SessionData
                          -> Day
                          -> ReaderHandler (GeneralResponse HealthRecordCSV)
healthRecordDeleteHandler (Left err) _ = throwError err
healthRecordDeleteHandler (Right (SessionData _ accountId)) date = do
  profileCsvFilePath <- liftIO $ getCsvFilePath (show accountId) Profile
  isProfileFileExist <- liftIO $ doesFileExist profileCsvFilePath
  unless isProfileFileExist $ throwError haveNotCreateProfileError

  healthRecordCsvFilePath <- liftIO $ getCsvFilePath (show accountId) HealthRecord
  isHealthRecordFileExist <- liftIO $ doesFileExist healthRecordCsvFilePath
  liftIO $ unless isHealthRecordFileExist $ createCsvFile (show accountId) HealthRecord

  rawCsvByteString <- liftIO $ readCsv (show accountId) HealthRecord
  let deletedCsvData = rawCsvByteString >>=
                       parseByteStringToCsv >>=
                       checkRowOnDateExist date >>=
                       deleteHealthRecord
  saveCsvFileResult <- liftIO $ writeCsvToFile healthRecordCsvFilePath deletedCsvData
  case saveCsvFileResult of
    Left (serverError, maybeErrMsg) -> do
      when (isJust maybeErrMsg) $ liftIO $ putStrLn $ fromJust maybeErrMsg
      throwError serverError
    Right _ -> do
      return $ GeneralResponse Ok Nothing Nothing
  where
    deleteHealthRecord :: (C.Header, Vector HealthRecordTable, Int)
                             -> Either (ServerError, Maybe String)
                                       (C.Header, Vector HealthRecordTable)
    deleteHealthRecord (header, healthRecords, index) =
      return (header, V.ifilter (\i _ -> i /= index) healthRecords)

checkRowOnDateExist :: Day
                    -> (C.Header, Vector HealthRecordTable)
                    -> Either (ServerError, Maybe String)
                              (C.Header, Vector HealthRecordTable, Int)
checkRowOnDateExist updateDate (header, healthRecords) = do
  let maybeRow = V.findIndex (\h -> updateDate == healthRecordDate h) healthRecords
  let isoFormatRecordDate = cs $ iso8601Show updateDate
  case maybeRow of
    Nothing -> Left (jsonErr { errHTTPCode = 404
                             , errBody = A.encode $
                                         GeneralResponse Error
                                                         (Just $ "health record on " <>
                                                                 isoFormatRecordDate <>
                                                                 " not found")
                                                         (Nothing :: Maybe HealthRecordCSV)
                             }, Nothing)
    Just i -> return (header, healthRecords, i)

extractDefaultFilter :: Day
                     -> Maybe (MaybeQueryParam Day)
                     -> Maybe (MaybeQueryParam Day)
                     -> (Maybe Day, Maybe Day)
extractDefaultFilter today maybeFilterFrom maybeFilterTo =
  (getMaybeQueryParam =<< maybeFilterFrom, return $ fromMaybe today $ getMaybeQueryParam =<< maybeFilterTo)

filterHealthRecord :: Maybe Day
                   -> Maybe Day
                   -> (C.Header, Vector HealthRecordTable)
                   -> Either (ServerError, Maybe String)
                             (C.Header, Vector HealthRecordTable)
filterHealthRecord Nothing Nothing healthRecords = return healthRecords
filterHealthRecord (Just from) Nothing healthRecords = return $
  V.filter ((from <=) . healthRecordDate) <$> healthRecords
filterHealthRecord Nothing (Just to) healthRecords = return $
  V.filter ((to >=) . healthRecordDate) <$> healthRecords
filterHealthRecord (Just from) (Just to) healthRecords = return $
  V.filter (\r -> (from <= healthRecordDate r) && (to >= healthRecordDate r)) <$> healthRecords
