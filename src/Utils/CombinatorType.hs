{-# LANGUAGE DeriveGeneric #-}

module Utils.CombinatorType (MaybeQueryParam (MaybeQueryParam), getMaybeQueryParam) where

import qualified Data.Text                as TS
import           GHC.Generics             (Generic)
import           Text.Read                (readMaybe)
import           Web.HttpApiData          (FromHttpApiData)
import           Web.Internal.HttpApiData (FromHttpApiData (parseQueryParam))

-- need this newtype because empty input field will get empty String
-- FromHttpApiData Maybe instance will throw error when encounter empty String from form
-- need newtype to change the behavior to handle empty input
newtype MaybeQueryParam a = MaybeQueryParam {
  getMaybeQueryParam :: Maybe a
} deriving (Eq, Show, Generic)

instance (FromHttpApiData a, Read a) => FromHttpApiData (MaybeQueryParam a) where
  parseQueryParam = Right . MaybeQueryParam . readMaybe . TS.unpack
