{-# LANGUAGE DataKinds      #-}
{-# LANGUAGE KindSignatures #-}

module Utils.ReaderHandler (ReaderHandler, readerToHandler) where

import           Conf                       (Env)
import           Control.Monad.Trans.Reader (ReaderT (runReaderT))
import           Servant                    (Handler)

type ReaderHandler = ReaderT Env Handler

readerToHandler :: Env -> ReaderHandler a -> Handler a
readerToHandler env reader = runReaderT reader env
