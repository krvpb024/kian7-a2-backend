{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric  #-}

module Utils.HandleCsv (readCsv, parseByteStringToCsv, FileType (Profile, HealthRecord), createCsvFile, getCsvFilePath, fileNotFoundErr, csvEmptyError, writeCsvToFile, deleteAccountCsvFile) where

import           Control.Exception       (try)
import           Control.Monad.IO.Class  (MonadIO (liftIO))
import           Data.Aeson              (ToJSON, encode)
import qualified Data.ByteString.Lazy    as BL
import           Data.Csv                (FromNamedRecord, Header,
                                          ToNamedRecord, decodeByName,
                                          encodeByName)
import           Data.Int                (Int32)
import           Data.String.Conversions (cs)
import           Data.Vector             (Vector, toList)
import           GHC.Generics            (Generic)
import           GHC.IO.Exception        (IOErrorType (NoSuchThing),
                                          IOException (IOError))
import           Servant                 (ServerError (errBody, errHTTPCode))
import           System.Directory        (copyFile, createDirectoryIfMissing,
                                          removeDirectoryRecursive)
import           System.Environment      (getEnv)
import           Utils.ErrorResponse     (jsonErr, unknownError)
import           Utils.ResponseType      (GeneralResponse (GeneralResponse),
                                          Status (Error))

getCsvDirPath :: String -> IO FilePath
getCsvDirPath accountId = do
  dataDir <- getEnv "DATA_DIR"
  return $ dataDir <> "/" <> accountId

getCsvFilePath :: String -> FileType -> IO FilePath
getCsvFilePath accountId fileType = do
  userDataDir <- getCsvDirPath accountId
  return $ userDataDir <> getFileTypeFileName fileType

createCsvFile :: String -> FileType -> IO ()
createCsvFile accountId fileType = do
  csvDirPath <- getCsvDirPath accountId
  csvFilePath <- getCsvFilePath accountId fileType
  liftIO $ createDirectoryIfMissing True csvDirPath
  liftIO $ copyFile ("./csv_template" <> getFileTypeFileName fileType) csvFilePath

readCsv :: String -> FileType -> IO (Either (ServerError, Maybe String) BL.ByteString)
readCsv accountId fileType = do
  csvFilePath <- getCsvFilePath accountId fileType
  readFileResult <- try (BL.readFile csvFilePath) :: IO (Either IOError BL.ByteString)
  return $ case readFileResult of
    Left (IOError _ NoSuchThing _ _ _ _) -> Left $ fileNotFoundErr fileType
    Left err                             -> Left (unknownError, Just $ show err)
    Right rawByteString                  -> Right rawByteString

deleteAccountCsvFile :: Int32 -> IO (Either (ServerError, Maybe String) ())
deleteAccountCsvFile accountId = do
  csvDirPath <- getCsvDirPath $ show accountId
  deleteAccountCsvFileResult <- try (removeDirectoryRecursive csvDirPath) :: IO (Either IOError ())
  return $ case deleteAccountCsvFileResult of
    Right ()                             -> Right ()
    Left (IOError _ NoSuchThing _ _ _ _) -> Right ()
    Left err                             -> Left (unknownError, Just $ show err)

parseByteStringToCsv :: (FromNamedRecord p) => BL.ByteString
                                            -> Either (ServerError, Maybe String) (Header, Vector p)
parseByteStringToCsv rawByteString = case decodeByName rawByteString of
  Right parsedCsv -> Right parsedCsv
  Left err        -> Left (unknownError, Just err)

data FileType = Profile
              | HealthRecord
              deriving (Eq, Show, Generic, ToJSON)

getFileTypeFileName :: FileType -> String
getFileTypeFileName Profile      = "/profile.csv"
getFileTypeFileName HealthRecord = "/health_record.csv"

fileNotFoundErr :: FileType -> (ServerError, Maybe String)
fileNotFoundErr fileType = (jsonErr
  { errHTTPCode = 404
  , errBody = encode $ GeneralResponse Error (Just $ cs $ show fileType <> " data not found") (Nothing :: Maybe ())
  }, Nothing)

csvEmptyError :: FileType -> (ServerError, Maybe String)
csvEmptyError fileType = (jsonErr { errHTTPCode = 404
                         , errBody = encode $ GeneralResponse Error (Just $  cs $ show fileType <> " data not found") (Nothing :: Maybe ())
                         }, Just $ "csv file exist but no " <> cs (show fileType))

writeCsvToFile :: (ToNamedRecord p) => FilePath
                                    -> Either (ServerError, Maybe String)
                                              (Header, Vector p)
                                    -> IO (Either (ServerError, Maybe String) ())
writeCsvToFile filePath csvData = case csvData of
  Left err -> return $ Left err
  Right (header, profiles) -> do
    ioResult <- try $ BL.writeFile filePath $
                                    encodeByName header $
                                    toList profiles :: IO (Either IOError ())
    case ioResult of
      Left err -> return $ Left (unknownError, Just $ show err)
      Right _  -> return $ Right ()
