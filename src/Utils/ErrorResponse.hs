{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}

module Utils.ErrorResponse (customFormatter, notFoundFormatter, customFormatters, unknownError, jsonErr400, jsonErr, haveNotCreateProfileError, accountHasBeenTakenError, passwordConfirmNotPassError, accountNotFoundError) where

import           Data.Aeson               (encode)
import           Data.String.Conversions  (cs)
import           Network.Wai              (rawPathInfo)
import           Servant                  (ErrorFormatter,
                                           ErrorFormatters (bodyParserErrorFormatter, notFoundErrorFormatter, urlParseErrorFormatter),
                                           JSON, NotFoundErrorFormatter,
                                           Proxy (Proxy),
                                           ServerError (errBody, errHTTPCode, errHeaders),
                                           defaultErrorFormatters, err400,
                                           err404, err500, getAcceptHeader)
import           Servant.API.ContentTypes (AllCTRender (handleAcceptH))
import           Utils.ResponseType       (GeneralResponse (GeneralResponse),
                                           Status (Error))

customFormatter :: ErrorFormatter
customFormatter _ req err =
  case handleAcceptH (Proxy :: Proxy '[JSON]) accH value of
    Nothing -> err400 { errBody = cs err }
    Just (cType, body) -> err400
      { errBody = body
      , errHeaders = [("Content-Type", cs cType)]
      }
  where value = GeneralResponse Error (Just $ cs err) (Nothing :: Maybe ())
        accH = getAcceptHeader req

notFoundFormatter :: NotFoundErrorFormatter
notFoundFormatter req =
  err404 { errBody = cs $ "Not found path: " <> rawPathInfo req }

customFormatters :: ErrorFormatters
customFormatters = defaultErrorFormatters
  { bodyParserErrorFormatter = customFormatter
  , notFoundErrorFormatter = notFoundFormatter
  , urlParseErrorFormatter = customFormatter
  }

jsonErr :: ServerError
jsonErr = err500
  { errHeaders = [("Content-Type", "application/json; charset=utf-8")]
  }

jsonErr400 :: ServerError
jsonErr400 = jsonErr { errHTTPCode = 400 }

jsonErr404 :: ServerError
jsonErr404 = jsonErr { errHTTPCode = 404 }

unknownError :: ServerError
unknownError = jsonErr
  { errBody = encode $ GeneralResponse Error (Just "unknown error") (Nothing :: Maybe ())
  }

haveNotCreateProfileError :: ServerError
haveNotCreateProfileError = jsonErr400
  { errBody = encode $ GeneralResponse Error (Just "haven't create profile") (Nothing :: Maybe ())
  }

accountHasBeenTakenError :: ServerError
accountHasBeenTakenError = jsonErr400
  { errBody = encode (GeneralResponse Error (Just "account has been taken") Nothing
                      :: GeneralResponse ())
  }

accountNotFoundError :: ServerError
accountNotFoundError = jsonErr404
  { errBody = encode (GeneralResponse Error (Just "account not found") Nothing
                      :: GeneralResponse ())
  }

passwordConfirmNotPassError :: ServerError
passwordConfirmNotPassError = jsonErr400
  { errBody = encode (GeneralResponse Error (Just "password confirm not pass") Nothing
                      :: GeneralResponse ())
  }
