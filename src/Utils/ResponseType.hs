{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}

module Utils.ResponseType (GeneralResponse (GeneralResponse), Status (Ok, Error), CSV (CSV), HealthRecordCSV (HealthRecordCSV), getHealthRecordCSV, csvMediaType, getMediaType, RequestBodyContentType(CsvBody, JsonBody, FormBody)) where

import           Control.Exception       (PatternMatchFail (PatternMatchFail),
                                          throw)
import qualified Data.Aeson              as A
import qualified Data.Csv                as C
import           Data.String.Conversions (cs)
import qualified Data.Text.Lazy          as TL
import qualified Data.Vector             as V
import           GHC.Generics            (Generic)
import qualified Network.HTTP.Media      as HM
import           Servant                 (Accept (contentType),
                                          MimeRender (mimeRender))
import           Servant.Multipart       (FileData (fdPayload),
                                          FromMultipart (fromMultipart), Mem,
                                          lookupFile)
import           Types.HealthRecord      (HealthRecordTable)

data CSV = CSV

instance Accept CSV where
  contentType _ = csvMediaType

instance MimeRender CSV (GeneralResponse HealthRecordCSV) where
  mimeRender _ (GeneralResponse _ _ (Just (HealthRecordCSV (h, r)))) = C.encodeByName h $ V.toList r
  mimeRender _ _ = throw $ PatternMatchFail "not valid HealthRecord CSV type"

csvMediaType :: HM.MediaType
csvMediaType = "text" HM.// "csv" HM./: ("charset", "utf-8")

jsonMediaType :: HM.MediaType
jsonMediaType = "application" HM.// "json" HM./: ("charset", "utf-8")

formMediaType :: HM.MediaType
formMediaType = "application" HM.// "x-www-form-urlencoded"

data RequestBodyContentType = CsvBody
                            | JsonBody
                            | FormBody

getMediaType :: Maybe String -> RequestBodyContentType
getMediaType mediaType
  | mediaTypeIs csvMediaType  mediaType = CsvBody
  | mediaTypeIs jsonMediaType mediaType = JsonBody
  | mediaTypeIs formMediaType mediaType = FormBody
  | otherwise                           = JsonBody
  where
    mediaTypeIs :: HM.MediaType -> Maybe String -> Bool
    mediaTypeIs mt inputMediaType = maybe False
                                          (HM.matches mt)
                                          ((HM.parseAccept . cs =<< inputMediaType)
                                            :: Maybe HM.MediaType)

data GeneralResponse p = GeneralResponse { status  :: Status
                                         , message :: Maybe TL.Text
                                         , payload :: Maybe p
                                         } deriving (Eq, Show, Generic)

instance (A.ToJSON p) => A.ToJSON (GeneralResponse p) where
  toJSON (GeneralResponse s m p) =
    A.object [ "status"  A..= s
             , "message" A..= m
             , "payload" A..= p
             ]

data Status = Ok | Error
  deriving (Eq, Show, Generic, A.ToJSON)

newtype HealthRecordCSV = HealthRecordCSV
  { getHealthRecordCSV :: (C.Header, V.Vector HealthRecordTable)
  }

instance A.ToJSON HealthRecordCSV where
  toJSON (HealthRecordCSV csv) = A.toJSON $ snd csv

instance FromMultipart Mem HealthRecordCSV where
  fromMultipart multipartData =
    HealthRecordCSV <$> ( C.decodeByName . fdPayload =<<
                          lookupFile "health_record_csv" multipartData )
