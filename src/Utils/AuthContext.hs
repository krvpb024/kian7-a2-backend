{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingStrategies    #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}

module Utils.AuthContext (authContext, deleteSessionCookieSetting, defaultSessionCookieSetting) where

import           Conf                             (Env (getSessionKey))
import           Data.Aeson                       (decode, encode)
import qualified Data.ByteString                  as BS
import           Data.Either.Extra                (maybeToEither)
import           Data.Int                         (Int32)
import           Data.String.Conversions          (cs)
import qualified Data.Text.Lazy.Encoding          as TLE
import           Data.Time                        (secondsToDiffTime)
import           Network.HTTP.Types               (HeaderName)
import           Network.URI.Encode               (decodeByteString)
import           Network.Wai                      (Request (requestHeaders))
import           Servant.Server                   (Handler,
                                                   ServerError (errBody, errHTTPCode, errHeaders))
import           Servant.Server.Experimental.Auth (AuthHandler, mkAuthHandler)
import           Types.Auth                       (SessionData (SessionData))
import           Utils.ErrorResponse              (jsonErr)
import           Utils.ResponseType               (GeneralResponse (GeneralResponse),
                                                   Status (Error))
import qualified Web.ClientSession                as ClientSession
import           Web.Cookie                       (SetCookie (setCookieHttpOnly, setCookieMaxAge, setCookieName, setCookiePath, setCookieSameSite, setCookieSecure, setCookieValue),
                                                   defaultSetCookie,
                                                   parseCookies,
                                                   renderSetCookieBS,
                                                   sameSiteStrict)


authContext :: Env -> AuthHandler Request (Either ServerError SessionData)
authContext env = mkAuthHandler authHandler
  where
    authHandler :: Request -> Handler (Either ServerError SessionData)
    authHandler req = return $
                      getSessionData =<<
                      decryptSessionId (getSessionKey env) =<<
                      getAuthCookie req
      where
        getSessionData :: (BS.ByteString, BS.ByteString)
                         -> Either ServerError SessionData
        getSessionData (sid, decryptedSid) = maybeToEither
                                sessionDecodeErr
                                (SessionData (TLE.decodeUtf8 $ cs sid) <$>
                                (decode (cs decryptedSid) :: Maybe Int32))
        decryptSessionId :: ClientSession.Key
                         -> BS.ByteString
                         -> Either ServerError (BS.ByteString, BS.ByteString)
        decryptSessionId _ "" = Left haveNotSignInErr
        decryptSessionId sessionKey sid = case ClientSession.decrypt sessionKey sid of
          Just decryptedSid -> Right (sid, decryptedSid)
          Nothing           -> Left sessionDecryptErr

        getAuthCookie :: Request -> Either ServerError BS.ByteString
        getAuthCookie r = maybeToEither
                            haveNotSignInErr $
                            fmap decodeByteString $
                              (lookup "servant-auth-cookie" . parseCookies) =<<
                              lookup "cookie" (requestHeaders r)

        haveNotSignInErr :: ServerError
        haveNotSignInErr = jsonErr { errHTTPCode = 401
                                   , errBody = encode $ GeneralResponse Error (Just "haven't sign in") (Nothing :: Maybe ())
                                   }
        sessionDecryptErr :: ServerError
        sessionDecryptErr = jsonErr { errHTTPCode = 401
                                    , errBody = encode $ GeneralResponse Error (Just "session key corrupted") (Nothing :: Maybe ())
                                    , errHeaders = [deleteSessionCookieHeader]
                                    }
        sessionDecodeErr :: ServerError
        sessionDecodeErr = jsonErr { errHTTPCode = 401
                                   , errBody = encode $ GeneralResponse Error (Just "session value corrupted") (Nothing :: Maybe ())
                                   , errHeaders = [deleteSessionCookieHeader]
                                   }

deleteSessionCookieHeader :: (HeaderName, BS.ByteString)
deleteSessionCookieHeader = ("Set-Cookie", renderSetCookieBS deleteSessionCookieSetting)

deleteSessionCookieSetting :: SetCookie
deleteSessionCookieSetting = defaultSessionCookieSetting
  { setCookieValue = mempty
  , setCookieMaxAge = Just $ secondsToDiffTime (-1)
  }

defaultSessionCookieSetting :: SetCookie
defaultSessionCookieSetting = defaultSetCookie
  { setCookieName     = "servant-auth-cookie"
  , setCookieHttpOnly = True
  , setCookieSecure   = True
  , setCookieSameSite = Just sameSiteStrict
  , setCookiePath     = Just "/"
  }
