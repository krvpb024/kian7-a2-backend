{-# LANGUAGE TypeOperators #-}

module Lib (startApp, app, getDbPool) where

import           Conf                       (Env (Env))
import           Configuration.Dotenv       (defaultConfig, loadFile)
import           Control.Monad.Trans.Reader (Reader, runReader)
import           Data.String.Conversions    (cs)
import           Hasql.Pool                 (Pool, acquire)
import           Network.Wai.Handler.Warp   (run)
import           Route.Account              (AccountAPI, accountServerReader)
import           Route.Auth                 (AuthAPI, authServerReader)
import           Route.HealthRecord         (HealthRecordAPI,
                                             healthRecordServerReader)
import           Route.Profile              (ProfileAPI, profileServerReader)
import           Servant                    (Application,
                                             Context (EmptyContext, (:.)),
                                             Proxy (Proxy), Server,
                                             serveWithContext,
                                             type (:<|>) ((:<|>)))
import           System.Environment         (getEnv)
import           Utils.AuthContext          (authContext)
import           Utils.ErrorResponse        (customFormatters)
import           Web.ClientSession          (getDefaultKey)

startApp :: IO ()
startApp = run 8080 =<< app

app :: IO Application
app = do
  loadFile defaultConfig
  dataDir <- getEnv "DATA_DIR"
  pool <- getDbPool
  key <- getDefaultKey
  let env = Env pool key dataDir
  return $ serveWithContext api (customFormatters :. authContext env :. EmptyContext) $
           runReader server env

getDbPool :: IO Pool
getDbPool = do
  databasePoolSize <- read <$> getEnv "DATABASE_POOL_SIZE"
  databasePoolTimeout <- read <$> getEnv "DATABASE_POOL_TIMEOUT"
  databaseUrl <- cs <$> getEnv "DATABASE_URL"
  acquire databasePoolSize (Just databasePoolTimeout) databaseUrl

api :: Proxy API
api = Proxy

server :: Reader Env (Server API)
server = do
  authServer <- authServerReader
  profileServer <- profileServerReader
  healthRecordServer <- healthRecordServerReader
  accountServer <- accountServerReader
  return $ authServer
      :<|> profileServer
      :<|> healthRecordServer
      :<|> accountServer

type API = AuthAPI
      :<|> ProfileAPI
      :<|> HealthRecordAPI
      :<|> AccountAPI




