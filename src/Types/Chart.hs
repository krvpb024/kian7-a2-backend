{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Types.Chart (Chart (Chart), ChartValue (ChartValue), ChartValueData (ChartValueData), ChartValueDataInfo (ChartValueDataInfo), genChart, roundTo, getSvgWidth, getDropLineCoords, getSvgViewBox, getXBaseCoords, getXMaxCoords, getYBaseCoords, getYMaxCoords, getXAxesCoords, ceilingMinFloorMaxHealthRecordsValue, getMinMaxHealthRecordsValue, getPaddedHealthRecordTables, getAllValueByIndicator, getYAxesCoordByValue, getRangeCoords, getYAxesCoords, getYAxesCoordsWithInfo) where

import qualified Data.Aeson            as A
import           Data.Foldable         (Foldable (foldl'))
import           Data.Map.Strict       (Map)
import qualified Data.Map.Strict       as Map
import           Data.Maybe            (fromJust)
import           Data.Range            (Bound (Bound),
                                        Range (LowerBoundRange, SpanRange, UpperBoundRange),
                                        aboveRange, belowRange, inRange)
import           Data.Time             (Day, diffDays)
import           Data.Vector           (Vector)
import qualified Data.Vector           as V
import           GHC.Generics          (Generic)
import           Types.HealthIndicator (HealthIndicator (Bmi, BodyFatPercentage, WaistlineCm, WeightKg),
                                        getHealthIndicatorName)
import           Types.HealthRecord    (HealthRecordTable (healthRecordBodyFatPercentage, healthRecordDate, healthRecordWaistlineCm, healthRecordWeightKg),
                                        healthRecordBmi)

genChart :: Double
         -> Double
         -> Maybe Day
         -> Maybe Day
         -> HealthIndicator
         -> Maybe (Range Double)
         -> Vector HealthRecordTable
         -> Maybe Chart
genChart dataDistanceWidth
         height
         maybeFilterFrom
         maybeFilterTo
         indicatorType
         maybeHealthIndicatorRange
         healthRecordTables
  | null healthRecordTables = Nothing
  | otherwise = Just $ Chart
    { svgWidth = width
    , svgHeight = height
    , svgViewBox = getSvgViewBox width
                                 height
                                 (20, 50, 50, 50)
    , xAxesCoords = xAxesCoordsWithDay
    , xAxisLineCoords = (xBaseCoords, xMaxCoords)
    , yAxisLineCoords = (yBaseCoords, yMaxCoords)
    , yAxisLineMinMaxValue = minMaxHealthRecordsValue
    , dropLineCoords = getDropLineCoords yBaseCoords yMaxCoords xAxesCoordsWithDay
    , healthRecordValuesCoords = yAxesCoords
    , healthIndicatorName = getHealthIndicatorName indicatorType
    , healthIndicatorRange = getRangeCoords xBaseCoords
                                            xMaxCoords
                                            minMaxHealthRecordsValue
                                            height =<< maybeHealthIndicatorRange
    }
  where
    width :: Double
    width = getSvgWidth dataDistanceWidth paddedHealthRecordTables

    minMaxHealthRecordsValue :: Maybe (Double, Double)
    minMaxHealthRecordsValue =
      getMinMaxHealthRecordsValue maybeHealthIndicatorRange
                                  allPaddedYAxisValue

    xBaseCoords :: Coordinates
    xBaseCoords = getXBaseCoords width height

    xMaxCoords :: Coordinates
    xMaxCoords  = getXMaxCoords width height

    yBaseCoords :: Coordinates
    yBaseCoords = getYBaseCoords width height

    yMaxCoords :: Coordinates
    yMaxCoords  = getYMaxCoords width height

    paddedHealthRecordTables :: Map Day (Maybe HealthRecordTable)
    paddedHealthRecordTables = getPaddedHealthRecordTables maybeFilterFrom maybeFilterTo healthRecordTables

    allPaddedYAxisValue :: Map Day (Maybe Double)
    allPaddedYAxisValue = getAllValueByIndicator indicatorType
                                                 paddedHealthRecordTables

    xAxesCoordsWithDay :: Vector (Day, Coordinates)
    xAxesCoordsWithDay = getXAxesCoords width
                                        height $
                                        V.fromList $
                                          Map.keys paddedHealthRecordTables

    yAxesMaybeCoords :: [(Day, Maybe (Double, Coordinates))]
    yAxesMaybeCoords = getYAxesCoords height
                                      minMaxHealthRecordsValue
                                      xAxesCoordsWithDay
                                      allPaddedYAxisValue

    yAxesCoords :: [ChartValue]
    yAxesCoords = getYAxesCoordsWithInfo yAxesMaybeCoords
                                         maybeHealthIndicatorRange

roundTo :: Int -> Double -> Double
roundTo n x = fromInteger (round $ x * (10^n)) / (10.0^^n)

getSvgWidth :: Double -> Map Day (Maybe HealthRecordTable) -> Double
getSvgWidth dataDistanceWidth healthRecords = dataDistanceWidth *
                                              fromIntegral (length healthRecords)

getDropLineCoords :: Coordinates
                  -> Coordinates
                  -> Vector (Day, Coordinates)
                  -> Vector (Coordinates, Coordinates)
getDropLineCoords yBaseCoords yMaxCoords = fmap (\(_, (x, _)) ->
                                                   ( (x, snd yBaseCoords)
                                                   , (x, snd yMaxCoords) )
                                                )

getSvgViewBox :: Double
              -> Double
              -> Padding
              -> (Coordinates, Coordinates)
getSvgViewBox width height (topPadding, rightPadding, bottomPadding, leftPadding) =
  ( ( negate leftPadding
    , negate topPadding
    )
  , ( width + rightPadding + leftPadding
    , height + bottomPadding + topPadding
    )
  )

getXBaseCoords :: Double -> Double -> Coordinates
getXBaseCoords  = (,) . const 0

getXMaxCoords :: Double -> Double -> Coordinates
getXMaxCoords = (,)

getYBaseCoords :: Double -> Double -> Coordinates
getYBaseCoords = getXBaseCoords

getYMaxCoords :: Double -> Double -> Coordinates
getYMaxCoords _ _ = (0, 0)

getXAxesCoords :: Double
               -> Double
               -> Vector Day
               -> Vector (Day, Coordinates)
getXAxesCoords width height ds = V.imap getCoordsByDay ds
  where
    getCoordsByDay :: Int -> Day -> (Day, Coordinates)
    getCoordsByDay vectorElementIndex day =
      ( day
      , ( fst xBaseCoords +
          (fromIntegral vectorElementIndex /
           fromIntegral (V.length ds - 1)) *
          width
        , snd xBaseCoords
        )
      )
    xBaseCoords = getXBaseCoords width height

ceilingMinFloorMaxHealthRecordsValue :: (Double, Double) -> (Double, Double)
ceilingMinFloorMaxHealthRecordsValue (l, u) = ( fromInteger $ floor   (max 0 (l - 10) / 10) * 10
                                              , fromInteger $ ceiling ((u + 10) / 10) * 10 )

getMinMaxHealthRecordsValue :: Maybe (Range Double)
                            -> Map Day (Maybe Double)
                            -> Maybe (Double, Double)
getMinMaxHealthRecordsValue maybeRange mapDayAndMaybeHealthRecordTableValue
  | null mapDayAndMaybeHealthRecordTableValue = Nothing
  | null allValue = Nothing
  | otherwise = return $ case maybeRange of
    Just (SpanRange (Bound lower _) (Bound upper _)) ->
      ceilingMinFloorMaxHealthRecordsValue ( min lower minValue
                                           , max upper maxValue
                                           )
    Just (UpperBoundRange (Bound upper _)) ->
      ceilingMinFloorMaxHealthRecordsValue ( minValue
                                           , max upper maxValue
                                           )
    Just (LowerBoundRange (Bound lower _)) ->
      ceilingMinFloorMaxHealthRecordsValue ( min lower minValue
                                           , maxValue
                                           )
    _ ->
      ceilingMinFloorMaxHealthRecordsValue ( minValue
                                           , maxValue
                                           )
  where
    minValue = V.minimum allValue
    maxValue = V.maximum allValue
    allValue = V.catMaybes (V.fromList $
                            Map.elems mapDayAndMaybeHealthRecordTableValue)

getPaddedHealthRecordTables :: Maybe Day
                            -> Maybe Day
                            -> Vector HealthRecordTable
                            -> Map Day (Maybe HealthRecordTable)
getPaddedHealthRecordTables maybeFilterFrom maybeFilterTo hrts
  | V.null hrts = mempty
  | otherwise = Map.union healthRecordMap $ allDateMap allDate
  where
    allDateMap :: Vector Day -> Map Day (Maybe HealthRecordTable)
    allDateMap ds
      | null ds = mempty
      | otherwise = foldr (`Map.insert` Nothing)
                       mempty $
                       padDate 7 $ V.enumFromTo (V.minimum ds)
                                                (V.maximum ds)
      where
        padDate :: Int -> Vector Day -> Vector Day
        padDate minLength dates
          | length dates < minLength  = padDate minLength $ V.snoc dates $ succ $ V.last dates
          | otherwise                 = dates

    allDate :: Vector Day
    allDate = V.catMaybes (((Just . healthRecordDate <$> hrts) `V.snoc` maybeFilterFrom) `V.snoc` maybeFilterTo)

    healthRecordMap :: Map Day (Maybe HealthRecordTable)
    healthRecordMap = foldr (\h acc -> Map.insert (healthRecordDate h) (pure h) acc)
                            mempty
                            hrts

getAllValueByIndicator :: HealthIndicator
                       -> Map Day (Maybe HealthRecordTable)
                       -> Map Day (Maybe Double)
getAllValueByIndicator Bmi mapDayMaybeHealthRecordTable =
  fmap healthRecordBmi <$>
       mapDayMaybeHealthRecordTable
getAllValueByIndicator WeightKg mapDayMaybeHealthRecordTable =
  fmap healthRecordWeightKg <$>
       mapDayMaybeHealthRecordTable
getAllValueByIndicator BodyFatPercentage mapDayMaybeHealthRecordTable =
  (fmap . (=<<)) healthRecordBodyFatPercentage
                 mapDayMaybeHealthRecordTable
getAllValueByIndicator WaistlineCm mapDayMaybeHealthRecordTable =
  (fmap . (=<<)) healthRecordWaistlineCm
                 mapDayMaybeHealthRecordTable

getYAxesCoordByValue :: Double -> Double -> Double -> Double -> Double
getYAxesCoordByValue minValue maxValue height value =
  height - (height * percentage)
  where
    percentage = (value - minValue) / (maxValue - minValue)

getRangeCoords :: Coordinates
               -> Coordinates
               -> Maybe ( Double, Double )
               -> Double
               -> Range Double
               -> Maybe ( ( Double, ( Coordinates, Coordinates ), Bool )
                        , ( Double, ( Coordinates, Coordinates ), Bool ) )
getRangeCoords _ _ Nothing _ _ = Nothing
getRangeCoords (xBaseCoordX, _)
               (xMaxCoordX, _)
               (Just (minValue, maxValue))
               height
               maybeRange =
  case maybeRange of
    SpanRange (Bound lower _) (Bound upper _) ->
      Just ( ( lower
             , ( ( xBaseCoordX, getYAxesCoordsByMinMax lower )
               , ( xMaxCoordX,  getYAxesCoordsByMinMax lower ) )
             , True )
           , ( upper
             , ( ( xBaseCoordX, getYAxesCoordsByMinMax upper )
               , ( xMaxCoordX,  getYAxesCoordsByMinMax upper ) )
             , True )
           )
    UpperBoundRange (Bound upper _) ->
      Just ( ( minValue
             , ( ( xBaseCoordX, getYAxesCoordsByMinMax minValue )
               , ( xMaxCoordX,  getYAxesCoordsByMinMax minValue ) )
             , False )
           , ( upper
             , ( ( xBaseCoordX, getYAxesCoordsByMinMax upper )
               , ( xMaxCoordX,  getYAxesCoordsByMinMax upper ) )
             , True )
           )
    LowerBoundRange (Bound lower _) ->
      Just ( ( lower
             , ( ( xBaseCoordX, getYAxesCoordsByMinMax lower )
               , ( xMaxCoordX,  getYAxesCoordsByMinMax lower ) )
             , True )
           , ( maxValue
             , ( ( xBaseCoordX, getYAxesCoordsByMinMax maxValue )
               , ( xMaxCoordX,  getYAxesCoordsByMinMax maxValue ) )
             , False )
           )
    _ -> Nothing
  where
    getYAxesCoordsByMinMax = getYAxesCoordByValue minValue
                                                  maxValue
                                                  height

getYAxesCoords :: Double
               -> Maybe (Double, Double)
               -> Vector (Day, Coordinates)
               -> Map Day (Maybe Double)
               -> [(Day, Maybe (Double, Coordinates))]
getYAxesCoords _      Nothing                     _                  _ = mempty
getYAxesCoords height (Just (minValue, maxValue)) xAxesCoordsWithDay mapHrts
  | null xAxesCoordsWithDay = mempty
  | null mapHrts = mempty
  | otherwise = Map.toList maybeCoordsMap
  where
    maybeCoordsMap :: Map Day (Maybe (Double, Coordinates))
    maybeCoordsMap = Map.mapWithKey (\day maybeValue ->
                                      fmap (\value -> ( value
                                                      , ( fst $ getXAxesCoordByDay xAxesCoordsWithDay mapHrts day
                                                        , getYAxesCoordByValue minValue maxValue height value
                                                        )
                                                      ))
                                           maybeValue
                                    )
                                    mapHrts
    getXAxesCoordByDay :: Vector (Day, Coordinates)
                       -> Map Day (Maybe Double)
                       -> Day
                       -> Coordinates
    getXAxesCoordByDay dayCoords hrts d =
      snd $ (V.!) dayCoords $ fromJust $ V.elemIndex d $ V.fromList $ Map.keys hrts

getYAxesCoordsWithInfo :: [(Day, Maybe (Double, Coordinates))]
                       -> Maybe (Range Double)
                       -> [ChartValue]
getYAxesCoordsWithInfo yAxesMaybeCoords maybeHealthIndicatorRange = fst $ foldl' foldFn (mempty, Nothing) yAxesMaybeCoords
  where
    foldFn :: ([ChartValue], Maybe (Day, Double))
           -> (Day, Maybe (Double, Coordinates))
           -> ([ChartValue], Maybe (Day, Double))
    foldFn (vs, Nothing) (currentDate, Nothing) =
      ( vs <> [ChartValue currentDate Nothing]
      , Nothing )
    foldFn (vs, Nothing) (currentDate, Just (currentValue, currentCoords)) =
      ( vs <> [ChartValue currentDate
                          (Just (ChartValueData currentValue
                                                currentCoords
                                                (rangeCompare currentValue =<<
                                                  maybeHealthIndicatorRange)
                                                Nothing))]
      , Just (currentDate, currentValue) )
    foldFn (vs, Just (lastValueDate, lastValue)) (currentDate, Just (currentValue, currentCoords)) =
      ( vs <> [ChartValue currentDate
                          (Just (ChartValueData currentValue
                                                currentCoords
                                                (rangeCompare currentValue =<<
                                                  maybeHealthIndicatorRange)
                                                (Just (ChartValueDataInfo (diffDays currentDate lastValueDate)
                                                                          (currentValue - lastValue)))))]
      , Just (currentDate, currentValue) )
    foldFn (vs, Just (lastValueDate, lastValue)) (currentDate, Nothing) =
      ( vs <> [ChartValue currentDate Nothing]
      , Just (lastValueDate, lastValue) )

    rangeCompare :: Double -> Range Double -> Maybe (Bool, Bool, Bool)
    rangeCompare value range
      | belowRange range value = return (True,  False, False)
      | inRange    range value = return (False, True,  False)
      | aboveRange range value = return (False, False, True)
      | otherwise = Nothing

type Coordinates = (Double, Double)

type Padding = (Double, Double, Double, Double)

data ChartValue = ChartValue {
  chartValueDate      :: Day
, chartValueValueData :: Maybe ChartValueData
} deriving (Eq, Show, Generic)

instance A.ToJSON ChartValue where
  toJSON (ChartValue date valueData) =
    A.object [ "date"      A..= date
             , "valueData" A..= valueData
             ]

data ChartValueData = ChartValueData {
  chartValueDataValue           :: Double
, chartValueDataCoordinates     :: Coordinates
, chartValueDataRangeComparison :: Maybe (Bool, Bool, Bool)
, chartValueDataInfo            :: Maybe ChartValueDataInfo
} deriving (Eq, Show, Generic)

instance A.ToJSON ChartValueData where
  toJSON (ChartValueData value coords maybeRangeComparison info) =
    A.object [ "value"           A..= value
             , "coords"          A..= coords
             , "rangeComparison" A..= maybeRangeComparison
             , "info"            A..= info
             ]

data ChartValueDataInfo = ChartValueDataInfo {
  chartValueDataInfoDiffLastDate  :: Integer
, chartValueDataInfoDiffLastValue :: Double
} deriving (Eq, Show, Generic)

instance A.ToJSON ChartValueDataInfo where
  toJSON (ChartValueDataInfo diffDay value) =
    A.object [ "diffDay" A..= diffDay
             , "value"   A..= value
             ]

data Chart = Chart
  { svgWidth                 :: Double
  , svgHeight                :: Double
  , svgViewBox               :: (Coordinates, Coordinates)
  , xAxesCoords              :: Vector (Day, Coordinates)
  , xAxisLineCoords          :: (Coordinates, Coordinates)
  , yAxisLineCoords          :: (Coordinates, Coordinates)
  , yAxisLineMinMaxValue     :: Maybe (Double, Double)
  , dropLineCoords           :: Vector (Coordinates, Coordinates)
  , healthRecordValuesCoords :: [ChartValue]
  , healthIndicatorName      :: String
  , healthIndicatorRange     :: Maybe ((Double, (Coordinates, Coordinates), Bool),
                                       (Double, (Coordinates, Coordinates), Bool))
  } deriving (Eq, Show, Generic, A.ToJSON)

