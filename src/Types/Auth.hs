{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies      #-}

module Types.Auth (AuthSignUpData (AuthSignUpData), AuthSignInData (AuthSignInData), SessionData (SessionData)) where

import qualified Data.Aeson                       as A
import           Data.Int                         (Int32)
import qualified Data.Text.Lazy                   as TL
import           GHC.Generics                     (Generic)
import           Servant                          (AuthProtect, ServerError)
import           Servant.Server.Experimental.Auth (AuthServerData)
import           Web.FormUrlEncoded               (FromForm (fromForm),
                                                   parseUnique)

type instance AuthServerData (AuthProtect "cookie-auth") = Either ServerError SessionData

data AuthSignUpData = AuthSignUpData
  { authSignUpName            :: TL.Text
  , authSignUpPassword        :: TL.Text
  , authSignUpPasswordConfirm :: TL.Text
  } deriving (Eq, Show, Generic)

instance A.FromJSON AuthSignUpData where
  parseJSON = A.withObject "AuthSignUpData" $ \v ->
    AuthSignUpData <$> v A..: "name"
                   <*> v A..: "password"
                   <*> v A..: "passwordConfirm"

instance A.ToJSON AuthSignUpData where
  toJSON (AuthSignUpData name password passwordConfirm) =
    A.object [ "name"            A..= name
             , "password"        A..= password
             , "passwordConfirm" A..= passwordConfirm
             ]

instance FromForm AuthSignUpData where
  fromForm f =
    AuthSignUpData <$> parseUnique "name" f
                   <*> parseUnique "password" f
                   <*> parseUnique "passwordConfirm" f

data AuthSignInData = AuthSignInData
  { authSignInName     :: TL.Text
  , authSignInPassword :: TL.Text
  } deriving (Eq, Show, Generic)

instance A.FromJSON AuthSignInData where
  parseJSON = A.withObject "AuthSignInData" $ \v ->
    AuthSignInData <$> v A..: "name"
                   <*> v A..: "password"

instance A.ToJSON AuthSignInData where
  toJSON (AuthSignInData name password) =
    A.object [ "name"     A..= name
             , "password" A..= password
             ]

instance FromForm AuthSignInData where
  fromForm f =
    AuthSignInData <$> parseUnique "name" f
                   <*> parseUnique "password" f

data SessionData = SessionData { sessionDataId        :: TL.Text
                               , sessionDataAccountId :: Int32
                               } deriving (Eq, Show, Generic, A.FromJSON, A.ToJSON)
