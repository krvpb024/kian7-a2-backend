{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Types.Account (AccountTable (AccountTable), accountTableId, accountTableName, accountTablePassword, AccountValue (AccountValue), accountValueId,  accountValueName, AccountNamePatch (AccountNamePatch), newAccountName, AccountPasswordPatch (AccountPasswordPatch)) where

import qualified Data.Aeson     as A
import           Data.Int       (Int32)
import qualified Data.Text.Lazy as TL
import           GHC.Generics   (Generic)

data AccountTable = AccountTable
  { accountTableId       :: Int32
  , accountTableName     :: TL.Text
  , accountTablePassword :: TL.Text
  } deriving (Eq, Show, Generic)

newtype AccountNamePatch = AccountNamePatch
  { newAccountName :: TL.Text
  } deriving (Eq, Show, Generic)

instance A.FromJSON AccountNamePatch where
  parseJSON = A.withObject "AccountNamePatch" $ \v ->
    AccountNamePatch <$> v A..: "name"

instance A.ToJSON AccountNamePatch where
  toJSON (AccountNamePatch newAccName) =
    A.object [ "name" A..= newAccName
             ]

data AccountValue = AccountValue
  { accountValueId   :: Int32
  , accountValueName :: TL.Text
  }

instance A.FromJSON AccountValue where
  parseJSON = A.withObject "AccountValue" $ \v ->
    AccountValue <$> v A..: "id"
                 <*> v A..: "name"

instance A.ToJSON AccountValue where
  toJSON (AccountValue accId accName) =
    A.object [ "id"   A..= accId
             , "name" A..= accName
             ]

data AccountPasswordPatch = AccountPasswordPatch
  { newAccountPasswordOld        :: TL.Text
  , newAccountPasswordNew        :: TL.Text
  , newAccountPasswordNewConfirm :: TL.Text
  } deriving (Eq, Show, Generic)

instance A.FromJSON AccountPasswordPatch where
  parseJSON = A.withObject "AccountPasswordPatch" $ \v ->
    AccountPasswordPatch <$> v A..: "passwordOld"
                         <*> v A..: "passwordNew"
                         <*> v A..: "passwordNewConfirm"

instance A.ToJSON AccountPasswordPatch where
  toJSON (AccountPasswordPatch newAccPasswordOld newAccPasswordNew newAccPasswordNewConfirm) =
    A.object [ "passwordOld"        A..= newAccPasswordOld
             , "passwordNew"        A..= newAccPasswordNew
             , "passwordNewConfirm" A..= newAccPasswordNewConfirm
             ]
