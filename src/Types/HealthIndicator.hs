{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Types.HealthIndicator (HealthIndicator (Bmi, BodyFatPercentage, WaistlineCm, WeightKg), getHealthIndicatorName, getHealthIndicatorNormalRange, readIndicatorJson, getAgeKey, removeDotZero) where

import           Control.Exception      (PatternMatchFail (PatternMatchFail),
                                         throw)
import           Control.Monad.IO.Class (MonadIO (liftIO))
import qualified Data.Aeson             as A
import qualified Data.Aeson.Key         as A
import qualified Data.Aeson.KeyMap      as A
import qualified Data.ByteString.Lazy   as BL
import           Data.Functor           ((<&>))
import           Data.List              (isSuffixOf, sort)
import           Data.Maybe             (fromJust)
import           Data.Range             (Range, lbi, ube, (+=*))
import           GHC.Generics           (Generic)
import           Types.Profile          (Gender, getGenderKey)
import           Web.HttpApiData        (FromHttpApiData (parseQueryParam))

data HealthIndicator = Bmi
                     | BodyFatPercentage
                     | WaistlineCm
                     | WeightKg
                     deriving (Eq, Show, Generic, A.ToJSON)

instance FromHttpApiData HealthIndicator where
  parseQueryParam "bmi"               = Right Bmi
  parseQueryParam "bodyFatPercentage" = Right BodyFatPercentage
  parseQueryParam "waistlineCm"       = Right WaistlineCm
  parseQueryParam "weightKg"          = Right WeightKg
  parseQueryParam _                   = Left "No Such Indicator Type"

type HealthIndicatorJson = A.KeyMap (A.KeyMap (A.KeyMap (Maybe Double, Maybe Double)))

getHealthIndicatorName :: HealthIndicator -> String
getHealthIndicatorName Bmi               = "BMI"
getHealthIndicatorName BodyFatPercentage = "體脂率"
getHealthIndicatorName WaistlineCm       = "腰圍"
getHealthIndicatorName WeightKg          = "體重"

getHealthIndicatorSnakeCaseName :: HealthIndicator -> String
getHealthIndicatorSnakeCaseName Bmi               = "bmi"
getHealthIndicatorSnakeCaseName BodyFatPercentage = "body_fat_percentage"
getHealthIndicatorSnakeCaseName WaistlineCm       = "waistline_cm"
getHealthIndicatorSnakeCaseName WeightKg          = "weight_kg"

getHealthIndicatorNormalRange :: Gender
                              -> Double
                              -> Maybe HealthIndicatorJson
                              -> Maybe (Range Double)
getHealthIndicatorNormalRange _      _   Nothing       = Nothing
getHealthIndicatorNormalRange gender age indicatorJson = do
  let ageKeys = (indicatorJson >>=
                 A.lookup (A.fromString $ getGenderKey gender)) <&>
                 A.keys <&>
                 fmap (read . init . tail . show) :: Maybe [Double]
  let ageKey = A.fromString $ removeDotZero $ show $ getAgeKey age $ fromJust ageKeys
  let normalRange = indicatorJson >>=
                    A.lookup (A.fromString $ getGenderKey gender) >>=
                    A.lookup ageKey >>=
                    A.lookup (A.fromString "normal")
  case normalRange of
    Just (Nothing, Just u)  -> return $ ube u
    Just (Just l,  Nothing) -> return $ lbi l
    Just (Just l,  Just u)  -> return $ l +=* u
    Nothing                 -> Nothing
    _                       -> throw $ PatternMatchFail "invalid HealthIndicator normal range"

removeDotZero :: String -> String
removeDotZero d = if ".0" `isSuffixOf` d
  then takeWhile (/= '.') d
  else d

getAgeKey :: Double -> [Double] -> Double
getAgeKey age ageKeys = foldr (getClosestAgeKey age) 0 $ reverse $ sort ageKeys
  where
    getClosestAgeKey :: Double -> Double -> Double -> Double
    getClosestAgeKey a ak acc
      | (acc <= a) && (a < ak) = acc
      | otherwise = ak

readIndicatorJson :: (MonadIO m) => HealthIndicator -> m (Maybe HealthIndicatorJson)
readIndicatorJson WeightKg = return Nothing
readIndicatorJson indicator = do
  normalRangeBS <- liftIO $ BL.readFile $
                            "static/" <> getHealthIndicatorSnakeCaseName indicator <> "_indicator.json"
  return $ A.decode normalRangeBS
