{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Types.Profile (ProfileTable (ProfileTable), profileTableGender, profileTableBirthDate, Gender (Male, Female), getGenderKey, ageFromTo) where

import qualified Data.Aeson               as A
import           Data.Char                (toLower)
import qualified Data.Csv                 as C
import           Data.Maybe               (fromJust)
import           Data.String.Conversions  (cs)
import           Data.Time                (Day, DayOfMonth, MonthOfYear, Year)
import           Data.Time.Format.ISO8601 (iso8601ParseM, iso8601Show)
import           GHC.Generics             (Generic)

data ProfileTable = ProfileTable { profileTableGender    :: Gender
                                 , profileTableBirthDate :: Day
                                 } deriving (Eq, Show, Generic)

ageFromTo :: (Year, MonthOfYear, DayOfMonth) -> (Year, MonthOfYear, DayOfMonth) -> Double
ageFromTo (fromYear, fromMonth, _) (toYear, toMonth, _) = fromIntegral (fromYear - toYear) + (fromIntegral (fromMonth - toMonth) / 12)

instance A.FromJSON ProfileTable where
  parseJSON = A.withObject "Profile" $ \v ->
    ProfileTable <$> v A..: "gender"
                 <*> v A..: "birthDate"

instance A.ToJSON ProfileTable where
  toJSON (ProfileTable gender birthDate) =
    A.object [ "gender"     A..= gender
             , "birthDate" A..= birthDate
             ]

instance C.FromNamedRecord ProfileTable where
  parseNamedRecord r =
    ProfileTable <$> r C..: "gender"
                 <*> (fromJust . iso8601ParseM <$> r C..: "birthDate")

instance C.ToNamedRecord ProfileTable where
  toNamedRecord (ProfileTable gender birthDate) =
    C.namedRecord [ "gender" C..= show gender
                  , "birthDate" C..= iso8601Show birthDate
                  ]

data Gender = Male | Female
  deriving (Eq, Show, Read, Generic, A.FromJSON, A.ToJSON)

instance C.FromField Gender where
  parseField s
    | s == "Male"   = return Male
    | s == "Female" = return Female
    | otherwise     = fail "parsing gender error"

instance C.ToField Gender where
    toField = cs . show

getGenderKey :: Gender -> String
getGenderKey g = toLower <$> show g
