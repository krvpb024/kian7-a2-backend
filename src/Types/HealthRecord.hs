{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Types.HealthRecord (HealthRecordTable (HealthRecordTable), healthRecordDate, healthRecordHeightCm, healthRecordWeightKg, healthRecordBodyFatPercentage, healthRecordWaistlineCm, healthRecordBmi) where

import qualified Data.Aeson               as A
import           Data.Aeson.Types         (Parser)
import           Data.Bool                (bool)
import qualified Data.Csv                 as C
import           Data.Time                (Day)
import           Data.Time.Format.ISO8601 (iso8601ParseM, iso8601Show)
import           GHC.Generics             (Generic)


data HealthRecordTable = HealthRecordTable
  { healthRecordDate              :: Day
  , healthRecordHeightCm          :: Double
  , healthRecordWeightKg          :: Double
  , healthRecordBodyFatPercentage :: Maybe Double
  , healthRecordWaistlineCm       :: Maybe Double
  } deriving (Eq, Show, Generic)

healthRecordBmi :: HealthRecordTable -> Double
healthRecordBmi (HealthRecordTable _ height weight _ _) = weight / ((height / 100) ** 2)

instance C.FromNamedRecord HealthRecordTable where
  parseNamedRecord r = do
    HealthRecordTable <$> (checkParseDayValid . iso8601ParseM =<< (r C..: "date"))
                      <*> r C..: "heightCm"
                      <*> r C..: "weightKg"
                      <*> r C..: "bodyFatPercentage"
                      <*> r C..: "waistlineCm"
    where
      checkParseDayValid :: Maybe Day -> C.Parser Day
      checkParseDayValid (Just d) = return d
      checkParseDayValid _        = fail "parse date field error"

instance C.ToNamedRecord HealthRecordTable where
  toNamedRecord (HealthRecordTable date heightCm weightKg bodyFatPercentage waistlineCm) =
    C.namedRecord [ "date" C..= iso8601Show date
                  , "heightCm" C..= heightCm
                  , "weightKg" C..= weightKg
                  , "bodyFatPercentage" C..= bodyFatPercentage
                  , "waistlineCm" C..= waistlineCm
                  ]

instance A.FromJSON HealthRecordTable where
  parseJSON = A.withObject "HealthRecord" $ \v ->
    HealthRecordTable <$> v A..: "date"
                      <*> (nanToError "heightCm" =<< (v A..: "heightCm"))
                      <*> (nanToError "weightKg" =<< (v A..: "weightKg"))
                      <*> (nanToNothing "bodyFatPercentage" =<< (v A..: "bodyFatPercentage"))
                      <*> (nanToNothing "waistlineCm" =<< (v A..: "waistlineCm"))
    where
      nanToError :: String -> Double -> Parser Double
      nanToError k n = bool (return n)
                            (fail $ "['" <> k <> "']: parsing Double failed, unexpected Null")
                            (isNaN n)
      nanToNothing :: String -> Maybe Double -> Parser (Maybe Double)
      nanToNothing _ n = return $ case isNaN <$> n of
        Just False -> n
        _          -> Nothing

instance A.ToJSON HealthRecordTable where
  toJSON (HealthRecordTable date heightCm weightKg bodyFatPercentage waistlineCm) =
    A.object [ "date" A..= date
             , "heightCm" A..= heightCm
             , "weightKg" A..= weightKg
             , "bodyFatPercentage" A..= bodyFatPercentage
             , "waistlineCm" A..= waistlineCm
             ]
