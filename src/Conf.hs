{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Conf (Env (Env), getPool, getSessionKey, getDataDir, Session(..)) where

import           Data.Aeson        (ToJSON)
import           Data.Text.Lazy    (Text)
import           GHC.Generics      (Generic)
import           Hasql.Pool        (Pool)
import           Web.ClientSession (Key)

data Env = Env { getPool       :: Pool
               , getSessionKey :: Key
               , getDataDir    :: String
               }

newtype Session= Session { account :: Text
                         } deriving (Eq, Show, Generic, ToJSON)
